#include "ThirdPay.h"
//#include "mmiap/MMIAP.h"
#include "JniUtils.h"
#include "platform/android/jni/JniHelper.h"
#include "OuterCallBack.h"

using namespace cocos2d;

namespace LexAndroid
{
	namespace Pay
	{

		extern "C"
		{
			void Java_com_test_sdk_activity_SdkMainActivity_stopWaitCallBack(JNIEnv *env,jobject thiz)
			{
				//LOGI("Java_com_lexgame_mmIAP_InitFinishCallback");
				//std::string str = JniHelper::jstring2string(json);
				////PayMMIAP::getShared()->afterInit(str.data(), result);
				//Lex::ThreadHelper::TPMessage tpmsg;
				//tpmsg.isSuccess = result;
				//tpmsg.msg = str;
				//Lex::ThreadHelper::CallBackTask cbt = {(int)ThirdPay::MMIAPinit, tpmsg};
				//Lex::ThreadHelper::addCBQueue(cbt);
				//LOGI("%s", str.c_str());


				//Outer::onWaitCall.run();
			}

			void Java_com_test_sdk_activity_SdkShowAllActivity_BillingFinishCallback(JNIEnv *env,jobject thiz,jstring json, jboolean result)
			{
				LOGI("Java_com_lexgame_mmIAPListener_BillingFinishCallback");
				std::string str = JniHelper::jstring2string(json);
				LOGI("JniHelper::jstring2string(json) %s", str.c_str());
				Lex::ThreadHelper::TPMessage tpmsg;
				tpmsg.isSuccess = result;
				tpmsg.msg = str;
				Lex::ThreadHelper::CallBackTask cbt = {(int)ThirdPay::MM360Pay, tpmsg};
				Lex::ThreadHelper::addCBQueue(cbt);
				str = tpmsg.msg;
				LOGI("afterpay tpmsg %s", str.c_str());
				LOGI("%s", str.c_str());
			}
		}

		PayCM360 * PayCM360::getShared()
		{
			static PayCM360 * me = new PayCM360();
			return me;
		}

		//void PayMMIAP::init(const char * appID, const char *appKey, GetPayMsg isInitSuccessFunc)
		//{
		//	LexAndroid::MMIAP::initMMIAP(appID, appKey, isInitSuccessFunc);
		//}

		//void PayMMIAP::createOrder(const char* payCode, GetPayMsg _afterPay)
		//{
		//	LexAndroid::MMIAP::createOrder(payCode, _afterPay);
		//}

		void PayCM360::createOrder(const char* payCode, char* payCost, GetPayMsg _afterPay){
			afterPay = _afterPay;
			JniMethodInfo orderInfo;
			bool isHave = JniHelper::getStaticMethodInfo(orderInfo, "com/lexgame/shuihu/shuihu", 
				"createOrderStatic", "(Ljava/lang/String;Ljava/lang/String;)V");
			if(isHave)
			{
				LOGI("createOrderStatic -- found");
				jstring mpayCode = Lex::JniUtils::stoJstring(orderInfo.env, payCode);
				jstring mpayCost = Lex::JniUtils::stoJstring(orderInfo.env, payCost);
				orderInfo.env->CallStaticVoidMethod(orderInfo.classID, orderInfo.methodID, mpayCode, mpayCost);
				LOGI("createOrderStatic -- end");
			}
			else
				LOGE("createOrderStatic -- not found");
		}

		void PayCM360::init(const char * appID, const char *appKey, GetPayMsg _afterInit){
			afterInit = _afterInit;
			JniMethodInfo initInfo;
			bool isHave = JniHelper::getStaticMethodInfo(initInfo, "com/lexgame/mmIAP/MMIAP",
				"initMMIAPStatic", "(Ljava/lang/String;Ljava/lang/String;)V");
			if(isHave)
			{
				LOGI("initMMIAPStatic -- found");
				jstring mappID = Lex::JniUtils::stoJstring(initInfo.env, appID);
				jstring mappKey = Lex::JniUtils::stoJstring(initInfo.env, appKey);
				initInfo.env->CallStaticVoidMethod(initInfo.classID, initInfo.methodID, mappID, mappKey);
				LOGI("initMMIAPStatic -- end");
			}
			else
				LOGE("initMMIAPStatic -- not found");
		}

		void PayCM360::runAfterInit(const char* str, bool success)
		{
			afterInit(str, success);
		}

		void PayCM360::runAfterPay(const char* str, bool success)
		{
			afterPay(str, success);
		}
	}
}
