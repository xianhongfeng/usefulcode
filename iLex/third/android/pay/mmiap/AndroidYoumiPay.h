﻿#pragma once
//#include <iostream>
#include "platform/android/jni/JniHelper.h"
#include "cocos2d.h"
#include "ThirdPay.h"

using namespace cocos2d;
//using namespace std;

namespace LexAndroid{
	namespace YoumiPay{
		
		extern void initYoumiPay(const char* _appID, const char* _appSecret, GetPayMsg isInitSuccessFunc, bool _isSDKCallback = true);
		/**
			* @param _description	订单描述
			* @param _total 订单金额
			* @param _tradeno 订单号【可选】外部订单号
			* @param _infotmation	具体信息【可选】游戏开发商自定义数据。该值将在用户充值成功后，在支付工具服务器回调给游戏开发商时携带该数据
			* @param _afterPay 回调
			*/
		extern void createOrderAndPay(const char * _description, int _total, GetPayMsg _afterPay, const char * _tradeno = "", const char* _information = "");
	}
}