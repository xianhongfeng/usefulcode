﻿#include "AndroidYoumiPay.h"
#include "JniUtils.h"

namespace LexAndroid{
	namespace YoumiPay{
		GetPayMsg onPayGetPoints;
		GetPayMsg onInitGetmsg;
		#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
		extern "C"{
			// 支付回调
			jboolean Java_com_lexgame_youmipay_JNICocos2dShowMsg(JNIEnv *env,jobject thiz,jstring json)
			{
				/*
				【】使用CSContentJsonDictionary解析json
				getItemStringValue(const char *pszKey);
				pszKey      desc
				"Oid", "支付订单号"
				"TradeNo", "外部订单号"
				"Rid", "用户标记：" 
				"Status", "订单状态"
				"PayType", "支付通道"
				"Amount", "获得金币或道具卡"
				"RealMoney", "金额"
				"payTime", "支付时间"
				"CData", "自定义数据"
				*/
				onPayGetPoints(JniHelper::jstring2string(json));
			}
			// 初始化回调
			jboolean Java_com_lexgame_youmipay_JNIisInitSuccess(JNIEnv *env,jobject thiz, jstring json)
			{
				/*
				key			value
				true		空
				false		信息
				*/
				onInitGetmsg(JniHelper::jstring2string(json));
			}
		}

		//YOUMIPAY * YOUMIPAY::GETSHARED()
		//{
		//	STATIC YOUMIPAY * ME = NEW YOUMIPAY();
		//	RETURN ME;
		//}
		
		void initYoumiPay(const char* _appID, const char* _appSecret, GetPayMsg isInitSuccessFunc, bool _isSDKCallback)
		{
			LOGI("YoumiPayLex -- init");
			onInitGetmsg = isInitSuccessFunc;
			JniMethodInfo initInfo;
			bool isHave = JniHelper::getStaticMethodInfo(initInfo, "com/lexgame/youmipay/YoumiPay", "initYoumiPayStatic", "(Ljava/lang/String;Ljava/lang/String;Z)V");
			if(isHave)
			{
				LOGI("YoumiPayLex -- init found");
				jstring appID = Lex::JniUtils::stoJstring(initInfo.env ,_appID);
				jstring appSecret = Lex::JniUtils::stoJstring(initInfo.env, _appSecret);
				jboolean isSDKCallback = _isSDKCallback;
				initInfo.env->CallStaticVoidMethod(initInfo.classID, initInfo.methodID, appID, appSecret, isSDKCallback);
				LOGI("YoumiPayLex -- init end");
			}
			else
			{
				LOGI("YoumiPayLex -- not found");
			}
		}

		void createOrderAndPay(const char * _description, int _total, GetPayMsg _afterPay, const char * _tradeno, const char* _information)
		{
			onPayGetPoints = _afterPay;
			JniMethodInfo initInfo;
			bool isHave = JniHelper::getStaticMethodInfo(initInfo, "com/lexgame/youmipay/YoumiPay", "payBillStatic", "(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V");
			if(isHave)
			{
				LOGI("YoumiPayLex -- create found");
				jint total = _total;
				jstring description = Lex::JniUtils::stoJstring(initInfo.env ,_description);
				jstring tradeno = Lex::JniUtils::stoJstring(initInfo.env, _tradeno);
				jstring information = Lex::JniUtils::stoJstring(initInfo.env,_information);
				initInfo.env->CallStaticVoidMethod(initInfo.classID, initInfo.methodID, tradeno, description, total, information);
				LOGI("YoumiPayLex -- create end");
			}
			else
			{
				LOGI("YoumiPayLex -- create not found");
			}
		}
		#endif
	}
}