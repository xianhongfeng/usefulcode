﻿#include "MMIAP.h"
#include "JniUtils.h"
#include "platform/android/jni/JniHelper.h"
using namespace cocos2d;

namespace LexAndroid{
	namespace MMIAP{
		Pay::GetPayMsg afterPay;
		Pay::GetPayMsg afterInit;

		extern "C"
		{
			void Java_com_lexgame_mmIAP_MMIAPListener_InitFinishCallback(JNIEnv *env,jobject thiz,jstring json, jboolean result)
			{
				LOGI("Java_com_lexgame_mmIAP_InitFinishCallback");
				std::string str = JniHelper::jstring2string(json);
				afterInit(str.data(), result);
				LOGI("%s", str.c_str());

			}

			void Java_com_lexgame_mmIAP_MMIAPListener_BillingFinishCallback(JNIEnv *env,jobject thiz,jstring json, jboolean result)
			{
				LOGI("Java_com_lexgame_mmIAPListener_BillingFinishCallback");
				std::string str = JniHelper::jstring2string(json);
				LOGI("JniHelper::jstring2string(json) %s", str.c_str());
				LOGI("%d", afterPay);
				afterPay(str.data(), result);
				LOGI("afterpay");
				LOGI("111");
				LOGI("%s", str.c_str());
			}
		}

		#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
		void createOrder(const char* payCode, Pay::GetPayMsg _afterPay){
			afterPay = _afterPay;
			JniMethodInfo orderInfo;
			bool isHave = JniHelper::getStaticMethodInfo(orderInfo, "com/lexgame/mmIAP/MMIAP", 
				"createOrderStatic", "(Ljava/lang/String;)V");
			if(isHave)
			{
				LOGI("createOrderStatic -- found");
				jstring mpayCode = Lex::JniUtils::stoJstring(orderInfo.env, payCode);
				orderInfo.env->CallStaticVoidMethod(orderInfo.classID, orderInfo.methodID, mpayCode);
				LOGI("createOrderStatic -- end");
			}
			else
				LOGE("createOrderStatic -- not found");
		}

		void initMMIAP(const char * appID, const char *appKey, Pay::GetPayMsg _afterInit){
			afterInit = _afterInit;
			JniMethodInfo initInfo;
			bool isHave = JniHelper::getStaticMethodInfo(initInfo, "com/lexgame/mmIAP/MMIAP",
				"initMMIAPStatic", "(Ljava/lang/String;Ljava/lang/String;)V");
			if(isHave)
			{
				LOGI("initMMIAPStatic -- found");
				jstring mappID = Lex::JniUtils::stoJstring(initInfo.env, appID);
				jstring mappKey = Lex::JniUtils::stoJstring(initInfo.env, appKey);
				initInfo.env->CallStaticVoidMethod(initInfo.classID, initInfo.methodID, mappID, mappKey);
				LOGI("initMMIAPStatic -- end");
			}
			else
				LOGE("initMMIAPStatic -- not found");
		}
		#endif
	}
}