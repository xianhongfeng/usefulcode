#pragma once
#include "ThirdPay.h"

namespace LexAndroid{
	namespace MMIAP{
		extern void createOrder(const char* payCode, Pay::GetPayMsg _afterPay);

		extern void initMMIAP(const char * appID, const char *appKey, Pay::GetPayMsg _afterInit);
	}
}
