#include "ThirdPay.h"
#include "JniUtils.h"
//#include "mmiap/JniUtils.h"
#include "cocos2d.h"
#include <sstream>
#include "platform/android/jni/JniHelper.h"
#include "Audio.h"

USING_NS_CC;

namespace LexAndroid 
{
	namespace Pay
	{

		extern "C"
		{
			void Java_com_lexgame_cmgc_CMGC_setSoundNative(JNIEnv *env, jobject thiz, jboolean isplay)
			{
				if(isplay)
					return;
				bool sound = !g::audio.hasMusic();
				g::audio.setMusic(sound);
				g::audio.setSound(sound);
			}

			void Java_com_lexgame_cmgc_CMGC_afterBillingCBNative(JNIEnv *env, jobject thiz, jstring json, jboolean result)
			{
				//֧�����
				std::string str = JniHelper::jstring2string(json);
				Lex::ThreadHelper::TPMessage tpmsg;
				tpmsg.msg = str;
				tpmsg.isSuccess = result;
				Lex::ThreadHelper::CallBackTask cbt = {(int)ThirdPay::GCPay, tpmsg};
				Lex::ThreadHelper::addCBQueue(cbt);
				CCLog("cmgc after pay");
			}

			void Java_com_lexgame_cmgc_CMGC_afterSendScoreNative(JNIEnv *env, jobject thiz, jstring json, jboolean result)
			{
				//�ύ�������
				std::string str = JniHelper::jstring2string(json);
				Lex::ThreadHelper::TPMessage tpmsg;
				tpmsg.msg = str;
				tpmsg.isSuccess = result;
				Lex::ThreadHelper::CallBackTask cbt = {(int)ThirdPay::GCSendScore, tpmsg};
				Lex::ThreadHelper::addCBQueue(cbt);
			}

			void Java_com_lexgame_cmgc_CMGC_afterInitRankNative(JNIEnv *env, jobject thiz, jstring json, jboolean result)
			{
				//��ʼ�����а���
				std::string str = JniHelper::jstring2string(json);
				Lex::ThreadHelper::TPMessage tpmsg;
				tpmsg.msg = str;
				tpmsg.isSuccess = result;
				Lex::ThreadHelper::CallBackTask cbt = {(int)ThirdPay::GCSendScore, tpmsg};
				Lex::ThreadHelper::addCBQueue(cbt);
			}
		}

		PayCMGC * PayCMGC::getShared()
		{
			static PayCMGC * me = new PayCMGC();
			return me;
		}

		void PayCMGC::init(const char * key, const char *secret, const char *appId, GetPayMsg isInitSuccessFunc)
		{
			afterInit = isInitSuccessFunc;
			JniMethodInfo initInfo;
			bool isHave = JniHelper::getStaticMethodInfo(initInfo, "com/lexgame/cmgc/CMGC", "initRankStatic", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V");
			if(isHave)
			{
				jstring jkey = Lex::JniUtils::stoJstring(initInfo.env, key);
				jstring jsecret = Lex::JniUtils::stoJstring(initInfo.env, secret);
				jstring jappId = Lex::JniUtils::stoJstring(initInfo.env, appId);
				initInfo.env->CallStaticVoidMethod(initInfo.classID, initInfo.methodID, jkey, jsecret, jappId);
			}
			else
				CCLog("cmgc init not found");
		}

		void PayCMGC::createOrder(const char* billingIndex, GetPayMsg _afterPay, bool _useSms, bool _isRepeated, int _propsType, const char *  _cpParam)
		{
			afterPay = _afterPay;
			JniMethodInfo orderInfo;
			CCLog("cmgc createOrder before");

			bool isHave = JniHelper::getStaticMethodInfo(orderInfo, "com/lexgame/cmgc/CMGC", "doBillingStatic", "(ZZILjava/lang/String;Ljava/lang/String;)V");
			CCLog("cmgc createOrder ");

			if(isHave)
			{
				jstring jbillIndex = Lex::JniUtils::stoJstring(orderInfo.env, billingIndex);
				jstring jparam ;
				if(_cpParam != NULL)
				{
					jparam = Lex::JniUtils::stoJstring(orderInfo.env, _cpParam);
				}
				else
					jparam = NULL;
				orderInfo.env->CallStaticVoidMethod(orderInfo.classID, orderInfo.methodID, _useSms, _isRepeated, _propsType, jbillIndex, jparam);
			}
			else
				CCLog("cmgc createOrder not found");
		}

		void PayCMGC::sendScore(int64_t score, GetPayMsg _afterSend)
		{
			afterSend = _afterSend;
			JniMethodInfo sendInfo;
			std::stringstream stream;
			bool isHave = JniHelper::getStaticMethodInfo(sendInfo, "com/lexgame/cmgc/CMGC", "sendScoreStatic", "(Ljava/lang/String;)V");
			if(isHave)
			{
				char * cscore;
				stream << score;
				stream >> cscore;
				jstring jscore = Lex::JniUtils::stoJstring(sendInfo.env, cscore);
				sendInfo.env->CallStaticVoidMethod(sendInfo.classID, sendInfo.methodID, cscore);
			}
			else
				CCLog("cmgc sendScore not found");
		}

		void PayCMGC::showRank()
		{
			JniMethodInfo srInfo;
			bool isHave = JniHelper::getStaticMethodInfo(srInfo, "com/lexgame/cmgc/CMGC", "showRankStatic", "()V");
			if(isHave)
			{
				srInfo.env->CallStaticVoidMethod(srInfo.classID, srInfo.methodID);
			}
		}

		void PayCMGC::runAfterInit(const char* str, bool success)
		{
			afterInit(str, success);
		}

		void PayCMGC::runAfterPay(const char * str, bool success)
		{
			afterPay(str, success);
		}

		void PayCMGC::runAfterSendScore(const char* str, bool success)
		{
			afterSend(str, success);
		}
	}
}
