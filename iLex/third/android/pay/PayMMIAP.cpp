﻿#include "ThirdPay.h"
//#include "mmiap/MMIAP.h"
#include "JniUtils.h"
#include "platform/android/jni/JniHelper.h"

using namespace cocos2d;

namespace LexAndroid
{
	namespace Pay
	{
		extern "C"
		{
			void Java_com_lexgame_mmIAP_MMIAPListener_InitFinishCallback(JNIEnv *env,jobject thiz,jstring json, jboolean result)
			{
				LOGI("Java_com_lexgame_mmIAP_InitFinishCallback");
				std::string str = JniHelper::jstring2string(json);
				//PayMMIAP::getShared()->afterInit(str.data(), result);
				Lex::ThreadHelper::TPMessage tpmsg;
				tpmsg.isSuccess = result;
				tpmsg.msg = str;
				Lex::ThreadHelper::CallBackTask cbt = {(int)ThirdPay::MMIAPinit, tpmsg};
				Lex::ThreadHelper::addCBQueue(cbt);
				LOGI("%s", str.c_str());

			}

			void Java_com_lexgame_mmIAP_MMIAPListener_BillingFinishCallback(JNIEnv *env,jobject thiz,jstring json, jboolean result)
			{
				LOGI("Java_com_lexgame_mmIAPListener_BillingFinishCallback");
				std::string str = JniHelper::jstring2string(json);
				LOGI("JniHelper::jstring2string(json) %s", str.c_str());
				Lex::ThreadHelper::TPMessage tpmsg;
				tpmsg.isSuccess = result;
				tpmsg.msg = str;
				Lex::ThreadHelper::CallBackTask cbt = {(int)ThirdPay::MMIAPPay, tpmsg};
				Lex::ThreadHelper::addCBQueue(cbt);
				
				str = tpmsg.msg;
				LOGI("afterpay tpmsg %s", str.c_str());
				LOGI("%s", str.c_str());
			}
		}

		PayMMIAP * PayMMIAP::getShared()
		{
			static PayMMIAP * me = new PayMMIAP();
			return me;
		}

		//void PayMMIAP::init(const char * appID, const char *appKey, GetPayMsg isInitSuccessFunc)
		//{
		//	LexAndroid::MMIAP::initMMIAP(appID, appKey, isInitSuccessFunc);
		//}

		//void PayMMIAP::createOrder(const char* payCode, GetPayMsg _afterPay)
		//{
		//	LexAndroid::MMIAP::createOrder(payCode, _afterPay);
		//}

		void PayMMIAP::createOrder(const char* payCode, GetPayMsg _afterPay){
			afterPay = _afterPay;
			JniMethodInfo orderInfo;
			bool isHave = JniHelper::getStaticMethodInfo(orderInfo, "com/lexgame/mmIAP/MMIAP", 
				"createOrderStatic", "(Ljava/lang/String;)V");
			if(isHave)
			{
				LOGI("createOrderStatic -- found");
				jstring mpayCode = Lex::JniUtils::stoJstring(orderInfo.env, payCode);
				orderInfo.env->CallStaticVoidMethod(orderInfo.classID, orderInfo.methodID, mpayCode);
				LOGI("createOrderStatic -- end");
			}
			else
				LOGE("createOrderStatic -- not found");
		}

		void PayMMIAP::init(const char * appID, const char *appKey, GetPayMsg _afterInit){
			afterInit = _afterInit;
			JniMethodInfo initInfo;
			bool isHave = JniHelper::getStaticMethodInfo(initInfo, "com/lexgame/mmIAP/MMIAP",
				"initMMIAPStatic", "(Ljava/lang/String;Ljava/lang/String;)V");
			if(isHave)
			{
				LOGI("initMMIAPStatic -- found");
				jstring mappID = Lex::JniUtils::stoJstring(initInfo.env, appID);
				jstring mappKey = Lex::JniUtils::stoJstring(initInfo.env, appKey);
				initInfo.env->CallStaticVoidMethod(initInfo.classID, initInfo.methodID, mappID, mappKey);
				LOGI("initMMIAPStatic -- end");
			}
			else
				LOGE("initMMIAPStatic -- not found");
		}

		void PayMMIAP::runAfterInit(const char* str, bool success)
		{
			afterInit(str, success);
		}

		void PayMMIAP::runAfterPay(const char* str, bool success)
		{
			afterPay(str, success);
		}
	}
}
