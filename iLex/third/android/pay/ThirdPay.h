#pragma once
#include "ThreadHelper.h"
#include <iostream>
namespace LexAndroid
{
	namespace Pay
	{
		enum class ThirdPay
		{
			YoumiInit = 0, YoumiPay, MMIAPinit, MMIAPPay, MM360Pay, GCRankInit, GCSendScore, GCPay
		};

		typedef void (*GetPayMsg)(const char* json, bool success);

		class PayYoumi{
			PayYoumi(){}
		public:
			static PayYoumi * getShared();
			virtual void init(const char* _appID, const char* _appSecret, GetPayMsg isInitSuccessFunc, bool _isSDKCallback = true);
			virtual void createOrder(const char * _description, int _total, GetPayMsg _afterPay, const char * _tradeno = "", const char* _information = "");
			virtual void runAfterInit();
			virtual void runAfterPay();
		};

		class PayMMIAP{
			PayMMIAP(){}
		public:
			//Lex::ThreadHelper::TPMessage tpmsg;
			GetPayMsg afterInit;
			GetPayMsg afterPay;
			static PayMMIAP * getShared();
			virtual void init(const char * appID, const char *appKey, GetPayMsg isInitSuccessFunc);
			virtual void createOrder(const char* payCode, GetPayMsg _afterPay);
			virtual void runAfterInit(const char* str, bool success);
			virtual void runAfterPay(const char* str, bool success);
		};

		class PayCMGC{
			PayCMGC(){};
		public:
			//Lex::ThreadHelper::TPMessage tpmsg;
			GetPayMsg afterInit;
			GetPayMsg afterPay;
			GetPayMsg afterSend;
			static PayCMGC * getShared();
			virtual void init(const char * key, const char *secret, const char *appId, GetPayMsg isInitSuccessFunc);
			virtual void createOrder(const char* billingIndex, GetPayMsg _afterPay, bool _useSms = true, bool _isRepeated = true, int _propsType = 2, const char *  _cpParam = NULL);
			virtual void sendScore(int64_t score, GetPayMsg _afterSend);
			virtual void showRank();
			virtual void runAfterSendScore(const char* str, bool success);
			virtual void runAfterInit(const char* str, bool success);
			virtual void runAfterPay(const char* str, bool success);
		};

		class PayCM360
		{
		public:
			GetPayMsg afterInit;
			GetPayMsg afterPay;
			static PayCM360 * getShared();
			virtual void init(const char * appID, const char *appKey, GetPayMsg isInitSuccessFunc);
			virtual void createOrder(const char* payCode, char* payCost, GetPayMsg _afterPay);
			virtual void runAfterInit(const char* str, bool success);
			virtual void runAfterPay(const char* str, bool success);
		};
		// called from main thread
		void checkCBQueue();
	}
}
