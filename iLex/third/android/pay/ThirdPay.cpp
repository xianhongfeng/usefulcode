#include "ThirdPay.h"
#include "JniUtils.h"

namespace LexAndroid
{
	namespace Pay
	{
		void checkCBQueue()
		{
			//LOGI("checkCBQueue");
			while (Lex::ThreadHelper::sizeCBQueue() > 0)
			{
				Lex::ThreadHelper::CallBackTask cbt = Lex::ThreadHelper::popCBQueue();
				std::string str;
				str = cbt.tpmsg.msg;
				LOGI("%s", str.c_str());
				switch (cbt.key)
				{
				/*case (int)ThirdPay::YoumiInit:
					PayYoumi::getShared()->runAfterInit();
				case (int)ThirdPay::YoumiPay:
					PayYoumi::getShared()->runAfterPay();*/
				case (int)ThirdPay::MMIAPinit:
					LOGI("checkCBQueue MMIAPinit %s",  str.c_str());
					PayMMIAP::getShared()->runAfterInit(cbt.tpmsg.msg.c_str(), cbt.tpmsg.isSuccess);
					break;
				case (int)ThirdPay::MMIAPPay:
					LOGI("checkCBQueue MMIAPPay %s", str.c_str());
					PayMMIAP::getShared()->runAfterPay(cbt.tpmsg.msg.c_str(), cbt.tpmsg.isSuccess);
					break;
				case (int)ThirdPay::GCRankInit:
					PayCMGC::getShared()->runAfterInit(cbt.tpmsg.msg.c_str(), cbt.tpmsg.isSuccess);
					break;
				case (int)ThirdPay::GCPay:
					PayCMGC::getShared()->runAfterPay(cbt.tpmsg.msg.c_str(), cbt.tpmsg.isSuccess);
					break;
				case (int)ThirdPay::GCSendScore:
					PayCMGC::getShared()->runAfterSendScore(cbt.tpmsg.msg.c_str(), cbt.tpmsg.isSuccess);
					break;
				case (int)ThirdPay::MM360Pay:
					PayCM360::getShared()->runAfterPay(cbt.tpmsg.msg.c_str(), cbt.tpmsg.isSuccess);
					break;
				default:
					break;
				}
			}
		}
	}
}
