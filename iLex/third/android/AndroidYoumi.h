#pragma once
#include "cocos2d.h"
#include "AdWall.h"
using namespace cocos2d;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include <jni.h>
#include "JniUtils.h"
#include "platform/android/jni/JniHelper.h"

#endif

namespace LexAndroid{
	namespace YoumiAdwall{
		extern OnAdWallGetPoint afterConsumption;
		//初始化
		extern bool initialize(const char* publishID, const char* privateKey, bool isTestMode = false);
		extern bool show();
		extern void checkPoints(OnAdWallGetPoint onGet);
		//插屏广告
		extern void showSpotAd();
		//积分Banner
		extern void showOfferBanner();
		//关闭积分banner
		extern void hideOfferBanner();
		//积分对话框
		extern void showOfferDialog();
		//积分墙
		extern void showOfferWall();
		//SmartBanner
		//static void showSmartBanner();
		//展示Banner
		extern void showBanner();
		//关闭banner
		extern void hideBanner();
		//增加积分
		extern void awardPoints( int point );
		//减少积分
		extern void spendPoints( int point );
		//查询积分
		extern int queryPoints();

	}
}
