﻿//
//  Wechat.h
//  Shuihuzhuan
//
//  Created by leaving on 14-4-21.
//
//

#ifndef __Shuihuzhuan__Wechat__
#define __Shuihuzhuan__Wechat__

#include <iostream>

typedef void (*WechatCallBack)(bool result);

enum class WechatScene {
    
    WXSceneSession  = 0,        /**< 聊天界面    */
    WXSceneTimeline = 1,        /**< 朋友圈      */
    WXSceneFavorite = 2,        /**< 收藏       */
};
class Wechat
{
	Wechat(){}
public:
	static Wechat * getShared();
	void initialize(const char * iden, WechatCallBack onPublishOK);
	bool sendTextContent(const char * text, WechatScene toScene);
	bool sendImageContent(const char * imagePath, const char * thumbImagePath, WechatScene toScene);
	bool sendLinkContent(const char *link, const char * withText, const char * thumbPath, WechatScene toScene);
	bool isWechatInstalled();
};
#endif /* defined(__Shuihuzhuan__Wechat__) */
