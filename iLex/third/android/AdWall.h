//
//  AdWall.h
//  ZhongGuoLong
//
//  Created by leaving on 14-4-17.
//
//

#pragma once
#include <cstdint>
enum class EAdWallType
{
    NONE = 0, Limei = 1, Youmi = 2, Adwo = 3, DianRu, Count,
};
typedef EAdWallType AdWallClass;

typedef void (*OnAdWallGetPoint)(int64_t, EAdWallType from);

class Mixad
{
    Mixad() {}
public:
    static Mixad * getShared();
    void initialize(const char * iden);
    bool show();
};

class AdWallBase
{
public:
    virtual bool show() = 0;
    virtual void checkPoints(OnAdWallGetPoint onGet) =0;
};

class AdWallAdwo: public AdWallBase
{
    AdWallAdwo(){}
public:
    static AdWallAdwo * getShared();
    bool initialize(const char * iden);
    virtual bool show();
    virtual void checkPoints(OnAdWallGetPoint onGet);
};
class AdWallLimei: public AdWallBase
{
    AdWallLimei(){}
public:
    static AdWallLimei * getShared();
    bool initialize(const char * iden);
    virtual bool show();
    virtual void checkPoints(OnAdWallGetPoint onGet);
};
class AdWallYoumi: public AdWallBase
{
    AdWallYoumi() {}
public:
    static AdWallYoumi * getShared();
    bool initialize(const char * iden, char const * code);
    virtual bool show();
    virtual void checkPoints(OnAdWallGetPoint onGet);
    void showAd();
};

class AdWallDianRu : public AdWallBase
{
    AdWallDianRu(){}
public:
    static AdWallDianRu * getShared();
    bool initialize(const char * iden);
    virtual bool show();
    virtual void checkPoints(OnAdWallGetPoint onGet);
};