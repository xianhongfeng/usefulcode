#include "AndroidYoumi.h"

//AdWallYoumi::~AdWallYoumi(){}

namespace LexAndroid{
	namespace YoumiAdwall{
		extern "C"
		{
			jint Java_com_lexgame_youmi_YoumiAdwall_pointsBalanceChange( JNIEnv *env,jobject thiz,jint point )
			{
				//do sth.
				//CCLog("积分更改，现剩余： %d\n",point);
				LexAndroid::YoumiAdwall::afterConsumption(point, (AdWallClass)2);
				return 0;
			}
		}
		//AdWallYoumi * AdWallYoumi::getShared()
		//{
		//	static AdWallYoumi * me = new AdWallYoumi();
		//	return me;
		//}
		OnAdWallGetPoint afterConsumption;
		bool show()
		{
			showOfferWall();
		}

		void checkPoints(OnAdWallGetPoint onGet)
		{
			afterConsumption = onGet;	
		}

		bool initialize(const char* publishID, const char* privateKey, bool isTestMode)
		{
		#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
			JniMethodInfo initInfo;
			bool isHave = JniHelper::getStaticMethodInfo(initInfo, "com/lexgame/youmi/YoumiAdwall", "initYoumiStatic", "(Ljava/lang/String;Ljava/lang/String;Z)V");
			if(!isHave) {
				LOGI("jni:initYoumiStatic not found");
			}else{
				jstring _publishID = Lex::JniUtils::stoJstring(initInfo.env, publishID);
				//LOGI("jstring success");
				jstring _temp = _publishID;
				jstring _privateKey = Lex::JniUtils::stoJstring(initInfo.env, privateKey);
				//LOGI("publishID %s", (JniHelper::jstring2string(_temp)).data());
				jboolean _isTestMode = isTestMode;
				initInfo.env->CallStaticVoidMethod(initInfo.classID, initInfo.methodID, _publishID, _privateKey, _isTestMode);
				//LOGI("jni:initYoumiStatic end");
			}
		#endif
		}

		void showSpotAd()
		{
		#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
			JniMethodInfo showSpotAd;
			bool isHave = JniHelper::getStaticMethodInfo(showSpotAd,"com/lexgame/youmi/YoumiAdwall","showSpotAdStatic", "()V"); 
 
			if (!isHave) {
				CCLog("jni:showSpotAdStatic此函数不存在");
			}else{
				CCLog("jni:showSpotAdStatic此函数存在");
				showSpotAd.env->CallStaticVoidMethod(showSpotAd.classID, showSpotAd.methodID);
			}
		#endif
		}

		void showBanner()
		{
		#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
			JniMethodInfo showBanner;
			bool isHave = JniHelper::getStaticMethodInfo(showBanner,"com/lexgame/youmi/YoumiAdwall","showBannerStatic", "()V"); 
 
			if (!isHave) {
				CCLog("jni:showBannerStatic此函数不存在");
			}else{
				CCLog("jni:showBannerStatic此函数存在");
				showBanner.env->CallStaticVoidMethod(showBanner.classID, showBanner.methodID);
			}
		#endif

		}

		void hideBanner()
		{
		#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
			JniMethodInfo hideBanner;
			bool isHave = JniHelper::getStaticMethodInfo(hideBanner,"com/lexgame/youmi/YoumiAdwall","hideBannerStatic", "()V"); 
 
			if (!isHave) {
				CCLog("jni:hideBannerStatic此函数不存在");
			}else{
				CCLog("jni:hideBannerStatic此函数存在");
				hideBanner.env->CallStaticVoidMethod(hideBanner.classID, hideBanner.methodID);
			}
		#endif

		}


		void showOfferBanner()
		{
		#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
			JniMethodInfo showOfferBanner;
			bool isHave = JniHelper::getStaticMethodInfo(showOfferBanner,"com/lexgame/youmi/YoumiAdwall","showOfferBannerStatic", "()V"); 
 
			if (!isHave) {
				CCLog("jni:showOfferBannerStatic此函数不存在");
			}else{
				CCLog("jni:showOfferBannerStatic此函数存在");
				showOfferBanner.env->CallStaticVoidMethod(showOfferBanner.classID, showOfferBanner.methodID);
			}
		#endif

		}

		void hideOfferBanner()
		{
		#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
			JniMethodInfo hideOfferBanner;
			bool isHave = JniHelper::getStaticMethodInfo(hideOfferBanner,"com/lexgame/youmi/YoumiAdwall","hideOfferBannerStatic", "()V"); 
 
			if (!isHave) {
				CCLog("jni:hideOfferBannerStatic此函数不存在");
			}else{
				CCLog("jni:hideOfferBannerStatic此函数存在");
				hideOfferBanner.env->CallStaticVoidMethod(hideOfferBanner.classID, hideOfferBanner.methodID);
			}
		#endif

		}


		void showOfferDialog()
		{
		#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
			JniMethodInfo showOfferDialog;
			bool isHave = JniHelper::getStaticMethodInfo(showOfferDialog,"com/lexgame/youmi/YoumiAdwall","showOfferDialogStatic", "()V"); 
 
			if (!isHave) {
				CCLog("jni:showOfferDialogStatic此函数不存在");
			}else{
				CCLog("jni:showOfferDialogStatic此函数存在");
				showOfferDialog.env->CallStaticVoidMethod(showOfferDialog.classID, showOfferDialog.methodID);
			}
		#endif
		}


		void showOfferWall()
		{
		#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
			JniMethodInfo showOfferWall;
			bool isHave = JniHelper::getStaticMethodInfo(showOfferWall,"com/lexgame/youmi/YoumiAdwall","showOfferWallStatic", "()V"); 
 
			if (!isHave) {
				LOGI("jni:showOfferWallStatic not found");
			}else{
				LOGI("jni:showOfferWallStatic found");
				showOfferWall.env->CallStaticVoidMethod(showOfferWall.classID, showOfferWall.methodID);
			}
		#endif

		}

		void awardPoints( int point )
		{
		#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
			JniMethodInfo awardPoints;
			bool isHave = JniHelper::getStaticMethodInfo(awardPoints,"com/lexgame/youmi/YoumiAdwall","awardPoints", "(I)V"); 

			if (!isHave) {
				CCLog("jni:awardPoints此函数不存在");
			}else{
				CCLog("jni:awardPoints此函数存在");

				awardPoints.env->CallStaticVoidMethod(awardPoints.classID, awardPoints.methodID, point);
			}
		#endif
		}

		void spendPoints( int point )
		{
		#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
			JniMethodInfo spendPoints;
			bool isHave = JniHelper::getStaticMethodInfo(spendPoints,"com/lexgame/youmi/YoumiAdwall","spendPoints", "(I)V"); 
 
			if (!isHave) {
				CCLog("jni:spendPoints此函数不存在");
			}else{
				CCLog("jni:spendPoints此函数存在");
				spendPoints.env->CallStaticVoidMethod(spendPoints.classID, spendPoints.methodID,point);
			}
		#endif
		}

		int queryPoints()
		{
		#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
			JniMethodInfo queryPoints;
			bool isHave = JniHelper::getStaticMethodInfo(queryPoints,"com/lexgame/youmi/YoumiAdwall","queryPointsStatic", "()I"); 
 
			if (!isHave) {
				CCLog("jni:queryPointsStatic此函数不存在");
			}else{
				CCLog("jni:queryPointsStatic此函数存在");
				return queryPoints.env->CallStaticIntMethod(queryPoints.classID, queryPoints.methodID);
			}
		#endif
		}
	}
}