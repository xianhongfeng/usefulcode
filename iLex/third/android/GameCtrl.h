﻿//
//  GameCtrl.h
//  Shuihuzhuan
//
//  Created by leaving on 14-8-4.
//
//

#pragma once

#include <string>
#include <vector>

class GameCtrl {
protected:
	bool bIsDisable;
	bool bShowRate;
	bool bUseWechat;
	bool bUseMoregames;
	bool bUseFullAd;
	bool bGoldMore;
	bool bPropMore;
	bool bUseAdWall;
	
	bool bTestServer;
	
	std::vector<std::string> mAdWallName;
	std::string mFullAdName;
	GameCtrl();
public:
	static GameCtrl * getShared();
	void initialize(const char * url);
	void initializeWithConfig(const char * str);
	bool isTestServer() { return bTestServer; }
	bool shouldShowRateTips();
	bool shouldShowWechatShareTips();
	bool shouldShowMoregames();
	bool shouldShowFullAd();
	bool canGetMoreGold();
	bool canGetMoreProp();
	bool hasAdWall();
	std::string getAdWallName(int nth);
	std::string getFullAdName();
};
