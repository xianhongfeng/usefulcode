#include "Wechat.h"
#include <iostream>
#include <stdint.h>

using namespace std;

Wechat * Wechat::getShared()
{
	static Wechat *me = new Wechat();
	return me;
}
void Wechat::initialize(const char * iden, WechatCallBack onPublishOK)
{
	 
}
bool Wechat::sendTextContent(const char * text, WechatScene toScene)
{
	return false;
}
bool Wechat::sendImageContent(const char * imagePath, const char * thumbImagePath, WechatScene toScene)
{
	return false;
}
bool Wechat::sendLinkContent(const char *link, const char * withText, const char * thumbPath, WechatScene toScene)
{
	return false;
}
bool Wechat::isWechatInstalled()
{
	return false;
}