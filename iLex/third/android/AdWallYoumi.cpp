#include "AdWall.h"
#include "AndroidYoumi.h"

AdWallYoumi * AdWallYoumi::getShared()
{
	static AdWallYoumi * me = new AdWallYoumi();
	return me;
}
bool AdWallYoumi::initialize(const char * iden, char const * code)
{
	LexAndroid::YoumiAdwall::initialize(iden, code);
    return true;
}
bool AdWallYoumi::show()
{
	LexAndroid::YoumiAdwall::showOfferWall();
    return true;
}
void AdWallYoumi::checkPoints(OnAdWallGetPoint onGet)
{
	LexAndroid::YoumiAdwall::checkPoints(onGet);
	LexAndroid::YoumiAdwall::spendPoints(LexAndroid::YoumiAdwall::queryPoints());
}
void AdWallYoumi::showAd()
{
	LexAndroid::YoumiAdwall::showOfferBanner();
}