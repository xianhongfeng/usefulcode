//
//  LexInMobi.cpp
//  Ludingjiv0
//
//  Created by pro on 15/3/25.
//
//

#include "LexInMobi.h"
#import <UIKit/UIKit.h>
#import "ImBanner.h"
#import "InMobi.h"
#import "IMInterstitial.h"
#import "RootViewController.h"
#include <string>

@interface InMobiDelegate : NSObject <IMInterstitialDelegate>

+(InMobiDelegate*)getShared;

@end

@interface InMobiDelegate ()

@property(nonatomic, copy) NSString * m_appId;

@end

@implementation InMobiDelegate
+(InMobiDelegate*)getShared
{
    static InMobiDelegate * dele = [InMobiDelegate new];
    return dele;
}

-(void)InMobiInitialize:(NSString*)pubId
{
//    [InMobi setLogLevel:IMLogLevelDebug];
    [InMobi initialize:pubId];
    self.m_appId = pubId;
}

- (void)interstitialDidReceiveAd:(IMInterstitial *)ad
{
}

- (void)interstitial:(IMInterstitial *)ad didFailToReceiveAdWithError:(IMError *)error
{
    NSString *errorMessage = [NSString stringWithFormat:@"Error code: %d, message: %@", [error code], [error localizedDescription]];
    NSLog(@"%@", errorMessage);
}

- (void)interstitialDidDismissScreen:(IMInterstitial *)ad
{
    Lex::InMobi::getShared()->initialize([self.m_appId UTF8String]);
}

@end

namespace Lex
{
    static IMInterstitial * interstitial = nil;
    std::string m_id = "";
    InMobi * InMobi::getShared()
    {
        static InMobi * mobi = new InMobi();
        return mobi;
    }
    
    void InMobi::initialize(const char * InMobi_appId)
    {
        m_id = InMobi_appId;
        NSString * appid = [NSString stringWithUTF8String:InMobi_appId];
        [[InMobiDelegate getShared] InMobiInitialize:appid];
        if (interstitial) [interstitial release];
        interstitial = [[IMInterstitial alloc] initWithAppId:appid];
        [interstitial setDelegate:[InMobiDelegate getShared]];
        [interstitial loadInterstitial];
    }
    
    bool InMobi::showAd()
    {
        switch (interstitial.state)
        {
            case kIMInterstitialStateUnknown :
            {
                NSLog(@"====kIMInterstitialStateUnknown");
                initialize(m_id.c_str());
                break;
            }
                
            case kIMInterstitialStateInit :
            {
                NSLog(@"====kIMInterstitialStateInit");
                [interstitial loadInterstitial];
                break;
            }
                
            case kIMInterstitialStateLoading :
            {
                NSLog(@"====kIMInterstitialStateLoading");
                [interstitial stopLoading];
                [interstitial loadInterstitial];
                break;
            }
                
            case kIMInterstitialStateReady :
            {
                NSLog(@"====kIMInterstitialStateReady");
                [interstitial presentInterstitialAnimated:YES];
                return true;
            }
                
            case kIMInterstitialStateActive :
            {
                NSLog(@"====kIMInterstitialStateActive");
                initialize(m_id.c_str());
                break;
            }
        }
    
        return false;
    }
}