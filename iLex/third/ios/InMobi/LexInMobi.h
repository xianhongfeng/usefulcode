//
//  LexInMobi.h
//  Ludingjiv0
//
//  Created by pro on 15/3/25.
//
//

#pragma once

namespace Lex
{
    class InMobi
    {
    public:
        static InMobi * getShared();
        void initialize(const char * InMobi_appId);
        bool showAd();
    };
}
