//
//  Youmen.mm
//  SimpleGameAd
//
//  Created by leaving on 2/27/14.
//
//

#include "MobClick.h"
#include "LexAdYoumen.h"

namespace Lex
{
    Youmen * Youmen::getShared()
    {
        static Youmen * me = new Youmen;
        return me;
    }
    void Youmen::initialize(char const * iden)
    {
        //    [MobClick setCrashReportEnabled:NO]; // 如果不需要捕捉异常，注释掉此行
        [MobClick setLogEnabled:YES];  // 打开友盟sdk调试，注意Release发布时需要注释掉此行,减少io消耗
        [MobClick setAppVersion:XcodeAppVersion]; //参数为NSString * 类型,自定义app版本信息，如果不设置，默认从CFBundleVersion里取
        //
        [MobClick startWithAppkey:[NSString stringWithUTF8String:iden] reportPolicy:(ReportPolicy) BATCH channelId:nil];
        //   reportPolicy为枚举类型,可以为 REALTIME, BATCH,SENDDAILY,SENDWIFIONLY几种
        //   channelId 为NSString * 类型，channelId 为nil或@""时,默认会被被当作@"App Store"渠道
        
        //      [MobClick checkUpdate];   //自动更新检查, 如果需要自定义更新请使用下面的方法,需要接收一个(NSDictionary *)appInfo的参数
        //    [MobClick checkUpdateWithDelegate:self selector:@selector(updateMethod:)];
        
        [MobClick updateOnlineConfig];  //在线参数配置
        
        //    1.6.8之前的初始化方法
        //    [MobClick setDelegate:self reportPolicy:REALTIME];  //建议使用新方法
        [[NSNotificationCenter defaultCenter] addObserver:nil selector:nil name:UMOnlineConfigDidFinishedNotification object:nil];
        
    }

}