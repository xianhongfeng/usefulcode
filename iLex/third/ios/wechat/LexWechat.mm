//
//  Wechat.cpp
//  Shuihuzhuan
//
//  Created by leaving on 14-4-21.
//
//

#include "LexWechat.h"
#import "AppController.h"


@interface AppController(WX)
@end

static bool iswx_ok = false;
static WXScene wx_sceen;
static Lex::WechatCallBack wx_onResult;

@implementation AppController(WX)

- (void) initWechat:(NSString *)iden withDescription:(NSString *)desc
{
	if(iswx_ok) return;
    //向微信注册
	
	iswx_ok = [WXApi registerApp:iden withDescription:desc];
    wx_sceen = WXSceneTimeline;
}
-(void) onResp:(BaseResp*)resp
{
    if([resp isKindOfClass:[SendMessageToWXResp class]])
    {
		wx_onResult(resp.errCode == 0);
#if 0
        NSString *strTitle = [NSString stringWithFormat:@"发送媒体消息结果"];
        NSString *strMsg = [NSString stringWithFormat:@"errcode:%d", resp.errCode];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:strTitle message:strMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
#endif
    }
}

- (BOOL) sendTextContent:(NSString*)text toScene:(WXScene)scene
{
    SendMessageToWXReq* req = [[[SendMessageToWXReq alloc] init]autorelease];
    req.text = text;
    req.bText = YES;
    req.scene = scene;
    
    return [WXApi sendReq:req];
}


- (BOOL) sendImageContent:(NSString*)imagePath thumb:(NSString*)thumbImagePath toScene:(WXScene)scene
{
    WXMediaMessage *message = [WXMediaMessage message];
    
    WXImageObject *ext = [WXImageObject object];
    ext.imageData = [NSData dataWithContentsOfFile:imagePath];
    
    UIImage* image = [UIImage imageWithData:ext.imageData];
    ext.imageData = UIImageJPEGRepresentation(image, 0.5f);
  
    [message setThumbImage:[UIImage imageWithData:[NSData dataWithContentsOfFile:thumbImagePath]]];
    message.mediaObject = ext;
    
    SendMessageToWXReq* req = [[[SendMessageToWXReq alloc] init]autorelease];
    req.bText = NO;
    req.message = message;
    req.scene = scene;
    
    return[WXApi sendReq:req];
}

- (BOOL) sendLinkContent:(NSString*)link withText:(NSString*)text thumb:(NSString *)filePath  toScene:(WXScene)scene
{
    WXMediaMessage *message = [WXMediaMessage message];
    message.title = text;
    message.description = @"";
    [message setThumbImage:[UIImage imageWithData:[NSData dataWithContentsOfFile:filePath]]];
    
    WXWebpageObject *ext = [WXWebpageObject object];
	ext.webpageUrl = link;
	
    message.mediaObject = ext;
    
    SendMessageToWXReq* req = [[[SendMessageToWXReq alloc] init]autorelease];
    req.bText = NO;
    req.message = message;
    req.scene = scene;
    
    return [WXApi sendReq:req];
}
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    return  [WXApi handleOpenURL:url delegate:self];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if(wx_onResult != nil)
    {
        BOOL isSuc = [WXApi handleOpenURL:url delegate:self];
		if(!isSuc)
		{
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"无法分享" message:@"无法通过微信进行分享，请检查您是否安装了微信" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[alert show];
			[alert release];
		}
    }
    return YES;
}

@end

namespace Lex
{
    Wechat * Wechat::getShared()
    {
        static Wechat * me = new Wechat;
        return me;
    }
    
    void Wechat::initialize(const char * iden, WechatCallBack onPublishOK)
    {
        wx_onResult = onPublishOK;
        [[AppController appControllerLastCreated] initWechat:[NSString stringWithUTF8String:iden] withDescription:@"game shared"];
    }
    bool Wechat::sendTextContent(const char * text, WechatScene toScene)
    {
        return [[AppController appControllerLastCreated] sendTextContent:[NSString stringWithUTF8String:text]
                                                                 toScene:(WXScene)(int)toScene];
    }
    bool Wechat::sendImageContent(const char * imagePath, const char * thumbImagePath, WechatScene toScene)
    {
        return [[AppController appControllerLastCreated] sendImageContent:[NSString stringWithUTF8String:imagePath] thumb:[NSString stringWithUTF8String:thumbImagePath] toScene:(WXScene)toScene];
    }
    bool Wechat::sendLinkContent(const char *link, const char * withText, const char * thumbPath, WechatScene toScene)
    {
        return [[AppController appControllerLastCreated] sendLinkContent:[NSString stringWithUTF8String:link] withText:[NSString stringWithUTF8String:withText] thumb:[NSString stringWithUTF8String:thumbPath] toScene:(WXScene)toScene];
    }
    bool Wechat::isWechatInstalled()
    {
        return [WXApi isWXAppInstalled] == YES;
    }
}