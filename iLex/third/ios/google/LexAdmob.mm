//
//  gma.cpp
//  ShuihuzhuanAries
//
//  Created by leaving on 14-10-9.
//
//

#include "LexAdmob.h"
#import <UIKit/UIKit.h>
#include <string>
#import <GoogleMobileAds/GoogleMobileAds.h>
#include "stdint.h"
namespace Lex
{
	static GADInterstitial *interstitial_ = nil;
    static std::string m_id;
	GAd * GAd::getShared()
	{
		static GAd * gad = new GAd();
		return gad;
	}
	void GAd::initialize(const char * adid)
	{
        m_id = adid;
		interstitial_ = [[GADInterstitial alloc] init];
		interstitial_.adUnitID = [NSString stringWithUTF8String:adid];
		[interstitial_ loadRequest:[GADRequest request]];
	}
	bool GAd::showAd()
	{
        [interstitial_ presentFromRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
        interstitial_ = [[GADInterstitial alloc] init];
        interstitial_.adUnitID = [NSString stringWithUTF8String:m_id.data()];
        [interstitial_ loadRequest:[GADRequest request]];
		return true;
	}
};