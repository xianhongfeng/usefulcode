//
//  TDAdTracking.m
//  testSHZ
//
//  Created by LexCo on 3/13/15.
//
//

#import <Foundation/Foundation.h>
#include "TDAdTracking.h"
#include "TalkingDataAppCpa.h"

TDAdTracking * TDAdTracking::getShared()
{
    static TDAdTracking * me = new TDAdTracking;
    return me;
}

void TDAdTracking::initialize(char const * iden, const char * channel)
{
    [TalkingDataAppCpa init: [NSString stringWithUTF8String:iden] withChannelId: [NSString stringWithUTF8String:channel]];
}

void TDAdTracking::onRegister(const char * userId)
{
    [TalkingDataAppCpa onRegister: [NSString stringWithUTF8String:userId] ];
}

void TDAdTracking::onLogin(const char * userId)
{
    [TalkingDataAppCpa onLogin : [NSString stringWithUTF8String:userId]];
}

void TDAdTracking::onPay(const char * account, const char * orderId, int amount, const char * currencyType, const char * payType)
{
    [TalkingDataAppCpa onPay: [NSString stringWithUTF8String:account] withOrderId: [NSString stringWithUTF8String:orderId] withAmount:amount withCurrencyType: [NSString stringWithUTF8String:currencyType ] withPayType:[NSString stringWithUTF8String:payType]];
}