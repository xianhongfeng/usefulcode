//
//  TDAdTracking.h
//  testSHZ
//
//  Created by LexCo on 3/13/15.
//
//

#pragma once

class TDAdTracking
{
    TDAdTracking() {};
public:
    static TDAdTracking * getShared();
    void initialize(const char * iden, const char * channel);
    void onRegister(const char * userId);
    void onLogin(const char * userId);
    void onPay(const char * account, const char * orderId, int amount, const char * currencyType, const char * payType);
    
    // 电商
    //    TDOrder orderWithOrderId(const char * orderId, int total, const char * currencyType);
    //    void onPlaceOrder(const char * account , const char * orderId, int total, const char * currencyType);
};