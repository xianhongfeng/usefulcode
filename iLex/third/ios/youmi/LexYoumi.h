//
//  LexYoumi.h
//  ILex
//
//  Created by pro on 15/3/4.
//  Copyright (c) 2015年 lover. All rights reserved.
//

#pragma once

namespace Lex
{
    class Youmi
    {
    public:
        static Youmi * getShared();
        void initialize(const char * youmi_appId, const char * youmi_secretId);
        bool showAd();
    };
}