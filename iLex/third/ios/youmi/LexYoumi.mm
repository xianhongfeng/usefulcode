//
//  LexYoumi.cpp
//  ILex
//
//  Created by pro on 15/3/4.
//  Copyright (c) 2015年 lover. All rights reserved.
//

#include "LexYoumi.h"
#import "ConfigHeader.h"

namespace Lex
{
    Youmi * Youmi::getShared()
    {
        static Youmi * ym = new Youmi();
        return ym;
    }
    
    void Youmi::initialize(const char * youmi_appId, const char * youmi_secretId)
    {
        [YouMiNewSpot initYouMiDeveloperParams:[NSString stringWithUTF8String:youmi_appId] YM_SecretId:[NSString stringWithUTF8String:youmi_secretId]];
        [YouMiNewSpot initYouMiDeveLoperSpot:kSPOTSpotTypeLandscape];
    }
    
    bool Youmi::showAd()
    {
        [YouMiNewSpot showYouMiSpotAction:^(BOOL flag) {
         if (flag)
         {
         NSLog(@"插屏成功");
         }
         else
         {
         NSLog(@"插屏失败");
         }
         }];
        
        return true;
    }
}