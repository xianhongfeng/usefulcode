//
//  LexDuomeng.h
//  Ludingjiv0
//
//  Created by pro on 15/3/24.
//
//

#pragma once

namespace Lex
{
    class Duomeng
    {
    public:
        static Duomeng * getShared();
        void initialize(const char * duomengID);
        bool showVideo();
    };
}