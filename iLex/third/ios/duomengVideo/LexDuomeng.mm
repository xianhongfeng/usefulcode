//
//  LexDuomeng.cpp
//  Ludingjiv0
//
//  Created by pro on 15/3/24.
//
//

#include "LexDuomeng.h"
#import "IndependentVideoManager.h"
#import <Foundation/Foundation.h>
#include "OuterCallBack.h"

@interface DuomengDelegate : NSObject <IndependentVideoManagerDelegate>

+(DuomengDelegate*)getShare;

@end

@interface DuomengDelegate ()
@property(nonatomic, assign) BOOL isVideoPlayed;
@end

@implementation DuomengDelegate
+(DuomengDelegate*)getShare
{
    static DuomengDelegate * dele = [DuomengDelegate new];
    return dele;
}

- (void)ivManagerDidStartLoad:(IndependentVideoManager *)manager
{
    NSLog(@"准备加载");
    _isVideoPlayed = NO;
}

- (void)ivManagerDidFinishLoad:(IndependentVideoManager *)manager
{
    NSLog(@"加载完成");
}

- (void)ivManager:(IndependentVideoManager *)manager
failedLoadWithError:(NSError *)error
{
    NSLog(@"多盟视频出错了");
#if defined(VER_ARIES)
    Outer::onVideoFailure.run();
#endif
}

- (void)ivManagerCompletePlayVideo:(IndependentVideoManager *)manager
{
    NSLog(@"视频播放完毕");
    _isVideoPlayed = YES;
}

- (void)ivManagerDidClosed:(IndependentVideoManager *)manager
{
    NSLog(@"被关闭了");
#if defined(VER_ARIES)
    if (!_isVideoPlayed)
    {
        Outer::onVideoDidClosed.run();
    }
    else
    {
        Outer::onVIdeoFinish.run();
        _isVideoPlayed = NO;
    }
#endif
}

@end

namespace Lex
{
    static IndependentVideoManager * mManager = nil;
    Duomeng * Duomeng::getShared()
    {
        static Duomeng * dm = new Duomeng();
        return dm;
    }
    
    void Duomeng::initialize(const char * duomengID)
    {
        mManager = [[IndependentVideoManager alloc] initWithPublisherID:[NSString stringWithUTF8String:duomengID]];
        [mManager setDelegate:[DuomengDelegate getShare]];
    }
    
    bool Duomeng::showVideo()
    {
        [mManager presentIndependentVideo];
        
        return true;
    }
}