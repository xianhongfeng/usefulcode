//
//  LexYoumiVideo.cpp
//  Ludingjiv0
//
//  Created by pro on 15/3/24.
//
//

#include "LexYoumiVideo.h"
#import "CocoBVideo.h"
#import <Foundation/Foundation.h>
#import "RootViewController.h"
#include "OuterCallBack.h"

namespace Lex
{
    YoumiVideo * YoumiVideo::getShared()
    {
        static YoumiVideo * ym = new YoumiVideo();
        return ym;
    }
    
    void YoumiVideo::initialize(const char * appID, const char * appIDSecret)
    {
        [CocoBVideo cBVideoInitWithAppID:[NSString stringWithUTF8String:appID] cBVideoAppIDSecret:[NSString stringWithUTF8String:appIDSecret]];
    }
    
    void YoumiVideo::showVideo()
    {
#if defined(VER_ARIES)
        [CocoBVideo cBCloseButtonHide];
        [CocoBVideo cBIsHaveVideo:^(int isHaveVideoStatue) {
            if (isHaveVideoStatue != 0)
            {
                Outer::onVideoFailure.run();
            }
        }];
        [CocoBVideo cBVideoPlay:[RootViewController rootViewControllerLastCreated] cBVideoPlayFinishCallBackBlock:^(BOOL isFinishPlay){
            NSLog(@"视频播放结束");
        } cBVideoPlayConfigCallBackBlock:^(BOOL isLegal){
            NSLog(@"此次播放是否有效：%d",isLegal);
            if (isLegal)
            {
                Outer::onVIdeoFinish.run();
            }
            else
            {
                Outer::onVideoDidClosed.run();
            }
        }];
#endif
    }
}