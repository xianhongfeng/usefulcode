//
//  LexYoumiVideo.h
//  Ludingjiv0
//
//  Created by pro on 15/3/24.
//
//

#pragma once

namespace Lex
{
    class YoumiVideo
    {
    public:
        static YoumiVideo * getShared();
        void initialize(const char * appID, const char * appIDSecret);
        void showVideo();
    };
}
