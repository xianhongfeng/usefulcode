//
//  LexMix.m
//  Shuihuzhuan
//
//  Created by lover on 15/3/6.
//
//

#import <Foundation/Foundation.h>
#import "AppController.h"
#import "MIXView.h"
#include "LexMix.h"

namespace Lex {
    AdMix * AdMix::getShared()
    {
        static AdMix admix;
        return &admix;
    }
    
    void AdMix::initialize(std::string _id)
    {
        [MIXView initWithID:[NSString stringWithUTF8String:_id.data()]];
        [MIXView preloadAdWithDelegate:[AppController appControllerLastCreated] withPlace:@"default"];
    }
    void AdMix::showAd()
    {
        if ([MIXView canServeAd:@"default"]) {
            //有广告缓存,展示
            [MIXView showAdWithDelegate:[AppController appControllerLastCreated] withPlace:@"default"];
        } else {
            //没有广告缓存,预加载对应触发位
            [MIXView preloadAdWithDelegate:[AppController appControllerLastCreated] withPlace:@"default"];
        }
        return ;
    }
}