//
//  LexMix.h
//  Shuihuzhuan
//
//  Created by lover on 15/3/6.
//
//

#pragma once
#include <string>

namespace Lex {
    class AdMix
    {
    public:
        static AdMix * getShared();
        void initialize(std::string _id);
        void showAd();
    };
}