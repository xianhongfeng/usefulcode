//
//  GameCenter.mm
//  iLexLib
//
//  Created by leaving on 14-5-20.
//
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <GameKit/GameKit.h>
#include"GameCenter.h"

@interface GameKitHelper :  NSObject <GKGameCenterControllerDelegate, UIViewControllerTransitioningDelegate>
{
    BOOL gameCenterAvailable;
    BOOL userAuthenticated;
}

@property (assign, readonly) BOOL gameCenterAvailable;

+ (GameKitHelper *)sharedGameKitHelper;

- (BOOL)authenticateLocalUser;

- (BOOL)showLeaderboard;
- (BOOL)checkVersion;
- (BOOL)submitScore:(int64_t)score to:(NSString*)leaderBoard;
- (BOOL)submitScores:(std::vector<std::pair<std::string, int64_t> >) scores;

- (BOOL)isLogin;
//- (void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController;
//- (void)finishInteractiveTransition;
//- (void)closeCurrentModalViewController;
//- (void)canceint32_teractiveTransition;
//- (void)updateInteractiveTransition:(CGFloat)percentComplete;
@end

@implementation GameKitHelper
@synthesize gameCenterAvailable;

//静态初始化 对外接口
static GameKitHelper *sharedHelper = nil;
static UIViewController* currentModalViewController = nil;
+ (GameKitHelper *) sharedGameKitHelper {
    if (!sharedHelper) {
        sharedHelper = [[GameKitHelper alloc] init];
    }
    return sharedHelper;
}

//用于验证
- (BOOL)isGameCenterAvailable {
    // check for presence of GKLocalPlayer API
    Class gcClass = (NSClassFromString(@"GKLocalPlayer"));
    
    // check if the device is running iOS 4.1 or later
    NSString *reqSysVer =@"4.1";
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    BOOL osVersionSupported = ([currSysVer compare:reqSysVer
                                           options:NSNumericSearch] != NSOrderedAscending);
    
    return (gcClass && osVersionSupported);
}

- (id)init {
    if ((self = [super init])) {
        gameCenterAvailable = [self isGameCenterAvailable];
        if (gameCenterAvailable) {
            NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
            [nc addObserver:self
                   selector:@selector(authenticationChanged)
                       name:GKPlayerAuthenticationDidChangeNotificationName
                     object:nil];
        }
    }
    return self;
}
- (BOOL)checkVersion
{
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    if(version < 6.999f)
    {
        return NO;
    }
    else return YES;
}
- (BOOL)submitScores:(std::vector<std::pair<std::string, int64_t> >) scores
{
    if(![self isGameCenterAvailable] || ![self isLogin]) return NO;
    if(![self checkVersion]) return NO;
    if([GKLocalPlayer localPlayer].authenticated)
    {
        NSMutableArray * array = [[NSMutableArray alloc] init];
        for(auto & i : scores)
        {
            GKScore * score = [[GKScore alloc] initWithLeaderboardIdentifier:[NSString stringWithUTF8String:i.first.data()]];
            [score setValue:i.second];
            [array addObject:score];
        }
        [GKScore reportScores:array withCompletionHandler:^(NSError *error) {
            if (!error || (![error code] && ![error domain])) {
                NSLog(@"Score submitted correctly.");
            } else {
                NSLog(@"Error for submit score.");
            }
        }];
        return YES;
    }
    else return NO;
}
- (BOOL)submitScore:(int64_t)score to:(NSString*)leaderBoard
{
    if (![self isGameCenterAvailable] || ![self isLogin]) return NO;
    if(![self checkVersion]) return NO;
    if ([GKLocalPlayer localPlayer].authenticated)
    {
        GKScore *scoreReporter = [[GKScore alloc] initWithLeaderboardIdentifier:leaderBoard];
        scoreReporter.value = score;
        scoreReporter.context = 0;
        
        NSArray *scores = @[scoreReporter];
        [GKScore reportScores:scores withCompletionHandler:^(NSError *error) {
            if (!error || (![error code] && ![error domain])) {
                NSLog(@"Score submitted correctly.");
            } else {
                NSLog(@"Error for submit score.");
            }
        }];
        return YES;
    }
    else
    {
        NSLog(@"Not authenticated");
        return NO;
    }
}

//后台回调登陆验证
- (void)authenticationChanged {
    
    if ([GKLocalPlayer localPlayer].isAuthenticated &&!userAuthenticated) {
        NSLog(@"Authentication changed: player authenticated.");
        userAuthenticated = TRUE;
    } else if (![GKLocalPlayer localPlayer].isAuthenticated && userAuthenticated) {
        NSLog(@"Authentication changed: player not authenticated");
        userAuthenticated = FALSE;
    }
    
}

- (BOOL) authenticateLocalUser
{
    if (!gameCenterAvailable) return NO;
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    localPlayer.authenticateHandler = ^(UIViewController *viewController, NSError *error){
        if (viewController != nil)
        {
            [self closeCurrentModalViewController];
            UIWindow *window = [[UIApplication sharedApplication] keyWindow];
            [window.rootViewController presentViewController:viewController animated:YES completion:nil];
        }
    };
    return YES;
}

//显示排行榜
- (BOOL) showLeaderboard
{
    if (![self isGameCenterAvailable] || ![self isLogin]) return NO;
    
    GKGameCenterViewController *leaderboardController = [[GKGameCenterViewController alloc] init];
    if (leaderboardController != nil) {
        [self closeCurrentModalViewController];
        
        leaderboardController.gameCenterDelegate = self;
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        currentModalViewController = [[UIViewController alloc] init];
        [window addSubview:currentModalViewController.view];
        //		[currentModalViewController presentModalViewController:leaderboardController animated:YES];
        [currentModalViewController presentViewController:leaderboardController animated:YES completion:nil];
    }
    return YES;
}

//关闭排行榜回调
- (void) gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController
{
    [self closeCurrentModalViewController];
}
- (void)closeCurrentModalViewController
{
    if(currentModalViewController != nil)
    {
        //		[currentModalViewController dismissModalViewControllerAnimated:NO];
        [currentModalViewController dismissViewControllerAnimated:NO completion:nil];
        [currentModalViewController release];
        [currentModalViewController.view removeFromSuperview];
        currentModalViewController = nil;
    }
}

- (BOOL)isLogin
{
    return userAuthenticated;
}

- (void)canceint32_teractiveTransition
{
    [self closeCurrentModalViewController];
}
- (void)finishInteractiveTransition
{
    [self closeCurrentModalViewController];
}
- (void)updateInteractiveTransition:(CGFloat)percentComplete
{
    [self closeCurrentModalViewController];
}
@end


namespace Lex {
	namespace GC {
		bool ConnectGameCenter()
		{
			return [[GameKitHelper sharedGameKitHelper] authenticateLocalUser];
		}
		bool IsLogin()
		{
			return [[GameKitHelper sharedGameKitHelper] isLogin];
		}
		bool ShowGameCenter()
		{
			return [[GameKitHelper sharedGameKitHelper] showLeaderboard];
		}
		bool SubmitScore(const char * leader, int64_t data)
		{
			return [[GameKitHelper sharedGameKitHelper] submitScore:data to:[NSString stringWithUTF8String:leader]];
		}
        bool SubmitScores(std::vector<std::pair<std::string, int64_t> > scores)
        {
            return [[GameKitHelper sharedGameKitHelper] submitScores:scores];
        }
		bool IsValid()
		{
			return [[GameKitHelper sharedGameKitHelper] checkVersion];
		}
	}
}