﻿//
//  GameCenter.h
//  iLexLib
//
//  Created by leaving on 14-5-19.
//
//

#pragma once

#include <iostream>
#include <string>
#include <vector>

namespace Lex {
	namespace GC
	{
		bool ConnectGameCenter();
		bool IsLogin();
		bool IsValid();
		bool ShowGameCenter();
		bool SubmitScore(const char * leader, int64_t data);
        bool SubmitScores(std::vector<std::pair<std::string, int64_t> > scores);
	}
}
