//
//  LexChangsi.h
//  ILex
//
//  Created by pro on 15/3/5.
//  Copyright (c) 2015年 lover. All rights reserved.
//

#pragma once

namespace Lex
{
    class Changsi
    {
    public:
        static Changsi * getShare();
        void initialize(const char * publish_id, const char * placement_id);
        bool showAd();
    };
}
