//
//  LexChangsi.cpp
//  ILex
//
//  Created by pro on 15/3/5.
//  Copyright (c) 2015年 lover. All rights reserved.
//

#include "LexChangsi.h"
#include "ChanceAd.h"
#include <iostream>

namespace Lex
{
    static std::string m_PlacementId;
    Changsi * Changsi::getShare()
    {
        static Changsi * cs = new Changsi();
        return cs;
    }
    
    void Changsi::initialize(const char * publish_id, const char * placement_id)
    {
        m_PlacementId = placement_id;
        NSLog(@"SDK版本号：%@", [ChanceAd sdkVersion]);
        [ChanceAd startSession:[NSString stringWithUTF8String:publish_id]];
    }
    
    bool Changsi::showAd()
    {
        CSADRequest *adRequest = [CSADRequest request];
        adRequest.placementID = [NSString stringWithUTF8String:m_PlacementId.c_str()];
        [CSInterstitial sharedInterstitial].loadNextWhenClose = NO;
        [CSInterstitial sharedInterstitial].didLoadAD = ^() {
            NSLog(@"----------%s", __PRETTY_FUNCTION__);
        };
        [CSInterstitial sharedInterstitial].loadADFailure = ^(CSRequestError *error){
            NSLog(@"----------%s, %@", __PRETTY_FUNCTION__, error);
        };
        [[CSInterstitial sharedInterstitial] showInterstitialWithScale:1.f];
        
        return true;
    }
}