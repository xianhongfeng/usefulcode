﻿//
//  jai_net.h
//  proj_ios
//
//  Created by leaving on 14-9-28.
//  Copyright (c) 2014年 leaving. All rights reserved.
//

#pragma once
#include "jai_net_define.h"

namespace jai {
	class Connector;
	class Listener;
	class Socket
	{
		//	friend class listener;
		//	friend class connector;
	protected:
		jai::NativeSocket mSocketID;
		
		Socket(Socket const &);
		void operator = (Socket const &);
	public:
		Socket();
		Socket(AddressFamily family,
			   SocketType type,
			   ProtocolType  ipproto);
		bool open(AddressFamily family = AddressFamily::inet,
				  SocketType type = SocketType::stream,
				  ProtocolType  ipproto = ProtocolType::ip);
		bool bind(IPV4 ip);
		bool close();
		bool shutdown(HowToShut how);
		NativeSocket native() const;
		bool isValid() const;
		bool send_oob(char c) ;
		bool recv_oob(char &c);
        ssize_t receive(char * buf, size_t size, MsgWay way = MsgWay::Default);
        ssize_t send(char const *buf, size_t size, MsgWay way = MsgWay::Default);
//		size_t receive(char * buf, size_t size, MsgWay way = MsgWay::Default);
//		size_t send(char const *buf, size_t size, MsgWay way = MsgWay::Default);
		bool setNonBlock(bool flag) ;
		size_t couldReadBytes() ;
		bool isConnect();
	};
	class Connector : public Socket
	{
	protected:
		Connector(Connector const &);
		void operator = (Connector const &);
	public:
		Connector();
		Connector(AddressFamily family ,
				  SocketType type,
				  ProtocolType  ipproto);
		bool connectTo(IPV4 server, AddressFamily family);
	};
}