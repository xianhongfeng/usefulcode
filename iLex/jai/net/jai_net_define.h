﻿//
//  jai_net.h
//  proj_ios
//
//  Created by leaving on 14-9-28.
//  Copyright (c) 2014年 leaving. All rights reserved.
//

#pragma once
#include "../basic/jai_endian.h"
#include "../basic/jai_platform.h"
#include "../basic/jai_log.h"
#ifdef JAI_OS_WIN
#include <Windows.h>
#else
#include <errno.h>
#include <unistd.h>
#include<sys/time.h>
#include <sys/socket.h>
#include <sys/proc.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <netdb.h> // gethostbyname
#include <sys/ioctl.h>
#endif

#define JAI_INVALID_SOCKET INVALID_SOCKET
#define JAI_SOCKET_ERROR SOCKET_ERROR

#ifdef JAI_OS_WIN
#define MSG_DONTWAIT 0
#pragma comment(lib, "ws2_32.lib")
typedef signed long ssize_t;
#endif

namespace jai {
	
	typedef int NativeSocket;
	static const NativeSocket InvalidSocket = -1;
	
	enum class AddressFamily
	{
		unspec    = AF_UNSPEC    ,
		unix      = AF_UNIX      ,
		inet      = AF_INET      ,
		implink   = AF_IMPLINK   ,
		pup       = AF_PUP       ,
		chaos     = AF_CHAOS     ,
		ipx       = AF_IPX       ,
		ns        = AF_NS        ,
		iso       = AF_ISO       ,
		osi       = AF_OSI       ,
		ecma      = AF_ECMA      ,
		datakit   = AF_DATAKIT   ,
		ccitt     = AF_CCITT     ,
		sna       = AF_SNA       ,
		decnet    = AF_DECnet    ,
		dli       = AF_DLI       ,
		lat       = AF_LAT       ,
		hylink    = AF_HYLINK    ,
		appletalk = AF_APPLETALK ,
		netbios   = AF_NETBIOS   ,
		//		Voiceview = AF_VOICEVIEW ,
		//		Firefox   = AF_FIREFOX   ,
		//		Unknown1  = AF_UNKNOWN1  ,
		//		Ban       = AF_BAN       ,
	};
	enum class SocketType
	{
		stream    = SOCK_STREAM   ,
		dgram     = SOCK_DGRAM    ,
		raw       = SOCK_RAW      ,
		rdm       = SOCK_RDM      ,
		seqpacket = SOCK_SEQPACKET,
	};
	enum class ProtocolType
	{
		ip   = IPPROTO_IP  ,
		icmp = IPPROTO_ICMP,
		igmp = IPPROTO_IGMP,
		ggp  = IPPROTO_GGP ,
		tcp  = IPPROTO_TCP ,
		pup  = IPPROTO_PUP ,
		udp  = IPPROTO_UDP ,
		idp  = IPPROTO_IDP ,
		nd   = IPPROTO_ND  ,
		raw  = IPPROTO_RAW ,
		max  = IPPROTO_MAX ,
	};
	enum class HowToShut
	{
#ifdef JAI_OS_WIN
		NoRead = 0,
		NoWrite = 1,
		Neither = 2, 
#else
		NoRead = SHUT_RD,
		NoWrite = SHUT_WR,
		Neither = SHUT_WR,
#endif
	};
	enum class MsgWay
	{
		Default = 0,
		OOB = MSG_OOB,
		NoWait = MSG_DONTWAIT,
	};
	inline std::string GetStringByIp(uint32 ip)
	{
		in_addr add  = { ip };
		return inet_ntoa(add);
	}
	inline uint32_t	GetIpByName(const char * name)
	{
		hostent* pHostent = gethostbyname(name);
		if(pHostent == nullptr) return INADDR_NONE;
		uint32_t host = *(uint32_t*)pHostent->h_addr_list[0];
		return host;
	}
	
	class IPV4
	{
	private:
		uint32_t mIP;
		uint16_t mPort;
	public:
		IPV4()
		{
			mPort = 0;
			mIP = INADDR_NONE;
		}
		IPV4(uint16_t net_port, uint32_t net_bo_ip)
		{
			mPort = net_port;
			mIP = net_bo_ip;
		}
		IPV4(uint16_t net_port)
		{
			mPort = net_port;
			mIP = jai::Byte4(127, 0, 0, 1).value;
		}
		IPV4(uint16_t net_port, int8_t ip0, int8_t ip1, int8_t ip2, int8_t ip3)
		{
			mPort = net_port;
			mIP = jai::Byte4(ip0, ip1, ip2, ip3).value;
		}
		IPV4(uint16_t net_port, const char * addr)
		{
			mPort = net_port;
			mIP = ::inet_addr(addr);
			if(mIP == INADDR_NONE)
			{
				mIP = GetIpByName(addr);
			}
		}
		bool isValidIP()
		{
			return mIP != INADDR_NONE;
		}
		uint32_t ip() const
		{
			return mIP;
		}
		uint16_t port() const
		{
			return mPort;
		}
	};
	class SocketKeeper
	{
	private:
		SocketKeeper(SocketKeeper const &);
		void operator = (SocketKeeper const &);
	public:
		SocketKeeper(int16_t version = 0x0202)
		{
#ifdef JAI_OS_WIN
			WSADATA wsadata;
			WSAStartup(version, &wsadata);
#else
            signal(SIGPIPE, SIG_IGN);
#endif
		}
        void setPipeSignal(void (*func)(int))
        {
#ifndef JAI_OS_WIN
            signal(SIGPIPE, func);
#endif
        }
		~SocketKeeper()
		{
#ifdef JAI_OS_WIN
			WSACleanup();
#endif
		}
	};
	template<typename _A, typename _B, typename _C>
	int SocketCtrl(_A a, _B b, _C c)
	{
#ifdef JAI_OS_WIN
		return ::ioctlsocket(a, b, c);
#else
		return ::ioctl(a, b, c);
#endif
	}
	template<typename _A>
	int SocketClose(_A a)
	{
#ifdef JAI_OS_WIN
		return ::closesocket(a);
#else
		return ::close(a);
#endif
	}
	inline int GetSocketError()
	{
#ifdef JAI_OS_WIN
		return WSAGetLastError();
#else
		return errno;
#endif
	}
}
