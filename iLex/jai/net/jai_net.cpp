//
//  jai_net.cpp
//  proj_ios
//
//  Created by leaving on 14-9-28.
//  Copyright (c) 2014年 leaving. All rights reserved.
//

#include "jai_net.h"
#include "LexShared.h"

namespace jai {
	Socket::Socket()
	{
		mSocketID = InvalidSocket;
	}
	Socket::Socket(AddressFamily family ,
		   SocketType type,
		   ProtocolType  ipproto)
	{
		mSocketID = InvalidSocket;
		open(family, type, ipproto);
	}
	bool Socket::bind(jai::IPV4 ip)
	{
		sockaddr_in addr;
		addr.sin_family = AF_INET;
		addr.sin_port = ip.port();
		addr.sin_addr.s_addr = ip.ip();
		::memset(&(addr.sin_zero), 0, sizeof(addr.sin_zero)); /* zero the rest of the struct */
		if(::bind(mSocketID, (sockaddr*)&addr, sizeof(sockaddr_in)) == 0) return true;
		int eno = GetSocketError();
		jai::Log::Error(JAI_TAG, "error for bind,e:%d", eno);
		return false;
	}
	bool Socket::open(AddressFamily family,
			  SocketType type,
		   ProtocolType  ipproto)
	{
		if(mSocketID != InvalidSocket) this->close();
		mSocketID = ::socket((int)family, (int)type, (int)ipproto);
		if(mSocketID == InvalidSocket)
		{
			jai::Log::Error(JAI_TAG, "error when create socket");
			return false;
		}
		return true;
	}
	bool Socket::close()
	{
		if(SocketClose(mSocketID) != 0)
		{
			int eno = GetSocketError();
			jai::Log::Error(JAI_TAG, "error for close socket, e:%d", eno);
			return false;
		}
		mSocketID = InvalidSocket;
		return true;
	}
	bool Socket::shutdown(HowToShut how)
	{
		if(::shutdown(mSocketID, (int)how) == -1)
		{
			int eno = GetSocketError();
			jai::Log::Error(JAI_TAG, "error for shutdown socket, e:%d", eno);
			return false;
		}
		return true;
	}
	NativeSocket Socket::native() const
	{
		return mSocketID;
	}
	bool Socket::isValid() const
	{
		return mSocketID != InvalidSocket;
	}
	bool Socket::send_oob(char c)
	{
		return ::send(mSocketID, &c, 1, MSG_OOB) == 1;
	}
	bool Socket::recv_oob(char &c)
	{
		return ::recv(mSocketID, &c, 1, MSG_OOB) == 1;
	}
    
    ssize_t Socket::receive(char * buf, size_t size, MsgWay way)
    {
        int r = (int)recv(mSocketID, buf, size, (int)way);
        if (r < 0 &&
            (EAGAIN == errno ||
             EWOULDBLOCK == errno ||
             EINTR == errno))
        {
            return 0;
        }
        else if (r < 0 &&
                 (ECONNRESET == errno ||
                  EPIPE == errno))
        {
            return -1;
        }
        
        if (r < 0)
        {
            return -1;
        }
        
        if (0 == r)
        {
            return -1;
        }
        
        return r;
    }
    ssize_t Socket::send(char const *buf, size_t size, MsgWay way )
    {
        int r = (int)::send(mSocketID, buf, size, (int)way);
        if (r < 0 &&
            (EAGAIN == errno ||
             EWOULDBLOCK == errno ||
             EINTR == errno))
        {
            return 0;
        }
        else if (r < 0 &&
                 (ECONNRESET == errno ||
                  EPIPE == errno))
        {
            return -1;
        }
        
        if (r < 0)
        {
            return -1;
        }
        
        if (0 == r)
        {
            return -1;
        }
        
        return r;
    }
    
//	size_t Socket::receive(char * buf, size_t size, MsgWay way)
//	{
//		int r = (int)recv(mSocketID, buf, size, (int)way);
//		if(r >= 0) return r;
//		else
//		{
//			int eno = GetSocketError();
////			jai::Log::Error(JAI_TAG, "receive error, e:%d", eno);
//            //Lex::ShowAlert("网络错误", "连接断开。请重启。");
//            
//			return 0;
//		}
//	}
//	size_t Socket::send(char const *buf, size_t size, MsgWay way )
//	{
//		int r = (int)::send(mSocketID, buf, size, (int)way);
//		if(r >= 0) return r;
//		else
//		{
//			int eno = GetSocketError();
////			jai::Log::Error(JAI_TAG, "send error, e:%d", eno);
//			return 0;
//		}
//	}
	bool Socket::setNonBlock(bool flag)
	{
		u_long arg(flag?1:0);
		return SocketCtrl(mSocketID, FIONBIO, &arg) == 0;
	}
	size_t Socket::couldReadBytes()
	{
		u_long arg(0);
		if(SocketCtrl(mSocketID, FIONREAD, &arg) == 0) return arg;
		return 0;
	}
	bool Socket::isConnect()
	{
		if(::send(mSocketID, "", 0, MSG_DONTWAIT) != 0) return false;
		return true;
	}
	Connector::Connector()
	{
	}
	Connector::Connector(AddressFamily family ,
						 SocketType type,
						 ProtocolType  ipproto)
	: Socket(family, type, ipproto)
	{
	}
	bool Connector::connectTo(IPV4 server, AddressFamily family)
	{
		struct sockaddr_in addr;
		addr.sin_family = (int)family;
		addr.sin_port = server.port();
		addr.sin_addr.s_addr = server.ip();
		::memset(&(addr.sin_zero), 0, sizeof(addr.sin_zero)); /* zero the rest of the struct */
		if(::connect(mSocketID, (struct sockaddr *)&addr, sizeof(struct sockaddr)) == 0) return true;
		int eno = GetSocketError();
		jai::Log::Error(JAI_TAG, "failed to connect, e:%d", eno);
		return false;
	}
#if 0
	class listener
	{
	private:
		pretty_socket_t Myfd;
		listener(listener const &);
		void operator =(listener const &);
	public:
		listener(
				 ipv4 const & local_addr,
				 AddressFamily::Type const &family = AddressFamily::Inet,
				 SocketType::Type const &type = SocketType::Stream,
				 ProtocolType::Type const & ipproto = ProtocolType::Ip
				 )
		
		: Myfd(pretty_socket_open(family, type, ipproto))
		{
			if(Myfd == PRETTY_INVALID_SOCKET) return;
			
			struct sockaddr_in my_addr;
			my_addr.sin_family = family; /* host byte order */
			my_addr.sin_port = local_addr.port(); /* i2_t, network byte order */
			my_addr.sin_addr.s_addr =  local_addr.ip();
			::memset(&(my_addr.sin_zero), Value::S4_ZERO, sizeof(my_addr.sin_zero)); /* zero the rest of the struct */
			if(bind(Myfd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr)) == PRETTY_SOCKET_ERROR)
			{
				pretty_socket_close(Myfd);
				Myfd = PRETTY_INVALID_SOCKET;
			}
		}
		~listener()
		{
			pretty_socket_close(Myfd);
		}
		bool valid() const
		{
			return Myfd != PRETTY_INVALID_SOCKET;
		}
		bool listen(i4_t backlog)
		{
			if(Myfd == PRETTY_INVALID_SOCKET) return false;
			return pretty_listen(Myfd, backlog) != PRETTY_SOCKET_ERROR;
		}
		socket accept()
		{
			if(Myfd == PRETTY_INVALID_SOCKET) return socket(PRETTY_INVALID_SOCKET);
			struct sockaddr_in con_addr;
			i4_t addr_len = sizeof(struct sockaddr_in);
			return socket(pretty_accept(Myfd, (sockaddr *)&con_addr, &addr_len));
		}
	};
#endif
}
