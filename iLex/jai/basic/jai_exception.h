/*
 Copyright (c) 2013 Tianj. Guo
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 the Software, and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <typeinfo>
#include <exception>
namespace jai {
	struct Exception : public std::exception
	{
		char const * mFile;
		int mLine;
		char const * mWhat;
		int	mError;
		
		Exception() throw()
		{}
		Exception(char const * file, int line, char const * whatStr, int error = 0) throw()
			: mFile(file)
			, mLine(line)
			, mWhat(whatStr)
			, mError(error)
		{
		}
		virtual char const * what() const throw() 
		{
			return mWhat;
		}
		char const * file() const throw()
		{
			return mFile;
		}
		int line() const throw()
		{
			return mLine;
		}
		int error() const throw()
		{
			return mError;
		}
	};
}

#define must(cond) do { if(!(cond)) throw ::jai::Exception(__FILE__,__LINE__,  "Nil"); } while(0)

#define musts(cond, str) do { if(!(cond)) throw ::jai::Exception(__FILE__,__LINE__,  str); } while(0)

#define mustsn(cond, str, eno) do { if(!(cond)) throw ::jai::Exception(__FILE__,__LINE__,  str, eno); } while(0)

#define throws(str) throw ::jai::Exception(__FILE__,__LINE__, str)

#define throwsn(str, eno) throw ::jai::Exception(__FILE__,__LINE__, str, eno)

