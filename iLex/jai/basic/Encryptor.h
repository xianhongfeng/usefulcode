//
//  Encoder.h
//  Encoder
//
//  Created by lover on 15/1/8.
//  Copyright (c) 2015年 lover. All rights reserved.
//

#ifndef Encoder_Encoder_h
#define Encoder_Encoder_h

#include "jai_encryption.h"

typedef jai::Encrypt01<unsigned char> Encryptor;

/*
 
 usage:
 
    unsigned char temp[] = "helloworld";
    int seed = 1234;
    Encryptor en(seed);
    en.encode(temp, sizeof(temp));
 
    en.decode(temp, sizeof(temp));
    printf("%s\n", temp);
 
 
 
 */
#endif
