/*
 Copyright (c) 2013 Tianj. Guo
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 the Software, and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#include "jai_type.h"
#include "jai_exception.h"
namespace jai
{
	/* Linear Feedback Shift Register(LFSR)  */
	class RandomLFSR
	{
	protected:
		::uint32_t z1, z2, z3, z4;
		// seed limits: z1 > 1, z2 > 7, z3 > 15, z4 > 127
	public:
		static const ::uint32_t Max_value = ~(0U);
		
		RandomLFSR(::uint32_t seed = 0)
		{
			reset(seed);
		}
		void reset(::uint32_t seed)
		{
			z1 = seed | 0x2U;
			z2 = ((seed >> 4) ^ (seed << 8)) | 0x8U;
			z3 = ((seed >> 6) ^ (seed << 5)) | 0x10U;
			z4 = ((seed >> 8) ^ (seed << 2)) | 0x80U;
		}
		::uint32_t next()
		{
			::uint32_t tbit;
			tbit = ((z1 << 6) ^ z1) >> 13;
			z1 = ((z1 & 0xFFFFFFFE) << 18) ^ tbit;
			tbit = ((z2 << 2) ^ z2) >> 27;
			z2 = ((z2 & 0xFFFFFFF8) << 2 ) ^ tbit;
			tbit = ((z3 << 13) ^ z3) >> 21;
			z3 = ((z3 & 0xFFFFFFF0) << 7 ) ^ tbit;
			tbit = ((z4 << 3) ^ z4) >> 12;
			z4 = ((z4 & 0xFFFFFF80) << 13) ^ tbit;
			return (z1 ^ z2 ^ z3 ^ z4);
		}
		::uint32_t next2()
		{
			::uint32_t tbit;
			tbit = ((z1 << 7) ^ z1) >> 14;
			z1 = ((z1 & 0xFFFFFFFE) << 17) ^ tbit;
			tbit = ((z2 << 13) ^ z2) >> 27;
			z2 = ((z2 & 0xFFFFFFF8) << 2 ) ^ tbit;
			tbit = ((z3 << 2) ^ z3) >> 21;
			z3 = ((z3 & 0xFFFFFFF0) << 7 ) ^ tbit;
			tbit = ((z4 << 3) ^ z4) >> 13;
			z4 = ((z4 & 0xFFFFFF80) << 12) ^ tbit;
			return (z1 ^ z2 ^ z3 ^ z4);
		}
		::uint32_t operator ()()
		{
			return next();
		}
	};
	/* Well512 */
	class RandomWell
	{
	protected:
		static const ::uint32_t oStateCount = 16;
		::uint32_t mIndex;
		::uint32_t mStates[oStateCount];
	public:
		static const ::uint32_t Max_value = ~(0U);
		
		RandomWell(::uint32_t seed = 0)
		{
			reset(seed);
		}
		void reset(::uint32_t seed)
		{
			mIndex = 0;
			mStates[0] = seed;
			for(::uint32_t i = 1; i < oStateCount; ++i)
				mStates[i] = (seed >> i) ^ ((seed + i) << i);
		}
		/*
		 come from the book : Game Programming Gems 7 by Scott Jacobs.
		 */
		::uint32_t next()
		{
			::uint32_t a, b, c, d;
			a = mStates[mIndex];
			c = mStates[(mIndex + 13) & 15];
			b = a ^ c ^ (a << 16) ^ (c << 15);
			c = mStates[(mIndex + 9) & 15];
			c ^= (c >> 11);
			a = mStates[mIndex] = b ^ c;
			d = a ^ ((a << 5) & 0xDA442D24U);
			mIndex = (mIndex + 15) & 15;
			a = mStates[mIndex];
			mStates[mIndex] = a ^ b ^ d ^ (a << 2) ^ (b << 18) ^ (c << 28);
			return mStates[mIndex];
		}
		::uint32_t operator ()()
		{
			return this->next();
		}
	};
	/* @ RandRNG
	 tRandRun: raw random function. has operator() and ctor(uint)/ctor()/ctor(copy).
	 tMax: tRandRun will generate the numbers in the range [0, tMax).
	 */
	template<typename tRandFun = RandomLFSR, ::uint32_t tMax = tRandFun::Max_value>
	class RandomNG
	{
	public:
		static const ::uint32_t Max_value = tMax;
		typedef tRandFun Func;
	protected:
		mutable Func mRandFunc;
	public:
		RandomNG()
		{
		}
		RandomNG(Func const &fun)
		: mRandFunc(fun)
		{
		}
		RandomNG(::uint32_t seed)
		: mRandFunc(seed)
		{
		}
			void reset(::uint32_t seed)
			{
			    mRandFunc.reset(seed);
			}
		::uint32_t next() const
		{
			return mRandFunc();
		}
		::uint32_t next(::uint32_t max) const
		{
			if(max < 2) return 0;
			return mRandFunc() % max;
		}
		float nextFloat() const
		{
			return mRandFunc() / (float)Max_value;
		}
	};
	typedef RandomNG<> Random;
}
