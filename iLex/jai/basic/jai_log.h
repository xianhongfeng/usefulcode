﻿/*
 Copyright (c) 2013 Tianj. Guo
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 the Software, and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <cstdio>
#include <cstdarg>
#include <fstream>
#include <ctime>
#include "jai_platform.h"
#include "jai_type.h"
#include "jai_exception.h"

#ifdef JAI_OS_ANDROID
#include<android/log.h>
#define JAI_LOG_TYPE	android_LogPriority
#define JAI_LOG_ERROR	ANDROID_LOG_ERROR
#define JAI_LOG_INFO	ANDROID_LOG_INFO
#define JAI_LOG_DEBUG	ANDROID_LOG_DEBUG
#define JAI_LOG_WARN	ANDROID_LOG_WARN
#else
#define JAI_LOG_TYPE	 char const
#define JAI_LOG_ERROR	'E'
#define JAI_LOG_INFO	 'I'
#define JAI_LOG_DEBUG	'D'
#define JAI_LOG_WARN 'W'
#endif

#define JAI_TAG "jai"
namespace jai {
	class Log
	{
	public:
		static void Print(JAI_LOG_TYPE type, char const * tag, char const * fmt,
						  va_list va)
		{
#ifdef JAI_OS_ANDROID
			__android_log_vprint(type, tag, fmt, va);
#else
			time_t ti = ::time(0);
			tm * timeStruct = localtime(&ti);
			fprintf(stderr, "%c/%s:[%04d-%02d-%02d %02d:%02d:%02d] ",
					type, tag,
					timeStruct->tm_year + 1900,
					timeStruct->tm_mon + 1,
					timeStruct->tm_mday,
					timeStruct->tm_hour,
					timeStruct->tm_min,
					timeStruct->tm_sec);
			vfprintf(stderr, fmt, va);
			fprintf(stderr, "\n");
#endif
		}
	public:
		static void Error(char const * tag, char const * fmt, ...)
		{
			va_list va;
			va_start(va, fmt);
			Print(JAI_LOG_ERROR, tag, fmt, va);
			va_end(va);
		}
		static void Warn(char const * tag, char const * fmt, ...)
		{
			va_list va;
			va_start(va, fmt);
			Print(JAI_LOG_WARN, tag, fmt, va);
			va_end(va);
		}
		static void Info(char const * tag, char const * fmt, ...)
		{
			va_list va;
			va_start(va, fmt);
			Print(JAI_LOG_INFO, tag, fmt, va);
			va_end(va);
		}
		static void Debug(char const * tag, char const * fmt, ...)
		{
			va_list va;
			va_start(va, fmt);
			Print(JAI_LOG_DEBUG, tag, fmt, va);
			va_end(va);
		}
	};
}
