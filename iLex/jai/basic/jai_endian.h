/*
 Copyright (c) 2013 Tianj. Guo
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 the Software, and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#include "jai_type.h"

#ifdef __linux__
#	include <endian.h>
#	define JAI_BYTEORDER  __BYTE_ORDER
#define JAI_BYTEORDER_BIGENDIAN		__BIG_ENDIAN
#define JAI_BYTEORDER_LITENDIAN		__LITTLE_ENDIAN
#else 
#define JAI_BYTEORDER_BIGENDIAN 0x00010203
#define JAI_BYTEORDER_LITENDIAN 0x03020100
#	if defined(__hppa__) || defined(__m68k__) || defined(mc68000) || \
defined(_M_M68K) || (defined(__MIPS__) && defined(__MISPEB__)) || \
defined(__ppc__) || defined(__POWERPC__) || defined(_M_PPC) || \
defined(__sparc__)
#		define JAI_BYTEORDER   JAI_BYTEORDER_BIGENDIAN
#	else
#		define JAI_BYTEORDER   JAI_BYTEORDER_LITENDIAN
#	endif
#endif

namespace jai {
	inline std::uint32_t EndianSwap32(std::uint32_t num)
	{
		return ((num & 0xff) << 24) |((num & 0xff00) << 8) |
		((num & 0xff0000) >> 8) | ((num & 0xff000000) >> 24);
	}
	inline std::uint16_t EndianSwap16(std::uint16_t num)
	{
		return ((num & 0xff) << 8) | ((num & 0xff00) >> 8);
	}
	
	// switch between host-order and net-order(be)
	inline std::uint32_t HostNet32(std::uint32_t num)
	{
#if JAI_BYTEORDER == JAI_BYTEORDER_BIGENDIAN
		return num;
#else
		return EndianSwap32(num);
#endif
	}
	
	// switch between host-order and net-order(be)
	inline std::uint16_t HostNet16(std::uint16_t num)
	{
#if JAI_BYTEORDER == JAI_BYTEORDER_BIGENDIAN
		return num;
#else
		return EndianSwap16(num);
#endif
	}
	union Byte4 {
		std::uint32_t value;
		std::uint8_t  bytes[4];
		
		Byte4(std::uint32_t v = 0)
		{
			set(v);
		}
		Byte4(std::uint8_t v0, std::uint8_t v1, std::uint8_t v2, std::uint8_t v3)
		{
			set(v0, v1, v2, v3);
		}
		void set(std::uint32_t v)
		{
			value = v;
		}
		void setBE(std::uint32_t v)
		{
#if JAI_BYTEORDER == JAI_BYTEORDER_BIGENDIAN
			value = v;
#else
			value = EndianSwap32(v);
#endif
		}
		void setLE(std::uint32_t v)
		{
#if JAI_BYTEORDER == JAI_BYTEORDER_LITENDIAN
			value = v;
#else
			value = EndianSwap32(v);
#endif
		}
		std::uint32_t & get()
		{
			return value;
		}
		std::uint32_t get() const
		{
			return value;
		}
		std::uint32_t getBE()
		{
#if JAI_BYTEORDER == JAI_BYTEORDER_BIGENDIAN
			return value;
#else
			return EndianSwap32(value);
#endif
		}
		std::uint32_t getLE()
		{
#if JAI_BYTEORDER == JAI_BYTEORDER_LITENDIAN
			return value;
#else
			return EndianSwap32(v);
#endif
		}
		std::uint32_t getBE() const
		{
#if JAI_BYTEORDER == JAI_BYTEORDER_BIGENDIAN
			return value;
#else
			return EndianSwap32(value);
#endif
		}
		std::uint32_t getLE() const
		{
#if JAI_BYTEORDER == JAI_BYTEORDER_LITENDIAN
			return value;
#else
			return EndianSwap32(v);
#endif
		}
		// set byte
		void set(std::uint8_t v0, std::uint8_t v1, std::uint8_t v2, std::uint8_t v3)
		{
			bytes[0] = v0;
			bytes[1] = v1;
			bytes[2] = v2;
			bytes[3] = v3;
		}
		void setBE(std::uint8_t v0, std::uint8_t v1, std::uint8_t v2, std::uint8_t v3)
		{
#if JAI_BYTEORDER == JAI_BYTEORDER_BIGENDIAN
			set(v0, v1, v2, v3);
#else
			set(v3, v2, v1, v0);
#endif
		}
		void setLE(std::uint8_t v0, std::uint8_t v1, std::uint8_t v2, std::uint8_t v3)
		{
#if JAI_BYTEORDER == JAI_BYTEORDER_LITENDIAN
			set(v0, v1, v2, v3);
#else
			set(v3, v2, v1, v0);
#endif
		}
		// get byte
		std::uint8_t & get(uint32 i)
		{
			return bytes[i];
		}
		std::uint8_t & getBE(uint32 i)
		{
#if JAI_BYTEORDER == JAI_BYTEORDER_BIGENDIAN
			return bytes[i];
#else
			return bytes[3 - i];
#endif
		}
		std::uint8_t & getLE(uint32 i)
		{
#if JAI_BYTEORDER == JAI_BYTEORDER_LITENDIAN
			return bytes[i];
#else
			return bytes[3 - i];
#endif
		}
		// const get byte
		std::uint8_t get(uint32 i) const
		{
			return bytes[i];
		}
		std::uint8_t getBE(uint32 i) const
		{
#if JAI_BYTEORDER == JAI_BYTEORDER_BIGENDIAN
			return bytes[i];
#else
			return bytes[3 - i];
#endif
		}
		std::uint8_t getLE(uint32 i) const
		{
#if JAI_BYTEORDER == JAI_BYTEORDER_LITENDIAN
			return bytes[i];
#else
			return bytes[3 - i];
#endif
		}
		bool operator == (Byte4 const & o)
		{
			return value == o.value;
		}
		bool operator != (Byte4 const & o)
		{
			return value != o.value;
		}
	};
}
