/*
 Copyright (c) 2013 Tianj. Guo
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 the Software, and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

// define platform
#if defined(_WIN32)
#	define JAI_OS_WIN
#	define JAI_OS_WIN32

#elif defined(__ANDROID__)
#	define JAI_OS_LINUX
#	define JAI_OS_ANDROID

#elif defined(__APPLE__)
#	define JAI_OS_APPLE
#	if __has_feature(objc_arc)
#		define JAI_OC_RELEASE(_x)
#		define JAI_OC_AUTORELEASE(_x)
#		define JAI_OC_RETAIN(_x)
#	else
#		define JAI_OC_RELEASE(_x) [_x release]
#		define JAI_OC_AUTORELEASE(_x) [_x autorelease]
#		define JAI_OC_RETAIN(_x) [_x retain]
#	endif
#	if  1//!TARGET_OS_IPHONE
//#		define JAI_OS_APPLE_MAC
//#	else
#		define JAI_OS_APPLE_IOS
#	endif
#else
#	error "unknow os"
#endif

#if _DEBUG || DEBUG
#define JAI_DEBUG 1
#endif
