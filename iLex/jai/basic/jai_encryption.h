/*
 Copyright (c) 2013 Tianj. Guo
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 the Software, and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#include "jai_random.h"

namespace jai {
	// Encryption name type: "Encrypt", encryption type list, [s]
	enum class EEncryptWay
	{
		Random = 0, Or = 1, Plus = 2,
	};
	
	template<typename tType, typename tRandom = Random>
	class Encrypt01
	{
	protected:
		uint32 mSeed;
		
	public:
		typedef tType ElementType;
		typedef tRandom RandomType;
		
	public:
		inline Encrypt01(uint32 seed = 0);
		inline void setSeed(uint32 seed);
		inline uint32 getSeed() const;
		inline void encode(ElementType * buffer, uint32 count) const;
		inline void decode(ElementType * buffer, uint32 count) const;
	};
	template<typename tType>
	class Encrypt12s
	{
	protected:
		tType mSeed0, mSeed1;
		
	public:
		typedef tType ElementType;
		
	public:
		inline Encrypt12s(tType s0, tType s1);
		inline void setSeed(tType s0, tType s1);
		inline ElementType getSeed(int which); // which is 0, 1
		inline void encode(ElementType * buffer, uint32 count);
		inline void decode(ElementType * buffer, uint32 count);
	};
	
#define JAI_INT_MASK 0x569A37E84BCF1E69
	
	template<typename tInt, tInt vMask>
	class IntegerEncrypt
	{
	protected:
		tInt mValue;
	public:
		IntegerEncrypt(tInt value = 0)
		{
			mValue = value ^ vMask;
		}
		operator tInt() const
		{
			return mValue ^ vMask;
		}
		IntegerEncrypt & operator = (IntegerEncrypt const & other)
		{
			mValue = other.mValue;
			return *this;
		}
		IntegerEncrypt & operator *= (tInt value)
		{
			mValue = ((mValue ^ vMask) * value) ^ vMask;
			return *this;
		}
		IntegerEncrypt & operator /= (tInt value)
		{
			mValue = ((mValue ^ vMask) / value) ^ vMask;
			return *this;
		}
		IntegerEncrypt & operator += (tInt value)
		{
			mValue = ((mValue ^ vMask) + value) ^ vMask;
			return *this;
		}
		IntegerEncrypt & operator -= (tInt value)
		{
			mValue = ((mValue ^ vMask) - value) ^ vMask;
			return *this;
		}
	};
	typedef IntegerEncrypt<int64, 0x569A37E84BCF1E69> Int64Ex;
	typedef IntegerEncrypt<uint64, 0x569A37E84BCF1E69> UInt64Ex;
	typedef IntegerEncrypt<int32, 0x569A37E8> Int32Ex;
	typedef IntegerEncrypt<uint32, 0x569A37E8> UInt32Ex;
	typedef IntegerEncrypt<int16, 0x569A> Int16Ex;
	typedef IntegerEncrypt<uint16, 0x569A> UInt16Ex;
	typedef IntegerEncrypt<int8, 0x56> Int8Ex;
	typedef IntegerEncrypt<uint8, 0x56> UInt8Ex;
}

/// class Encryption
namespace jai {
	template<typename tType, typename tRandom>
	inline Encrypt01<tType, tRandom>::Encrypt01(uint32 seed)
	{
		mSeed = seed;
	}
	template<typename tType, typename tRandom>
	inline void Encrypt01<tType, tRandom>::setSeed(uint32 seed)
	{
		mSeed = seed;
	}
	template<typename tType, typename tRandom>
	inline uint32 Encrypt01<tType, tRandom>::getSeed() const
	{
		return mSeed;
	}
	template<typename tType, typename tRandom>
	inline void Encrypt01<tType, tRandom>::encode(ElementType * buffer, uint32 count) const
	{
		RandomType random(mSeed);
		for(uint32 i = 0; i < count; ++i)
		{
			*buffer += (ElementType)random.next();
			*buffer ^= (ElementType)random.next();
			++buffer;
		}
	}
	template<typename tType, typename tRandom>
	inline void Encrypt01<tType, tRandom>::decode(ElementType * buffer, uint32 count) const
	{
		RandomType random(mSeed);
		for(uint32 i = 0; i < count; ++i)
		{
			ElementType temp = (ElementType)random.next();
			*buffer ^= (ElementType)random.next();
			*buffer -= temp;
			++buffer;
		}
	}
}
namespace jai {
	template<typename tType>
	inline Encrypt12s<tType>::Encrypt12s(tType s0, tType s1)
	{
		setSeed(s0, s1);
	}
	template<typename tType>
	inline void Encrypt12s<tType>::setSeed(tType s0, tType s1)
	{
		mSeed0 = s0;
		mSeed1 = s1;
	}
	template<typename tType>
	inline tType Encrypt12s<tType>::getSeed(int which)
	{
		return (which==0?mSeed0:mSeed1);
	}
	template<typename tType>
	inline void Encrypt12s<tType>::encode(ElementType * buffer, uint32 count)
	{
		ElementType code0 = mSeed0;
		ElementType code1 = mSeed1;
		for(int i = 0; i < count; ++i)
		{
			*buffer ^= code0;
			code0 = *buffer;
			*buffer += code1;
			code1 = *buffer;
			++buffer;
		}
		code0 = mSeed1;
		code1 = mSeed0;
		for(int i = 0; i < count; ++i)
		{
			--buffer;
			*buffer ^= code0;
			code0 = *buffer;
			*buffer += code1;
			code1 = *buffer;
		}
	}
	template<typename tType>
	inline void Encrypt12s<tType>::decode(ElementType * buffer, uint32 count)
	{
		ElementType code0 = mSeed1;
		ElementType code1 = mSeed0;
		ElementType temp;
		buffer += count;
		for(int i = 0; i < count; ++i)
		{
			--buffer;
			temp = *buffer;
			*buffer -= code1;
			code1 = temp;
			temp = *buffer;
			*buffer ^= code0;
			code0 = temp;
		}
		code0 = mSeed0;
		code1 = mSeed1;
		for(int i = 0; i < count; ++i)
		{
			temp = *buffer;
			*buffer -= code1;
			code1 = temp;
			temp = *buffer;
			*buffer ^= code0;
			code0 = temp;
			++buffer;
		}
	}
}