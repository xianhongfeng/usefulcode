//
//  LexShared.cpp
//  ILex
//
//  Created by lover on 15/2/27.
//  Copyright (c) 2015年 lover. All rights reserved.
//

#include "LexShared.h"
//#include "cocos2d.h"

// random
namespace Lex {
    jai::Random & GetRandom()
    {
        static jai::Random rand(time(0));
        return rand;
    }
}

// emojs: move realization here
namespace Lex
{
	Emojs::Emojs()
	{
		std::string str =
			"\xf0\x9f\x8d\x92\x20"
			"\xf0\x9f\x8d\x87\x20"
			"\xf0\x9f\x8d\x8e\x20"
			"\xf0\x9f\x8d\x8f\x20"
			"\xf0\x9f\x8d\x8a\x20"
			"\xf0\x9f\x8d\x88\x20"
			"\xf0\x9f\x8d\x91\x20"
			"\xf0\x9f\x8d\x93\x20"
			"\xf0\x9f\x8d\x86\x20"
			"\xf0\x9f\x8d\x90\x20"
			"\xf0\x9f\x8d\x85\x20"
			"\xf0\x9f\x8c\xbd\x20"
			"\xf0\x9f\x8d\x88\x20"
			"\xf0\x9f\x8d\x8b\x20"
			"\xf0\x9f\x92\x9b\x20"
			"\xf0\x9f\x92\x99\x20"
			"\xf0\x9f\x92\x9c\x20"
			"\xf0\x9f\x92\x90\x20"
			"\xf0\x9f\x8c\xb8\x20"
			"\xf0\x9f\x8c\xb7\x20"
			"\xf0\x9f\x8d\x80\x20"
			"\xf0\x9f\x8c\xb9\x20"
			"\xf0\x9f\x8c\xbb\x20"
			"\xf0\x9f\x8c\xba\x20"
			"\xf0\x9f\x8d\x81\x20"
			"\xf0\x9f\x8d\x83\x20"
			"\xf0\x9f\x8d\x82\x20"
			"\xf0\x9f\x8c\xbf\x20"
			"\xf0\x9f\x8c\xbe\x20"
			"\xf0\x9f\x8d\x84\x20"
			"\xf0\x9f\x8c\xb5\x20"
			"\xf0\x9f\x8c\xb4\x20"
			"\xf0\x9f\x8c\xb2\x20"
			"\xf0\x9f\x8c\xb3\x20"
			"\xf0\x9f\x8c\xb0\x20"
			"\xf0\x9f\x8c\xb1\x20"
			"\xf0\x9f\x8c\xbc\x20";

		std::string::size_type index = 0;
		while (true) 
		{
			auto pos = str.find(' ', index);
			if(pos == str.npos) break;
			m_Subs.push_back(str.substr(index, pos - index));
			index = pos + 1;
		}
	}
}

// TextSrc
namespace Lex {
    
    static Lang language;
    
    Lang GetSystemLanguage()
    {
//        cocos2d::ccLanguageType currentLanguageType = cocos2d::CCApplication::sharedApplication()->getCurrentLanguage();
//        if(currentLanguageType == cocos2d::kLanguageChinese)
//            return Lang::Zh;
//        else
            return Lang::En;
    }
    
    Lang GetUsedLanguage()
    {
        return language;
    }
    
    void SetUsedLanguage(Lang lang)
    {
        language = lang;
    }
    
    /* realization @ LexShared.cpp*/
    
    TextSrc::TextSrc(const char * defaultText, ...)
    {
        va_list arg;
        va_start(arg, defaultText);
        char const * str = defaultText;
        while(str != nullptr)
        {
            mTexts.push_back(std::string(str));
            str = va_arg(arg, char const*);
        }
        va_end(arg);
    }
    const char * TextSrc::getCurrentLangText()
    {
        int32_t cur = (int32_t)GetUsedLanguage();
        if(cur <static_cast<int32_t>( mTexts.size()))
            return mTexts[cur].data();
        else
            return mTexts[0].data();
    }
    TextSrc::operator const char * ()
    {
        return getCurrentLangText();
    }
    const char * TextSrc::getText(Lang lang)
    {
        int32_t cur = (int32_t)lang;
        if(cur <static_cast<int32_t>( mTexts.size()))
            return mTexts[cur].data();
        else
            return mTexts[0].data();
    }
}