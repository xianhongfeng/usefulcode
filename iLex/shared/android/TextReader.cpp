#include "../LexShared.h"
#include "platform/android/jni/JniHelper.h"
#include "JniUtils.h"
#include "cocos2d.h"
#include "Login.h"

using namespace cocos2d;

static std::function<void (bool, std::string)> OnEnd;

namespace Lex
{
	std::string text;
	bool change;
	void InputString(std::string title, EInputType type, int length, std::function<void (bool res, std::string)> callback, std::string defaultText)
	{
		OnEnd = callback;
		JniMethodInfo jmi;
		bool ishave = JniHelper::getStaticMethodInfo(jmi, "com/lexgame/utils/LexBundle", "textReader", "(Ljava/lang/String;)V");
		if(ishave)
		{
			CCLog("textReader found");
			jstring jtitle = Lex::JniUtils::stoJstring(jmi.env, title.c_str());
			jmi.env->CallStaticVoidMethod(jmi.classID, jmi.methodID, jtitle);
		}
		else
		{
			CCLog("textReader not have");
		}
	}
	// �ж��Ƿ������ASCII�ַ�
	bool IsContainNonAscii(std::string text)
	{
		return true;
	}
	// ascii��һ���ַ�, ��ascii�������ַ�
	int GetLenght(std::string text)
	{
		return 6;
	}
	std::string GetSubWords(std::string text, int from, int to)
	{
		return "true";
	}
}

extern "C"
{
	void Java_com_lexgame_utils_TextReader_afterInput(JNIEnv *env, jobject *obj, jstring jstr)
	{

		CCLog("afterInput1");
		std::string str = cocos2d::JniHelper::jstring2string(jstr);
		Lex::text = str;
		Lex::change = true;
		OnEnd(true, str);
		CCLog("afterInput2");
	}
}