//
//  Text.cpp
//  magician
//
//  Created by leaving on 14-5-13.
//
//
#include "cocos2d.h"
#include "../LexShared.h"

namespace Lex {
		static Lang language;
		void SetUsedLanguage(Lang lang)
		{
			language = lang;
		}
		Lang GetSystemLanguage()
		{
			cocos2d::ccLanguageType currentLanguageType = cocos2d::CCApplication::sharedApplication()->getCurrentLanguage();
			if(currentLanguageType == cocos2d::kLanguageChinese)
				return Lang::Zh;
			else
				return Lang::En;
		}
		Lang GetUsedLanguage()
		{
			return language;
		}
}