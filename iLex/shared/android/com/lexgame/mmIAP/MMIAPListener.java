 package com.lexgame.mmIAP;

import java.util.HashMap;

import mm.purchasesdk.OnPurchaseListener;
import mm.purchasesdk.Purchase;
import mm.purchasesdk.core.PurchaseCode;

import org.cocos2dx.lib.Cocos2dxActivity;
import org.json.JSONException;
import org.json.JSONObject;

import com.lexgame.utils.LexBundle;
//import com.umeng.analytics.MobclickAgent;

import android.content.Context;
import android.app.Activity;
import android.os.Message;
import android.util.Log;

public class MMIAPListener implements OnPurchaseListener {

	private final String TAG = "LexIAP";
	private Context context;
	private MMIAPHandler iapHandler;

	public MMIAPListener(Context thiz, MMIAPHandler iapHandler) {
		this.context = thiz;
		this.iapHandler = iapHandler;
	}
	

	@Override
	public void onAfterApply() {

	}

	@Override
	public void onAfterDownload() {

	}

	@Override
	public void onBeforeApply() {

	}

	@Override
	public void onBeforeDownload() {

	}

	@Override
	public void onInitFinish(int code) {
		Log.d(TAG, "Init finish, status code = " + code);
		Message message = iapHandler.obtainMessage(MMIAPHandler.INIT_FINISH);
		String result = "初始化结果：" + Purchase.getReason(code);
		message.obj = result;
		message.sendToTarget();
		JSONObject object = new JSONObject();
		Log.i(TAG, "onInitFinish");
		try {
			object.put("msg", result);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.i(TAG, e.getMessage());
		}
		if(result.contains("成功")){
			InitFinishCallback(object.toString() ,true);
		}
		else
			InitFinishCallback(object.toString(), false);
		Log.i(TAG, "InitFinishCallback");
	}

	@Override
	public void onBillingFinish(int code, HashMap arg1) {
		Log.d(TAG, "billing finish, status code = " + code);
		String result = "订购结果：订购成功";
		//JSONArray jarray = new JSONArray();
		Message message = iapHandler.obtainMessage(MMIAPHandler.BILL_FINISH);
		// 此次订购的orderID
		String orderID = null;
		// 商品的paycode
		String paycode = null;
		// 商品的有效期(仅租赁类型商品有效)
		String leftday = null;
		// 商品的交易 ID，用户可以根据这个交易ID，查询商品是否已经交易
		String tradeID = null;
		
		String ordertype = null;
		Log.i("LexAndroid", "onBillingFinish");
		try{
			if (code == PurchaseCode.ORDER_OK || (code == PurchaseCode.AUTH_OK) ||(code == PurchaseCode.WEAK_ORDER_OK)) {
				/**
				 * 商品购买成功或者已经购买。 此时会返回商品的paycode，orderID,以及剩余时间(租赁类型商品)
				 */
				JSONObject object = new JSONObject();
				if (arg1 != null) {
					leftday = (String) arg1.get(OnPurchaseListener.LEFTDAY);
					if (leftday != null && leftday.trim().length() != 0) {
						result = result + ",剩余时间 ： " + leftday;
						object.put("LeftDay", leftday);
					}
					orderID = (String) arg1.get(OnPurchaseListener.ORDERID);
					if (orderID != null && orderID.trim().length() != 0) {
						result = result + ",OrderID ： " + orderID;
						object.put("OrderID", orderID);
					}
					paycode = (String) arg1.get(OnPurchaseListener.PAYCODE);
					if (paycode != null && paycode.trim().length() != 0) {
						result = result + ",Paycode:" + paycode;
						object.put("Paycode", paycode);
					}
					tradeID = (String) arg1.get(OnPurchaseListener.TRADEID);
					if (tradeID != null && tradeID.trim().length() != 0) {
						result = result + ",tradeID:" + tradeID;
						object.put("TradeID", tradeID);
					}
					ordertype = (String) arg1.get(OnPurchaseListener.ORDERTYPE);
					if (tradeID != null && tradeID.trim().length() != 0) {
						result = result + ",ORDERTYPE:" + ordertype;
						object.put("OrderType", ordertype);
					}
				}
				//umeng
				HashMap<String,String> map = new HashMap<String, String>();
				//map.put("deviceId", LexBundle.getCodeStatic());
				map.put("paycode:", paycode);
				//MobclickAgent.onEvent(context,"Pay", map);
				
				BillingFinishCallback(paycode ,true);				
			} else {
				/**
				 * 表示订购失败。
				 */
				result = "订购结果：" + Purchase.getReason(code);
				JSONObject object = new JSONObject();
				object.put("msg", result);
				BillingFinishCallback(result, false);
			}
		}catch(JSONException ex){
			JSONObject object = new JSONObject();
			try{
				object.put("msg", "Json exception" + ex.getMessage());
			}catch(JSONException ex1)
			{}
			BillingFinishCallback(ex.getMessage(), false);
		}
		//context.dismissProgressDialog();
		//System.out.println(result);

	}

	@Override
	public void onQueryFinish(int code, HashMap arg1) {
		Log.d(TAG, "license finish, status code = " + code);
		Message message = iapHandler.obtainMessage(MMIAPHandler.QUERY_FINISH);
		String result = "查询成功,该商品已购买";
		// 此次订购的orderID
		String orderID = null;
		// 商品的paycode
		String paycode = null;
		// 商品的有效期(仅租赁类型商品有效)
		String leftday = null;
		if (code != PurchaseCode.QUERY_OK) {
			/**
			 * 查询不到商品购买的相关信息
			 */
			result = "查询结果：" + Purchase.getReason(code);
		} else {
			/**
			 * 查询到商品的相关信息。
			 * 此时你可以获得商品的paycode，orderid，以及商品的有效期leftday（仅租赁类型商品可以返回）
			 */
			leftday = (String) arg1.get(OnPurchaseListener.LEFTDAY);
			if (leftday != null && leftday.trim().length() != 0) {
				result = result + ",剩余时间 ： " + leftday;
			}
			orderID = (String) arg1.get(OnPurchaseListener.ORDERID);
			if (orderID != null && orderID.trim().length() != 0) {
				result = result + ",OrderID ： " + orderID;
			}
			paycode = (String) arg1.get(OnPurchaseListener.PAYCODE);
			if (paycode != null && paycode.trim().length() != 0) {
				result = result + ",Paycode:" + paycode;
			}
		}
		System.out.println(result);
		//context.dismissProgressDialog();
	}

	

	@Override
	public void onUnsubscribeFinish(int code) {
		// TODO Auto-generated method stub
		String result = "退订结果：" + Purchase.getReason(code);
		System.out.println(result);
		//context.dismissProgressDialog();
	}
	public native void BillingFinishCallback(String json, boolean result);
	public native void InitFinishCallback(String json, boolean result);
}
