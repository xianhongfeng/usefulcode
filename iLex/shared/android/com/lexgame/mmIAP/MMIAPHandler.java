package com.lexgame.mmIAP;

import mm.purchasesdk.Purchase;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

public class MMIAPHandler extends Handler {
	public static final int MMIAP_INIT = 0;
	public static final int MMIAP_ORDER = 1;
	
	public static final int INIT_FINISH = 10000;
	public static final int BILL_FINISH = 10001;
	public static final int QUERY_FINISH = 10002;
	public static final int UNSUB_FINISH = 10003;
	private static final String TAG = "LexIAP";

	private Context context;

	
	public MMIAPHandler(Activity thiz) {
		this.context = thiz;
	}

	@Override
	public void handleMessage(Message msg) {
		super.handleMessage(msg);
		int what = msg.what;
		switch (what) {
		case INIT_FINISH:
			initShow((String) msg.obj);
			//context.dismissProgressDialog();
			break;
		case MMIAP_INIT:
			MMAccount acc = (MMAccount)msg.obj;
			initMMIAP(acc.appID, acc.appKey);
			break;
		case MMIAP_ORDER:
			try{
				Log.i(TAG, "2222222");
				Log.i(TAG, "handleMessage order");
				MMIAP.purchase.order(context, (String)msg.obj, MMIAP.mListener);
				Log.i("LexAndroid", (String)msg.obj);
			}catch(Exception ex){
				Log.e(TAG, ex.getMessage());
			}
			break; 
		default:
			Log.i(TAG, "default");
			break; 
		}
	}
	
	
	private void initMMIAP(String appID, String appKey){
			/**
			 * IAP组件初始化.包括下面3步。
			 */
			/**
			 * step1.实例化PurchaseListener。实例化传入的参数与您实现PurchaseListener接口的对象有关。
			 * 例如，此Demo代码中使用IAPListener继承PurchaseListener，其构造函数需要Context实例。
			 */
			MMIAP.mListener = new MMIAPListener(context, MMIAP.iapHandler);
			Log.i(TAG, "mListener");
			/**
			 * 
			 * step2.获取Purchase实例。
			 */
			MMIAP.purchase = Purchase.getInstance();
			Log.i(TAG, "purchase");
		/**
		 * step3.向Purhase传入应用信息。APPID，APPKEY。 需要传入参数APPID，APPKEY。 APPID，见开发者文档
		 * APPKEY，见开发者文档
		 */
		try {
			MMIAP.purchase.setAppInfo(appID, appKey);
			Log.i(TAG, "setAppInfo");
		} catch (Exception e1) {
			Log.i(TAG, e1.getMessage());
		}
		/**
		 * step4. IAP组件初始化开始， 参数PurchaseListener，初始化函数需传入step1时实例化的
		 * PurchaseListener。
		 */
		try {
			MMIAP.purchase.init(context, MMIAP.mListener);
			Log.i(TAG, "init");
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
	}

	private void initShow(String msg) {
		//Toast.makeText(context, "初始化：" + msg, Toast.LENGTH_LONG).show();
	}

//	public void showDialog(String title, String msg) {
//		AlertDialog.Builder builder = new AlertDialog.Builder(context);
//		builder.setTitle(title);
//		builder.setIcon(context.getResources().getDrawable(R.drawable.icon));
//		builder.setMessage((msg == null) ? "Undefined error" : msg);
//		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//			public void onClick(DialogInterface dialog, int whichButton) {
//				dialog.dismiss();
//			}
//		});
//		builder.create().show();
//	}
	
}
