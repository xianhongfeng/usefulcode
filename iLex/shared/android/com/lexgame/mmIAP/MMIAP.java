/****************************************************************************
Copyright (c) 2010-2011 cocos2d-x.org

http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
package com.lexgame.mmIAP;

import mm.purchasesdk.Purchase;

import org.cocos2dx.lib.Cocos2dxActivity;
import org.cocos2dx.lib.Cocos2dxGLSurfaceView;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.Menu;

public class MMIAP{
	
	public static final int ITEM0 = Menu.FIRST;// 系统值
	public static final int ITEM1 = 2;
	public static final int ITEM2 = 3;
	private final static String TAG = "LexIAP";

	public static Purchase purchase;
	public static MMIAPHandler iapHandler;
//	private Context context;
	public static MMIAPListener mListener;
	
	// 计费信息
	// 计费信息 (现网环境)
	// 【】需要加密
	
	private static Context context;
	
    public MMIAP(Context thiz){
    	context = thiz;
		MMIAP.iapHandler = new MMIAPHandler((Activity) context);
		//purchase = Purchase.getInstance();
	}
    
    public static void createOrderStatic(String _paycode){
    	Log.i(TAG, "createOrderStatic: _paycode=" + _paycode);
    	Log.i(TAG, "11111");
    	Message msg = iapHandler.obtainMessage(MMIAPHandler.MMIAP_ORDER);
    	msg.obj = _paycode;
    	iapHandler.sendMessage(msg);
    }
    
    public static void initMMIAPStatic(String appID, String appKey){
    	Log.i(TAG, appID);
    	Message msg = iapHandler.obtainMessage(MMIAPHandler.MMIAP_INIT);
    	MMAccount acc = new MMAccount();
    	acc.appID = appID;
    	acc.appKey = appKey;
    	msg.obj = acc;
    	iapHandler.sendMessage(msg);
    }
}
