package com.lexgame.game;

import org.cocos2dx.lib.Cocos2dxActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.lexgame.dialog.AlertMsg;
import com.lexgame.utils.Datetime;

public class LexActivity extends Cocos2dxActivity {
	public static long gameTime = 0;
	public static long startTime = 0;
	public static Handler handler;
	public static final int NO_CALLBACK = 0;
	public static final int ONE_CALLBACK = 1;
	public static final int TWO_CALLBACKS = 2;
	public static final int THREE_CALLBACKS = 3;
	//unimplemented
	public static final int FOUR_CALLBACKS = 4;
	public static final String TAG = "SHUIHU";
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		startTime = System.currentTimeMillis();
		handler = new Handler(){
			AlertDialog.Builder builder = new AlertDialog.Builder(LexActivity.this);
			AlertMsg amsg = null;
			@Override
			public void handleMessage(Message msg){
				switch(msg.what){
				case NO_CALLBACK:
					amsg = (AlertMsg)msg.obj;
					builder.setTitle(amsg.title);
					builder.setMessage(amsg.message);
		            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
		                public void onClick(DialogInterface dialog, int whichButton) {
		                    //这里添加点击后的逻辑
		                	Log.i("testAlert", "NowhichButton=" + whichButton);
		                }
		            });
					break;
				case ONE_CALLBACK:
					amsg = (AlertMsg)msg.obj;
					builder.setTitle(amsg.title);
					builder.setMessage(amsg.message);
		            builder.setPositiveButton(amsg.BtnName[0], new DialogInterface.OnClickListener() {
		                public void onClick(DialogInterface dialog, int whichButton) {
		                    //这里添加点击后的逻辑
		                	Log.i("testAlert", "OnewhichButton=" + whichButton);
		                	runOnGLThread(new Runnable() {
								public void run() {
									Log.i("testAlert", "runnable");
									oneCallBack();
								}
							});
		                }
		            });
					break;
				case TWO_CALLBACKS:
					amsg = (AlertMsg)msg.obj;
					builder.setTitle(amsg.title);
					builder.setMessage(amsg.message);
		            builder.setPositiveButton(amsg.BtnName[0], new DialogInterface.OnClickListener() {
		                public void onClick(DialogInterface dialog, int whichButton) {
		                    //这里添加点击后的逻辑
		                    //showDialog("提示", amsg.BtnName[0]);
		                	runOnGLThread(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									twoCallBack0();
								}
							});
		                }
		            });
		            builder.setNegativeButton(amsg.BtnName[1], new DialogInterface.OnClickListener() {
		                public void onClick(DialogInterface dialog, int whichButton) {
		                    //这里添加点击后的逻辑
		                	runOnGLThread(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									twoCallBack1();
								}
							});
		                }
		            });
					break;
				case THREE_CALLBACKS:
					amsg = (AlertMsg)msg.obj;
					builder.setTitle(amsg.title);
					builder.setMessage(amsg.message);
		            builder.setPositiveButton(amsg.BtnName[0], new DialogInterface.OnClickListener() {
		                public void onClick(DialogInterface dialog, int whichButton) {
		                    //这里添加点击后的逻辑
		                	runOnGLThread(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									threeCallBack0();
								}
							});
		                }
		            });
		            builder.setNegativeButton(amsg.BtnName[1], new DialogInterface.OnClickListener() {
		                public void onClick(DialogInterface dialog, int whichButton) {
		                    //这里添加点击确定后的逻辑
		                	runOnGLThread(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									threeCallBack1();
								}
							});
		                }
		            });
		            builder.setNegativeButton(amsg.BtnName[2], new DialogInterface.OnClickListener() {
		                public void onClick(DialogInterface dialog, int whichButton) {
		                    //这里添加点击后的逻辑
		                	runOnGLThread(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									threeCallBack2();
								}
							});
		                }
		            });
					break;
				case FOUR_CALLBACKS:
					//【】unimplemented
					break;
				default:
					break;
				}
				builder.create().show();
			}
		};
		Log.d(TAG, "Create ok");
		//调试
//		MobclickAgent.setDebugMode( true );
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Log.d(TAG, "Resume ok");
		startTime = System.currentTimeMillis();
//		MobclickAgent.onResume(this);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		Log.d(TAG, "Pause ok");
		gameTime += System.currentTimeMillis() - startTime;
//		MobclickAgent.onPause(this);
	}

	// called from cpp
    public static void noCBAlert(String message, String title){
    	Log.i("testAlert", "noCBAlert");
    	Message msg = handler.obtainMessage(NO_CALLBACK);
    	AlertMsg amsg = new AlertMsg();
    	amsg.message = message;
    	amsg.title = title;
    	msg.obj = amsg;
    	handler.sendMessage(msg);
    }
    
    public static void oneCBAlert(String message, String title, String btn0){
    	Log.i("testAlert", "oneCBAlert");
    	Message msg = handler.obtainMessage(ONE_CALLBACK);
    	AlertMsg amsg = new AlertMsg();
    	amsg.message = message;
    	amsg.title = title;
    	amsg.BtnName = new String[1];
    	amsg.BtnName[0] = btn0;
    	msg.obj = amsg;
    	handler.sendMessage(msg);
    }
    
    public static void twoCBAlert(String message, String title, String btn0, String btn1){
    	Message msg = handler.obtainMessage(TWO_CALLBACKS);
    	AlertMsg amsg = new AlertMsg();
    	amsg.message = message;
    	amsg.title = title;
    	amsg.BtnName = new String[2];
    	amsg.BtnName[0] = btn0;
    	amsg.BtnName[1] = btn1;
    	msg.obj = amsg;
    	handler.sendMessage(msg);
    }
    
    public static void threeCBAlert(String message, String title, 
    		String btn0, String btn1, String btn2){
    	Message msg = handler.obtainMessage(THREE_CALLBACKS);
    	AlertMsg amsg = new AlertMsg();
    	amsg.message = message;
    	amsg.title = title;
    	amsg.BtnName = new String[3];
    	amsg.BtnName[0] = btn0;
    	amsg.BtnName[1] = btn1;
    	amsg.BtnName[2] = btn2;
    	msg.obj = amsg;
    	handler.sendMessage(msg);
    }

    public static void fourCBAlert(String message, String title, 
    		String btn0, String btn1, String btn2, String btn3){
    	Message msg = handler.obtainMessage(FOUR_CALLBACKS);
    	AlertMsg amsg = new AlertMsg();
    	amsg.message = message;
    	amsg.title = title;
    	amsg.BtnName = new String[4];
    	amsg.BtnName[0] = btn0;
    	amsg.BtnName[1] = btn1;
    	amsg.BtnName[2] = btn2;
    	amsg.BtnName[3] = btn3;
    	msg.obj = amsg;
    	handler.sendMessage(msg);
    }
	
	 // native methods
    public native void oneCallBack();
    public native void twoCallBack0();
    public native void twoCallBack1();
    public native void threeCallBack0();
    public native void threeCallBack1();
    public native void threeCallBack2();
}
