/****************************************************************************
Copyright (c) 2010-2011 cocos2d-x.org

http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
package com.lexgame.shuihu;

import java.util.HashMap;
import java.util.Map;

import net.youmi.android.AdManager;
import net.youmi.android.spot.SpotManager;

import org.cocos2dx.lib.Cocos2dxGLSurfaceView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;

import c.g.m.MIXView;
import cn.waps.AppConnect;
import cn.waps.UpdatePointsNotifier;

import com.dlnetwork.Dianle;
import com.lexgame.addwall.DL;
import com.lexgame.addwall.WP;
import com.lexgame.dialog.AlertMsg;
import com.lexgame.game.LexActivity;
import com.lexgame.mmIAP.MMIAP;
import com.lexgame.utils.FileUtils;
import com.lexgame.utils.LexBundle;
import com.lexgame.utils.URLContent;
import com.lexgame.screenadd.Ad;
import com.lexgame.screenadd.youmi;
import com.umeng.analytics.MobclickAgent;

public class shuihu extends LexActivity {
//	public static Handler handler;
//	public static final int ON_PAY = 0;
//
	PowerManager.WakeLock wl = null;
	private void acquireWakeLock() {
		  if (null == wl) {
			   PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
			   wl = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK
			     | PowerManager.ON_AFTER_RELEASE, getClass()
			     .getCanonicalName());
			   if (null != wl) {
				   wl.acquire();
			   }
		  }
	 }

		 // 释放设备电源锁
	
	 private void releaseWakeLock() {
		  if (null != wl && wl.isHeld()) {
		   //Log.i(TAG, "call releaseWakeLock");
			  wl.release();
			  wl = null;
		  }
	 }
	
    @Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		LexBundle.clearNotifyStatic();
		acquireWakeLock();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		releaseWakeLock();
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		releaseWakeLock();
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		acquireWakeLock();
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		releaseWakeLock();
	}

	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		LexBundle.clearNotifyStatic();
		//acquireWakeLock();
	}

	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		AppConnect.getInstance("c4a249a9ed4352031c7ab0d398b46469", "default", this);
		Dianle.initGoogleContext(this, "bb417fde9adfab33b29bb89fc497b688");
		MIXView mixView=MIXView.initWithID("113044366a38d081", this);
		AdManager.getInstance(this).init("85aa56a59eac8b3d",
				"a14006f66f58d5d7", false);
		WP wp = new WP(this);
		DL dl = new DL(this);
		Ad ad = new Ad(this, mixView);
		youmi youmiad = new youmi(this);
		MMIAP iap = new MMIAP(this);
		FileUtils fu = new FileUtils(this);
		LexBundle lb = new LexBundle(this);
		LexBundle.clearNotifyStatic();
		URLContent content = new URLContent();
		int i= 0;
//		handler = new Handler()
//  	    {
//  	    @Override
//  	    public void handleMessage(Message msg)
//  	        {
//  	    	super.handleMessage(msg);
//  	            switch(msg.what)
//  	            {
//  	                case ON_PAY :
////  	                	payInfo mInfo = (payInfo)msg.obj;
////  	                	Intent intent = new Intent(shuihu.this, SdkShowAllActivity.class);
////  	                	intent.putExtra("costs", (String)mInfo.payCost);
////  	                	intent.putExtra("codes", (String)mInfo.payCode);
////  	                    startActivity(intent);
//  	                    break;
//  	                default:
//  	                    Log.i("LexGame","3333");
//  	                    break;
//  	            }
//  	            
//  	        }
//  	    };
	}
//
    public Cocos2dxGLSurfaceView onCreateView() {
    	Cocos2dxGLSurfaceView glSurfaceView = new Cocos2dxGLSurfaceView(this);
    	// shuihu should create stencil buffer
    	glSurfaceView.setEGLConfigChooser(5, 6, 5, 0, 16, 8);
    	
    	return glSurfaceView;
    }

    private long mkeyTime = 0;
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //二次返回退出
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if ((System.currentTimeMillis() - mkeyTime) > 2000) {
                mkeyTime = System.currentTimeMillis();
                Toast.makeText(this, "再按一次退出游戏", Toast.LENGTH_LONG).show();
            } else {
            	//umeng
            	Map<String, String> map_value = new HashMap<String, String>();
            	map_value.put("deviceId", LexBundle.getCodeStatic());
            	map_value.put("gameTime", LexActivity.gameTime+"");
            	MobclickAgent.onEventValue(this,"gameTime", map_value, (int) (LexActivity.gameTime / 60000));
            	MobclickAgent.onKillProcess(this);
            	
                finish();
                AppConnect.getInstance(this).close();
                System.exit(0);
            }
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }
    
    
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) { 
      if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
        /*
         * 注意在if判断中要加一个event.getAction() == KeyEvent.ACTION_DOWN判断，
         * 因为按键有两个事件ACTION_DOWN和ACTION_UP，也就是按下和松开，
         * 如果不加这个判断，代码会执行两遍
         */
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
        	if ((System.currentTimeMillis() - mkeyTime) > 2000) {
                mkeyTime = System.currentTimeMillis();
                Toast.makeText(this, "再按一次退出游戏", Toast.LENGTH_LONG).show();
            } else {
            	//umeng
            	Map<String, String> map_value = new HashMap<String, String>();
            	map_value.put("deviceId", LexBundle.getCodeStatic());
            	map_value.put("gameTime", LexActivity.gameTime+"");
            	MobclickAgent.onEventValue(this,"gameTime", map_value, (int) (LexActivity.gameTime / 60000));
            	MobclickAgent.onKillProcess(this);
            	
                finish();
                AppConnect.getInstance(this).close();
                System.exit(0);
            }
        }
        return true;
      }
      return super.dispatchKeyEvent(event);
    }
//    
//    public static void createOrderStatic(String _paycode, String _paycost){
//    	Message msg = new Message();
//        msg.what = ON_PAY ;
//        payInfo info = new payInfo();
//        info.payCode = _paycode;
//        info.payCost = _paycost;
//        msg.obj = info;
//        handler.sendMessage(msg);
//    }	

    static {
        System.loadLibrary("hellocpp");
    }
}
