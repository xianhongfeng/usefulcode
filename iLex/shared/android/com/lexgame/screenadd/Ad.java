package com.lexgame.screenadd;

import c.g.m.MIXView;
import android.app.Activity;
import android.content.Context;
import android.os.Message;

public class Ad {
	
	private static AdHandler adHandler;
	
	public Ad(Context context, MIXView mixview)
	{	
		adHandler = new AdHandler((Activity)context, mixview);
	}
	
	public static void showMix()
	{
		Message msg = new Message();
		msg.what = AdHandler.MIX_SHOW;
		adHandler.sendMessage(msg);
	}

}
