package com.lexgame.screenadd;

import c.g.m.MIXView;

import com.dlnetwork.Dianle;
import com.lexgame.shuihu.shuihu;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class AdHandler extends Handler{
	public static final int MIX_SHOW = 0;
	private Context context;
	private MIXView mixview;
	
	public AdHandler(Activity thiz, MIXView mixview)
	{
		this.context = thiz;
		this.mixview = mixview;
	}
	
	@Override
	public void handleMessage(Message msg) {
		super.handleMessage(msg);
		int what = msg.what;
		switch (what) {
		case MIX_SHOW:
			mixview.showAd((Activity)context,"default");
			break;		
		default:
			break; 
		}
	}

}
