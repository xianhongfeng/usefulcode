package com.lexgame.screenadd;

import c.g.m.MIXView;
import android.app.Activity;
import android.content.Context;
import android.os.Message;
import android.util.Log;

public class youmi {
	
	private static youmiHandler adHandler;
	
	public youmi(Context context)
	{	
		adHandler = new youmiHandler((Activity)context);
	}
	
	public static void showYoumi()
	{
		Message msg = new Message();
		msg.what = youmiHandler.YOUMI_SHOW;
		adHandler.sendMessage(msg);
	}

}
