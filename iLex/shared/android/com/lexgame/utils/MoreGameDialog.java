package com.lexgame.utils;

import com.lexgame.shuihu.R;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MoreGameDialog extends Dialog implements android.view.View.OnClickListener {
	
	private TextView title;
	private ImageButton imgbtn;
	
	public MoreGameDialog(Context context, String url) {
		super(context);
		// TODO Auto-generated constructor stub
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.lextitilebar);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.lextitilebar);		
		imgbtn = (ImageButton)findViewById(R.id.titleButton);
		title = (TextView)findViewById(R.id.bartitle);
		//title.setText(R.string.more_game);
		LinearLayout layout = new LinearLayout(getContext());
		layout.setOrientation(LinearLayout.HORIZONTAL);
		
		WindowManager m = this.getWindow().getWindowManager();
		Display d = m.getDefaultDisplay();  //为获取屏幕宽、高    
		android.view.WindowManager.LayoutParams p = getWindow().getAttributes();  //获取对话框当前的参数值    
		p.height = (int) (d.getHeight() * 0.9);   //高度设置为屏幕的0.9 
		p.width = (int) (d.getWidth() * 0.9);    //宽度设置为屏幕的0.9  
//		p.alpha = 1.0f;      //设置本身透明度  
//		p.dimAmount = 0.0f;      //设置黑暗度 -
		getWindow().setAttributes(p);
		
		imgbtn.setOnClickListener(this);
	    WebView webview = new WebView(context);
		webview.setWebViewClient(new WebViewClient() {   
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl)   
            {                 // Handle the error       
                 
            }             
                   
            public boolean shouldOverrideUrlLoading(WebView view, String url) 
            {   
            	
            	if (openWithWevView(url)) {  
                    view.loadUrl(url);  
                 }else{  
                    Uri uri = Uri.parse(url); //url为你要链接的地址  
                    Intent intent =new Intent(Intent.ACTION_VIEW, uri);  
                    LexBundle.activity.startActivity(intent);
                 }  
                return true;
            }

			private boolean openWithWevView(String url) {
				// TODO Auto-generated method stub
				//Toast.makeText(context, url, Toast.LENGTH_SHORT).show();
				if(url.endsWith(".apk"))
					return false;
				else
					return true;
			}
        });
		webview.loadUrl(url);
		layout.addView(webview);
		//addContentView(layout, new LinearLayout.LayoutParams(display.getWidth() - ((int) (dimensions[0] * scale + 0.5f)), display.getHeight() - ((int) (dimensions[1] * scale + 0.5f))))
		setContentView(layout);
	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		if(view.getId() == R.id.titleButton){
			this.dismiss();
			LexBundle.activity.runOnGLThread(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					
				}
			});
		}
	}
	
}
