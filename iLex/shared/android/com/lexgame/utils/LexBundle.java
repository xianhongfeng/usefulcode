package com.lexgame.utils;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.cocos2dx.lib.Cocos2dxActivity;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.sax.StartElementListener;
import android.telephony.TelephonyManager;
import android.content.ClipboardManager;
import android.util.Log;

import com.lexgame.shuihu.R;

/**
 * @author 
 *
 */
@SuppressLint("NewApi")
public class LexBundle {
	public static Cocos2dxActivity activity;
	private static Handler handler = null;
	private static final int COPY_CLIPBOARD = 1;
	private static final int SHOW_MOREGAME = 2;
	public static Timer mtimer = new Timer();
	public LexBundle(Cocos2dxActivity actvt){
		activity = actvt;
		handler = new Handler(){

			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				super.handleMessage(msg);
				switch(msg.what)
				{
				case COPY_CLIPBOARD:
					ClipboardManager cm = (ClipboardManager)activity.getSystemService(Context.CLIPBOARD_SERVICE);
					cm.setPrimaryClip(ClipData.newPlainText(null, (String)msg.obj));
					break;
				case SHOW_MOREGAME:
//					Intent intent = new Intent (activity, MoreGameActivity.class);
//					intent.putExtra("url", (String)msg.obj);
//					activity.startActivity(intent);
					MoreGameDialog mgd = new MoreGameDialog(activity, (String)msg.obj);
					mgd.show();
					break;
					default:
						break;
				}
			}
			
		};
	}
	
	public static String getCodeStatic(){
		final TelephonyManager tm = (TelephonyManager)activity.getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
		return tm.getDeviceId();
	}
	
	/**
	 * @param delay 显示延时
	 * @param str 内容
	 * @param title 标题
	 * @param clazzname	类名
	 */
	@SuppressLint("NewApi")
	public static void sendNotifyStatic(final int delay, final String str, final String title, final String clazzname){
		mtimer.purge();
		mtimer.schedule(new TimerTask(){
			@SuppressLint("NewApi")
			@Override
			public void run() {                  
				NotificationManager manager = (NotificationManager)activity.getSystemService(Context.NOTIFICATION_SERVICE);
				//manager.cancel(R.string.app_name);
				Class clazz = null;
				try {
					clazz = Class.forName(clazzname);
					Intent intent = new Intent(activity, clazz);
					PendingIntent pi= PendingIntent.getActivity(activity, 0, intent, 0);
					Notification.Builder builder = new Notification.Builder(activity);
					builder.setSmallIcon(R.drawable.notify)
						.setWhen(System.currentTimeMillis() + delay * 1000)
						.setDefaults(Notification.DEFAULT_LIGHTS)
						.setContentTitle(title)
						.setContentText(str)
						.setContentIntent(pi);
					Notification notify = builder.getNotification();
					manager.notify(R.string.app_name, notify);
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
		}, delay * 1000);
		
		
	}
	
	/**
	 * 清除消息
	 */
	public static void clearNotifyStatic(){
		NotificationManager manager = (NotificationManager)activity.getSystemService(Context.NOTIFICATION_SERVICE);
		manager.cancel(R.string.app_name);
	}
	
	/**
	 * @param str 要拷贝的数据
	 */
	@SuppressLint("NewApi") 
	public static void copyClipboardStatic(String str){
		Log.i("androidBundle", str);
		Message msg = handler.obtainMessage(COPY_CLIPBOARD);
		msg.obj = str;
		handler.sendMessage(msg);
	}
	
	public static void openLinkStatic(String link){
		Uri uri = Uri.parse(link);
		Intent intent = new Intent(Intent.ACTION_VIEW, uri);
		activity.startActivity(intent);
	}
	
	public static void moreGameStatic(String url) {
    	Message msg = handler.obtainMessage(SHOW_MOREGAME);
    	msg.obj = url;
    	handler.sendMessage(msg);
    }

	public static boolean sendEmailOutAppStatic(String address, String title, String message){
		Intent data=new Intent(Intent.ACTION_SENDTO); 
		data.setData(Uri.parse("mailto:" + address)); 
		data.putExtra(Intent.EXTRA_SUBJECT, title); 
		data.putExtra(Intent.EXTRA_TEXT, message); 
		activity.startActivity(data);
		return true;
	}
}
