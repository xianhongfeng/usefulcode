package com.lexgame.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.util.Log;

public class FileUtils {
	
	public static Activity activity = null;
	
	public FileUtils(Activity actvt){
		activity = actvt;
	}
	
	public static String getResPathStatic(String filename){
		Resources res = activity.getResources();
		AssetManager am = res.getAssets();
		String path = "/asserts/icon";

		Log.i("Java Lex Game File Utills getResPathStatic", path);

		try {
			String[] files = am.list("icon");
			for(String file : files){
				if(file.equals(filename))
				Log.i("androidBundle", "file: " + file);
				break;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return path;
	}
	
	public static String getDocPathStatic(String filename){
		Log.i("Java Lex Game File Utills getDocPathStatic:begin get doc", filename);
		File priFile = activity.getFilesDir();
		Log.i("Java Lex Game File Utills getDocPathStatic:", "canWrite," + priFile.canWrite() + "," + priFile.getParent());
		return new String(priFile.getPath() + "/" + filename);
	}
	
	public static boolean writeFileStatic(String filename, String data, int size){
		FileOutputStream outStream = null;
		Log.i("androidBundle", data);
		boolean result = true;
		try {
			outStream = activity.openFileOutput(filename, Context.MODE_PRIVATE);
			outStream.write(data.getBytes(), 0, data.length());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = false;
		} finally{
			try {
				outStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
	}
	
	public static String loadFileStatic(String filename){
		FileInputStream inStream = null;
		byte[] buffer = null;
		try {
			inStream = activity.openFileInput(filename);
			int length = inStream.available();
			buffer = new byte[length];
			inStream.read(buffer);
			String str = new String(buffer);
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.i("Java Lex Game File Utills Load", "FileNotFoundException");
			//return buffer;
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.i("Java Lex Game File Utills Load", "IOException");
		} finally{
			try {
				inStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return new String(buffer);
	}
}
