package com.lexgame.utils;

import java.io.BufferedInputStream;  
import java.io.BufferedReader;
import java.io.DataOutputStream;  
import java.io.InputStream;  
import java.io.InputStreamReader;  
import java.io.Reader;  
import java.net.HttpURLConnection;  
import java.net.URL;  
import java.net.URLEncoder;  
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.util.Log;
  
public class URLContent {  
	private final String myurl = "http://www.ariesgames.net/switch/mm_shuihu_1.0.2.html";
    public URLContent()
    {  
    	StringBuffer res = new StringBuffer();
		HttpURLConnection conn = null;
		try {
			URL serverUrl = new URL(myurl);
			conn = (HttpURLConnection) serverUrl.openConnection();
			conn.setRequestMethod("GET");// "POST" ,"GET"
			conn.setConnectTimeout(10000);
			conn.setReadTimeout(10000);
			conn.connect();
			InputStream ins = conn.getInputStream();
			String charset = "UTF-8";
			InputStreamReader inr = new InputStreamReader(ins, charset);
			BufferedReader bfr = new BufferedReader(inr);
			String line = "";
			do {
				res.append(line);
				line = bfr.readLine();
			} while (line != null);
			inr.close();
			bfr.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
		Pattern pattern = Pattern.compile("pmtips[\\w\\W].*?<h3>(.*?)</h3>");
		//Matcher matcher = pattern.matcher(res);
		System.out.println(res);
		String str = res.toString();
		Log.i("print", str);
		Log.i("print", "b");
		sendContent(str);
//		if (matcher.find()) {
//			String ret = matcher.group(1);
//			Log.i("print",ret);
//			//sendContent(ret);
//			//System.out.println(ret);
//		}
    }

    public native void sendContent(String content);
}  