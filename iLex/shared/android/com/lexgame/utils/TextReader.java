package com.lexgame.utils;

import java.util.Timer;
import java.util.TimerTask;

import com.lexgame.shuihu.R;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;



public class TextReader extends Activity{
	
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE |
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN); 
		setContentView(R.layout.textreader);	
		final EditText text = (EditText)findViewById(R.id.text);
		text.setFocusable(true);
		text.setFocusableInTouchMode(true);
		text.requestFocus();  
		
		//text.setInputType(InputType.TYPE_NULL);

      
		Timer timer = new Timer();
	    timer.schedule(new TimerTask()
	     {
	         public void run() 
	         {
	        	 InputMethodManager inputManager =
	        		        (InputMethodManager)text.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
	        		        inputManager.showSoftInput(text, 0);
	         }
	     },  100);
//	    
//	    Timer timer1 = new Timer();
//	    timer1.schedule(new TimerTask()
//	     {
//	         public void run() 
//	         {
//	        	 //Log.i("timer", "timer");
//	        	 String s = text.getText().toString();
//	        	 if(s.equals("a"))
//	        	 {
//	        		 finish();
//	        	 }
//	         }
//	     },  1000, 100);
	}	
}
