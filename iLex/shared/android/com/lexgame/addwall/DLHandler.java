package com.lexgame.addwall;


import com.dlnetwork.Dianle;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class DLHandler extends Handler {
	public static final int DL_OPEN = 1;
	public static final int DL_CLOSE = 2;
	
	private Context context;
	private DLListener dlListener;
	
	public DLHandler(Activity thiz) {
		this.context = thiz;
		dlListener = new DLListener((Activity)thiz);
	}
	
	@Override
	public void handleMessage(Message msg) {
		super.handleMessage(msg);
		int what = msg.what;
		switch (what) {
		case DL_OPEN:
			Dianle.showOffers(context);
			break;		
		case DL_CLOSE:
			dlListener.getResult();
			//int  gold = 10;
			long addGold = dlListener.getGold();
			Log.i("b", "" + addGold);
			//dlListener.spendGold(gold);
			//if(addGold > 0)
				//afterAddWall(addGold);
			break;
		default:
			break; 
		}
	}
}
