package com.lexgame.addwall;

import android.app.Activity;
import android.content.Context;
import cn.waps.AppConnect;
import cn.waps.UpdatePointsNotifier;

public class WPListener implements UpdatePointsNotifier {
	
	private Context context;
	private int gold;

	@Override
	public void getUpdatePoints(String arg0, int arg1) {
		// TODO Auto-generated method stub
		gold = arg1;
	}

	@Override
	public void getUpdatePointsFailed(String arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public WPListener(Activity thiz)
	{
		this.context = thiz;
	}
	
	public void getResult()
	{
		AppConnect.getInstance(context).getPoints(this);
	}
	
	public int getGold()
	{
		return gold;
	}
	
	public void spendGold(int golds)
	{
		AppConnect.getInstance(context).spendPoints(golds,this);
	}
}
