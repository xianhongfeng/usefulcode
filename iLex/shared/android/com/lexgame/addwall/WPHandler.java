package com.lexgame.addwall;

import cn.waps.AppConnect;

import com.lexgame.mmIAP.MMAccount;
import com.lexgame.mmIAP.MMIAP;
import com.lexgame.utils.URLContent;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class WPHandler extends Handler {
	public static final int WP_OPEN = 1;
	public static final int WP_CLOSE = 2;
	
	private Context context;
	private static WPListener wpListener;
	
	public WPHandler(Activity thiz) {
		this.context = thiz;
		wpListener = new WPListener((Activity)thiz);
	}
	
	@Override
	public void handleMessage(Message msg) {
		super.handleMessage(msg);
		int what = msg.what;
		switch (what) {
		case WP_OPEN:
			AppConnect.getInstance(context).showOffers(context);
			URLContent url = new URLContent();
			break;		
		case WP_CLOSE:
			wpListener.getResult();
			int addGold = wpListener.getGold();
			wpListener.spendGold(addGold);
			if(addGold > 0)
				afterAddWall(addGold);
			AppConnect.getInstance(context).close();
			break;
		default:
			break; 
		}
	}
	
	public native void afterAddWall(int gold);
}
