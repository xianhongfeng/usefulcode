package com.lexgame.addwall;

import cn.waps.AppConnect;
import cn.waps.AppListener;
import cn.waps.UpdatePointsNotifier;
import android.app.Activity;
import android.content.Context;
import android.os.Message;
import android.util.Log;

public class WP {
	
	private static Context context;
	private static WPHandler wpHandler; 
	private static WPListener wpListener;
	
	public WP(Context thiz) 
	{
		this.context = thiz;
		wpHandler = new WPHandler((Activity)thiz);
		wpListener = new WPListener((Activity)thiz);
		//设置关闭积分墙癿监听接口，必须在showOffers接口之前调用
		AppConnect.getInstance(thiz).setOffersCloseListener(new AppListener(){
		@Override
		public void onOffersClose() {
		// TODO 关闭积分墙时癿操作代码
		Log.i("b", "b");
		wpListener.getResult();
		int addGold = wpListener.getGold();
		wpListener.spendGold(addGold);
		AppConnect.getInstance(context).close();
		}
		});
	}
	
	public static void createWPaddWall()
	{
		Message msg = new Message();
		msg.what = WPHandler.WP_OPEN;
        wpHandler.sendMessage(msg);
	}
	
	public static void closeWPaddWall()
	{
		Message msg = new Message();
		msg.what = DLHandler.DL_CLOSE;
        wpHandler.sendMessage(msg);
	}




}
