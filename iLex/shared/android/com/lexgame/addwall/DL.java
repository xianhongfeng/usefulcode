package com.lexgame.addwall;

import cn.waps.AppConnect;
import cn.waps.AppListener;
import cn.waps.UpdatePointsNotifier;
import android.app.Activity;
import android.content.Context;
import android.os.Message;
import android.util.Log;

public class DL {
	
	private static Context context;
	private static DLHandler dlHandler; 
	private static DLListener dlListener;
	
	public DL(Context thiz) 
	{
		this.context = thiz;
		dlHandler = new DLHandler((Activity)thiz);
		dlListener = new DLListener((Activity)thiz);
	}
	
	public static void createDLaddWall()
	{
		Message msg = new Message();
		msg.what = DLHandler.DL_OPEN;
        dlHandler.sendMessage(msg);
	}
	
	public static void closeDLaddWall()
	{
		Message msg = new Message();
		msg.what = DLHandler.DL_CLOSE;
        dlHandler.sendMessage(msg);
	}

}
