package com.lexgame.addwall;

import com.dlnetwork.Dianle;
import com.dlnetwork.GetTotalMoneyListener;
import com.dlnetwork.SpendMoneyListener;

import android.app.Activity;
import android.content.Context;
import android.util.Log;


public class DLListener implements GetTotalMoneyListener, SpendMoneyListener  {
	
	private Context context;
	private long gold;

	
	public DLListener(Activity thiz)
	{
		this.context = thiz;
	}
	
	public void getResult()
	{
		Dianle.getTotalMoney(context, this);
	}
	
	public long getGold()
	{
		return gold;
	}
	
	public void spendGold(int golds)
	{
		Dianle.spendMoney(context, golds, this);
	}

	@Override
	public void getTotalMoneyFailed(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void getTotalMoneySuccessed(String arg0, long arg1) {
		// TODO Auto-generated method stub
		gold = arg1;
		Log.i("a", "" + arg1);
		Dianle.spendMoney(context, (int)gold, this);
		if(gold > 0)
		{
			afterAddWall((int)gold);
		}
		
	}

	@Override
	public void spendMoneyFailed(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void spendMoneySuccess(long arg0) {
		// TODO Auto-generated method stub
		
	}
	public native void afterAddWall(int gold);
}
