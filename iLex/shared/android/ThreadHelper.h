﻿#pragma once
//#include "cocos2d.h"
#include <queue>
#include <iostream>
#include <mutex>
//using namespace cocos2d;
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
//#include <pthread/pthread.h>
//#else
//#include <pthread.h>
//#endif

namespace Lex
{
	namespace ThreadHelper
	{
		
		struct TPMessage
		{
			std::string msg;
			bool isSuccess;
		};

		// key 指定用的什么第三方lib 
		struct CallBackTask
		{
			int key;
			TPMessage tpmsg;
		};

		extern int sizeCBQueue();

		extern void addCBQueue(CallBackTask task);

		extern CallBackTask popCBQueue();
	}
}