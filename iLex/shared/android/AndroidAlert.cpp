﻿//
//  Alert.h
//  iLexLib
//
//  Created by leaving on 14-5-19.
//
//

#include "platform/android/jni/JniHelper.h"
#include "../LexShared.h"
#include "cocos2d.h"
#include "JniUtils.h"
//#include <pthread.h>

using namespace cocos2d;
namespace Lex {
	static Lex::AlertCallBack callbak[10];
}

extern "C"{
	void Java_com_lexgame_game_LexActivity_oneCallBack(JNIEnv *env,jobject thiz)
	{
		CCLOG("testAlert Java_com_lexgame_game_LexActivity_oneCallBack");
		if(Lex::callbak[0] != nullptr)
			Lex::callbak[0]();
	}

	void Java_com_lexgame_game_LexActivity_twoCallBack0(JNIEnv *env,jobject thiz)
	{
		CCLOG("testAlert Java_com_lexgame_game_LexActivity_twoCallBack0");
		if(Lex::callbak[1] != nullptr)
			Lex::callbak[1]();
	}

	void Java_com_lexgame_game_LexActivity_twoCallBack1(JNIEnv *env,jobject thiz)
	{
		CCLOG("testAlert Java_com_lexgame_game_LexActivity_twoCallBack1");
		if(Lex::callbak[2] != nullptr)
			Lex::callbak[2]();
	}

	void Java_com_lexgame_game_LexActivity_threeCallBack0(JNIEnv *env,jobject thiz)
	{
		CCLOG("testAlert Java_com_lexgame_game_LexActivity_threeCallBack0");
		if(Lex::callbak[3] != nullptr)
			Lex::callbak[3]();
	}

	void Java_com_lexgame_game_LexActivity_threeCallBack1(JNIEnv *env,jobject thiz)
	{
		CCLOG("testAlert Java_com_lexgame_game_LexActivity_threeCallBack1");
		if(Lex::callbak[4] != nullptr)
			Lex::callbak[4]();
	}

	void Java_com_lexgame_shuihu_shuihu_threeCallBack2(JNIEnv *env,jobject thiz)
	{
		CCLOG("testAlert Java_com_lexgame_game_LexActivity_threeCallBack2");
		if(Lex::callbak[5] != nullptr)
			Lex::callbak[5]();
	}
}

// 系统弹窗
namespace Lex {
	void ShowAlert(const char * title, const char * message)
	{
		JniMethodInfo saInfo;
		bool isHave = JniHelper::getStaticMethodInfo(saInfo, "com/lexgame/game/LexActivity", "noCBAlert", "(Ljava/lang/String;Ljava/lang/String;)V");
		if(isHave)
		{
			CCLOG("testAlert showAlert0 found");
			jstring msg = Lex::JniUtils::stoJstring(saInfo.env, message);
			jstring ti = Lex::JniUtils::stoJstring(saInfo.env, title);
			saInfo.env->CallStaticVoidMethod(saInfo.classID, saInfo.methodID, msg, ti);
		}
		else
			CCLOG("testAlert showAlert0 not found");

	}
	
	void ShowAlert(const char * title, const char * message,
				   const char * bt0, Lex::AlertCallBack call0)
	{
		JniMethodInfo saInfo;
		callbak[0] = call0;
		bool isHave = JniHelper::getStaticMethodInfo(saInfo, "com/lexgame/game/LexActivity", "oneCBAlert", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V");
		if(isHave)
		{
			CCLOG("testAlert1 showAlert found");
			jstring msg =  Lex::JniUtils::stoJstring(saInfo.env, message);
			jstring ti =  Lex::JniUtils::stoJstring(saInfo.env, title);
			jstring jbt0 =  Lex::JniUtils::stoJstring(saInfo.env, bt0);
			saInfo.env->CallStaticVoidMethod(saInfo.classID, saInfo.methodID, msg, ti, jbt0);
		}
		else
			CCLOG("testAlert showAlert1 not found");
	}
	
	void ShowAlert(const char * title, const char * message,
				   const char * bt0, Lex::AlertCallBack call0,
				   const char * bt1, Lex::AlertCallBack call1)
	{
		JniMethodInfo saInfo;
		callbak[1] = call0;
		callbak[2] = call1;
		CCLog("twoCBAlert found before!");
		bool isHave = JniHelper::getStaticMethodInfo(saInfo, "com/lexgame/game/LexActivity", "twoCBAlert", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V");
		if(isHave)
		{
			CCLog("twoCBAlert found!");
			jstring msg =  Lex::JniUtils::stoJstring(saInfo.env, message);
			jstring ti =  Lex::JniUtils::stoJstring(saInfo.env, title);
			jstring jbt0 =  Lex::JniUtils::stoJstring(saInfo.env, bt0);
			jstring jbt1 =  Lex::JniUtils::stoJstring(saInfo.env, bt1);

			saInfo.env->CallStaticVoidMethod(saInfo.classID, saInfo.methodID, msg, ti, jbt0, jbt1);
		}
	}
				   
	void ShowAlert(const char * title, const char * message,
				   const char * bt0, Lex::AlertCallBack call0,
				   const char * bt1, Lex::AlertCallBack call1,
				   const char * bt2, Lex::AlertCallBack call2)
	{
		JniMethodInfo saInfo;
		callbak[3] = call0;
		callbak[4] = call1;
		callbak[5] = call2;
		bool isHave = JniHelper::getStaticMethodInfo(saInfo, "com/lexgame/game/LexActivity", "threeCBAlert", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V");
		if(isHave)
		{
			jstring msg =  Lex::JniUtils::stoJstring(saInfo.env, message);
			jstring ti =  Lex::JniUtils::stoJstring(saInfo.env, title);
			jstring jbt0 =  Lex::JniUtils::stoJstring(saInfo.env, bt0);
			jstring jbt1 =  Lex::JniUtils::stoJstring(saInfo.env, bt1);
			jstring jbt2 =  Lex::JniUtils::stoJstring(saInfo.env, bt1);

			saInfo.env->CallStaticVoidMethod(saInfo.classID, saInfo.methodID, msg, ti, jbt0, jbt1, jbt2);
		}
	}
	
	void ShowAlert(const char * title, const char * message,
				   const char * bt0, Lex::AlertCallBack call0,
				   const char * bt1, Lex::AlertCallBack call1,
				   const char * bt2, Lex::AlertCallBack call2,
				   const char * bt3, Lex::AlertCallBack call3)
	{
		callbak[6] = call0;
		callbak[7] = call1;
		callbak[8] = call2;
		callbak[9] = call3;
	}
}