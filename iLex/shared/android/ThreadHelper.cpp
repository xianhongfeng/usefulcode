#include "ThreadHelper.h"
#include <mutex>

namespace Lex
{
	namespace ThreadHelper
	{
		std::queue<CallBackTask> cbQueue;
		std::mutex cbQMutex;

		int sizeCBQueue()
		{
			cbQMutex.lock();
			int size = cbQueue.size();
			cbQMutex.unlock();
			return size;
		}

		void addCBQueue(CallBackTask task)
		{
			cbQMutex.lock();
			cbQueue.push(task);
			cbQMutex.unlock();
		}

		CallBackTask popCBQueue()
		{
			cbQMutex.lock();
			CallBackTask _msg = cbQueue.front();
			cbQueue.pop();
			cbQMutex.unlock();
			return _msg;
		}
	}
}