#include <stdio.h>
#include <algorithm>
#include "../LexShared.h"
#include <time.h>
#include "cocos2d.h"
#include "JniUtils.h"
#include <string.h>
#include "platform/android/jni/JniHelper.h"
#include "jai_encryption.h"

using namespace std;
using namespace cocos2d;
#define VARY_CHARS "ares"
#define MYLOG CCLOG
//#define MYLOG(...)


uint32_t getCode()
{
    MYLOG("%s", __func__);
	static bool inited = false;
	static uint32_t res = 0;
	if(inited) return res;
	JniMethodInfo getCodeInfo;
	jstring deviceid;
	bool isHave = JniHelper::getStaticMethodInfo(getCodeInfo, "com/lexgame/utils/LexBundle", "getCodeStatic", "()Ljava/lang/String;");
	if(isHave)
	{
        MYLOG("ok");
		deviceid = (jstring)getCodeInfo.env->CallStaticObjectMethod(getCodeInfo.classID, getCodeInfo.methodID);
	}
	else
	{
        CCLog("false");
	}
	auto str = JniHelper::jstring2string(deviceid);
	uint32_t t = 0;
	for(char i : str)
	{
		t += i * 56536756;
	}
	inited = true;
	CCLog("--- deviceid:%s, code:%d", str.c_str(), res);
	return (res = t);
}

        
std::string Lex::GetDeviceID(bool unique = false)
{
	MYLOG("%s", __func__);
	static bool inited = false;
	static uint32_t res = 0;
	JniMethodInfo getCodeInfo;
	jstring deviceid;
	bool isHave = JniHelper::getStaticMethodInfo(getCodeInfo, "com/lexgame/utils/LexBundle", "getCodeStatic", "()Ljava/lang/String;");
	if(isHave)
	{
        MYLOG("ok");
		deviceid = (jstring)getCodeInfo.env->CallStaticObjectMethod(getCodeInfo.classID, getCodeInfo.methodID);
	}
	else
	{
        CCLog("false");
	}
	auto str = JniHelper::jstring2string(deviceid);
	return str;
}

bool Lex::Bundle::Save(const char * name, void *data, size_t size)
{
    MYLOG("%s", __func__);
	jai::uint32 code = getCode();
    
	string path = Lex::Bundle::GetDocPath(name);
	jai::Encrypt12s<char> en1(code % 255, (code >> 4) % 255);
	jai::Encrypt01<char> en2(code);
	char * buf = new char[size + 4];
	for(int i = 0; i < 4; ++i)
	{
		buf[i] = VARY_CHARS[i];
	}
	::memcpy(buf + 4, data, size);
	en1.encode(buf, size + 4);
	en2.encode(buf, size + 4);

//	CCLog("androidBundle == %s == %s == %d", path.c_str(), (char*)buf, size+4);
    CCLOG("--- open file");

	FILE* stream;
	stream= fopen(path.c_str(),"w");
	if(!stream)
	{
        CCLog("--- open failed");
		return false;
	}
	else
	{
        MYLOG("--- save ok");
		fwrite((char*)buf,sizeof(char),size+4,stream);
		fclose(stream);
	}
	return true;
}

bool Lex::Bundle::Load(const char * name, void *data, size_t size)
{
    MYLOG("%s", __func__);
	string path = Lex::Bundle::GetDocPath(name);
	jai::uint32 code = getCode();
	CCLog("encrypt load %d", code);
	jai::Encrypt12s<char> en1(code % 255, (code >> 4) % 255);
	jai::Encrypt01<char> en2(code);
	char * buf = new char[size + 4];
	FILE* stream;
	CCLog("--- path:%s",  path.c_str());
	stream= fopen(path.c_str(),"r");
//	data = new char[size];
	if(!stream)
	{
        CCLog("--- no file to load");
		return false;
	}
	else
	{
		if(fread(buf,sizeof(char),size+4,stream) != size + 4) return false;
		fclose(stream);
	}
	//::memcpy(buf, data, size + 4);
	en2.decode(buf, size + 4);
	en1.decode(buf, size + 4);
	for (int i = 0; i < 4; ++i) {
		if(buf[i] != VARY_CHARS[i]) return false;
	}
	::memcpy(data, buf + 4, size);
    MYLOG("--- load ok");
	return true;

}

std::string Lex::Bundle::GetResPath(const char* path)
{
    MYLOG("%s", __func__);
	JniMethodInfo resPathInfo;
	string result;
	bool isHave = JniHelper::getStaticMethodInfo(resPathInfo, "com/lexgame/utils/FileUtils", "getResPathStatic", "(Ljava/lang/String;)V");
	if(isHave)
	{
        CCLog("ok");
		jstring jstr = Lex::JniUtils::stoJstring(resPathInfo.env, path);
		jstring temp = (jstring)resPathInfo.env->CallStaticObjectMethod(resPathInfo.classID, resPathInfo.methodID, jstr);
		result = JniHelper::jstring2string(temp);
	}
	else
	{
        CCLog("false");
	}
	return result;
}

std::string Lex::Bundle::GetDocPath(const char* path)
{
    MYLOG("%s", __func__);
	JniMethodInfo docInfo;
	bool isHave = JniHelper::getStaticMethodInfo(docInfo, "com/lexgame/utils/FileUtils", "getDocPathStatic", "(Ljava/lang/String;)Ljava/lang/String;");
	if(isHave)
	{
		jstring jpath = Lex::JniUtils::stoJstring(docInfo.env, path);
		auto str = JniHelper::jstring2string((jstring)docInfo.env->CallStaticObjectMethod(docInfo.classID, docInfo.methodID, jpath));
        return str;
	}
	else
	{
        MYLOG("false");
		return "";
	}
}