#include "../LexShared.h"
#include <time.h>
#include <iostream>
#include <cocos2d.h>
#include "platform/android/jni/JniHelper.h"

using namespace std;
using namespace cocos2d;

namespace Lex
{
	namespace WebTime
	{
		static int year = 0;
		static int month = 0;
		static int dayInMonth = 0;
		static int dayInWeek = 0;
		static int hour = 0;
		static int minute = 0;
		static int second = 0;
		static bool valid = false;
		static time_t local = 0;
		static time_t web_c_time = 0;
	}
}

static void GetTime()
{
    CCLOG("%s", __func__);
	JNIEnv * mEnv;
	JavaVM * mJvm;
    jclass mClass;
	

	struct tm *mtime;
	Lex::WebTime::web_c_time = 0;
	time_t timenow;
#if defined(UnuseWebTime)
	timenow = ::time(0);
#else
    CCLOG("%s", "getJavaVm");
    mJvm = JniHelper::getJavaVM();
    mJvm->GetEnv((void**)&mEnv,JNI_VERSION_1_4);
    mClass = mEnv->FindClass("com/lexgame/utils/Datetime");
	jmethodID getNow = mEnv->GetStaticMethodID(mClass, "getTime", "()J");
	time_t result = (mEnv->CallStaticIntMethod(mClass, getNow));
	CCLOG("datetime result %ld", result);
	timenow = result;
#endif
	if(timenow)
	{
		mtime = localtime(&timenow);
		Lex::WebTime::year = mtime->tm_year + 1900;
		Lex::WebTime::month = mtime->tm_mon + 1;
		Lex::WebTime::dayInMonth = mtime->tm_mday;
		Lex::WebTime::dayInWeek = mtime->tm_wday + 1;
		Lex::WebTime::hour = mtime->tm_hour;
		Lex::WebTime::minute = mtime->tm_min;
		Lex::WebTime::second = mtime->tm_sec;
		Lex::WebTime::valid = true;
		Lex::WebTime::web_c_time = timenow;
		Lex::WebTime::local = ::time(0);
	}else
	{
		Lex::WebTime::web_c_time = 0;
		Lex::WebTime::local = ::time(0);
	}
}

namespace Lex {
		bool UpdateWebDate(DateCallBack callback)
		{
			GetTime();
			if(callback) callback(Lex::WebTime::web_c_time != 0);
			return true;
		}

        time_t GetTimeNow()
		{
			CCLOG("datetime UpdateWebDate GetTimeNow %ld", web_c_time);
			if(web_c_time == 0) return 0;
			return (time(0) - local) + web_c_time;
		}
}