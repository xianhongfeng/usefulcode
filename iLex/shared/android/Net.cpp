#include "../LexShared.h"
#include "platform/android/jni/JniHelper.h"
#include "JniUtils.h"
#include "cocos2d.h"

using namespace cocos2d;

void Lex::Email::SetInAppDelegate(MailResult callback)
{
}

bool Lex::Email::SendMailInApp(std::string title, std::string message)
{
	return false;
}

bool Lex::Email::SendMailOutApp(std::string title, std::string message)
{
	return false;
}

bool Lex::Email::SendMailInAppTo(std::string address, std::string title, std::string message)
{
	JniMethodInfo mailInfo;
	bool isHave = JniHelper::getStaticMethodInfo(mailInfo, "com/lexgame/utils/LexBundle", "sendEmailOutAppStatic", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z");
	if(isHave)
	{
		jstring jaddress = Lex::JniUtils::stoJstring(mailInfo.env, address.c_str());
		jstring jtitle = Lex::JniUtils::stoJstring(mailInfo.env, title.c_str());
		jstring jmessage = Lex::JniUtils::stoJstring(mailInfo.env, message.c_str());
		return mailInfo.env->CallStaticBooleanMethod(mailInfo.classID, mailInfo.methodID, jaddress, jtitle, jmessage);
	}
	return false;
}

bool Lex::Email::SendMailOutAppTo(std::string address, std::string title, std::string message)
{
	JniMethodInfo mailInfo;
	bool isHave = JniHelper::getStaticMethodInfo(mailInfo, "com/lexgame/utils/LexBundle", "sendEmailOutAppStatic", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z");
	if(isHave)
	{
		jstring jaddress = Lex::JniUtils::stoJstring(mailInfo.env, address.c_str());
		jstring jtitle = Lex::JniUtils::stoJstring(mailInfo.env, title.c_str());
		jstring jmessage = Lex::JniUtils::stoJstring(mailInfo.env, message.c_str());
		return mailInfo.env->CallStaticBooleanMethod(mailInfo.classID, mailInfo.methodID, jaddress, jtitle, jmessage);
	}
	return false;
}

bool Lex::Web::OpenLinkInBrowser(std::string link)
{
	JniMethodInfo olInfo;
	bool isHave = JniHelper::getStaticMethodInfo(olInfo, "com/lexgame/utils/LexBundle", "openLinkStatic", "(Ljava/lang/String;)V");
	if(isHave)
	{
		CCLog("androidBundle OpenLinkInBrowser -- found");
		jstring jstr = Lex::JniUtils::stoJstring(olInfo.env, link.c_str());
		olInfo.env->CallStaticVoidMethod(olInfo.classID, olInfo.methodID, jstr);
		return true;
	}
	else
	{
		CCLog("androidBundle OpenLinkInBrowser -- not found");
		return false;
	}
}

//bool Lex::Web::OpenLinkInApp(std::string link, std::vector<std::string> subs,  std::shared_ptr<WebViewDelegate> delegate)
//{
//	return false;
//}

bool Lex::Web::OpenLinkInApp(std::string url, std::vector<std::string> subs, bool all, std::shared_ptr<WebViewDelegate> delegate, float scale)
{
	JniMethodInfo jmi;
	bool ishave = JniHelper::getStaticMethodInfo(jmi, "com/lexgame/utils/LexBundle", "moreGameStatic", "(Ljava/lang/String;)V");
	if(ishave)
	{
		CCLog("showWebView found");
		jstring jurl = Lex::JniUtils::stoJstring(jmi.env, url.c_str());
		jmi.env->CallStaticVoidMethod(jmi.classID, jmi.methodID, jurl);
        return true;
	}
	else
    {
		CCLog("showWebView not have");
        return false;
    }
}

