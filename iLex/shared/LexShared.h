//
//  LexShared.h
//  ILex
//
//  Created by lover on 15/2/27.
//  Copyright (c) 2015年 lover. All rights reserved.
//

#pragma once

#include "LexJai.h"
#include <iostream>
#include <string>
#include <functional>
#include <vector>

/*
    functions: ShowAlert
    arguments: 
        title: title of window
        msg: message info
        bt(N): Nth button name
        call(N): Nth button operation
 
 by Daniel.

    ios realization: @"ios/Alert.mm" 
        by Daniel
 
    android realization: @"android/..." 
        by ...
 
 */

namespace Lex {
    
    typedef std::function<void ()> AlertCallBack;
    
    void ShowAlert(const char * title, const char * msg);
    
    void ShowAlert(const char * title, const char * msg,
                   const char * bt0, AlertCallBack call0 = nullptr);
    
    void ShowAlert(const char * title, const char * msg,
                   const char * bt0, AlertCallBack call0,
                   const char * bt1, AlertCallBack call1);
    
    void ShowAlert(const char * title, const char * msg,
                   const char * bt0, AlertCallBack call0,
                   const char * bt1, AlertCallBack call1,
                   const char * bt2, AlertCallBack call2);
    
    void ShowAlert(const char * title, const char * msg,
                   const char * bt0, AlertCallBack call0,
                   const char * bt1, AlertCallBack call1,
                   const char * bt2, AlertCallBack call2,
                   const char * bt3, AlertCallBack call3);
}


/*
    function: IsJaiBroken & IsIAPBroken
        by Daniel
 */

namespace Lex {
    bool IsJaiBroken(); // default return false
    bool IsIAPBroken(); // default return false
}


/*
    function: GetDeviceID
 
    ios: define pre-build macro USE_IDFA=1 to get idfa, else get a temp-id.
        by Daniel
 */
namespace Lex {
    // unique=true and USE_IDFA=1 to get idfa for ios.
    std::string GetDeviceID(bool unique = false);

    std::vector<char> Load(const char * name);
    bool Save(const char * name, void *data, size_t size);
    bool Load(const char * name, void *data, size_t size);
    
    std::string GetResPath(const char * path);
    std::string GetDocPath(const char * path);
    
    std::string GetDataOfFile(const char * path);
    std::string GetDataOfURL(const char * path);
}


/*
    function: GetRandGen
    
    realization: LexShared.cpp
 */

namespace Lex {
    jai::Random & GetRandom();
}

/*
    function: GetWebTime
    
    no realization
 */

namespace Lex {
    typedef void (*DateCallBack)(bool);
    
    // this function is not synchronized.
    // return true only means the the calling is ok.
    // when time is really updated, 'callBack' will be triggered.
    bool UpdateWebDate(DateCallBack callBack = nullptr);
    
    time_t GetWebTime();
}


/* 
    class: Emojs (inline)
    about: get emoj characters
 */
namespace Lex {
    class Emojs
    {
    public:
        size_t count() const
        {
            return m_Subs.size();
        }
        std::string at(int index) const
        {
            return m_Subs[index];
        }
        std::string operator[](int index) const
        {
            return at(index);
        }
        Emojs();
    private:
        std::vector<std::string> m_Subs;
    };
}

/*
    function: language
 */
namespace Lex {
    
    enum Lang { En, Zh, Th };
    
    Lang GetSystemLanguage();
    
    Lang GetUsedLanguage();
    
    void SetUsedLanguage(Lang lang);
    
    /* realization @ LexShared.cpp*/
    
    struct TextSrc
    {
    protected:
        std::vector<std::string> mTexts;
    public:
        TextSrc(const char * defaultText, ...);
        const char * getCurrentLangText();
        operator const char * ();
        const char * getText(Lang);
    };
}

/*
    function: email
 */
namespace Lex {
    namespace Email
    {
        typedef std::function<void (bool)> EmailResult;
        
        bool SendMailInApp(std::string title, std::string message, EmailResult callback);
        bool SendMailOutApp(std::string title, std::string message);
        
        bool SendMailInAppTo(std::string address, std::string title, std::string message, EmailResult callback);
        bool SendMailOutAppTo(std::string address, std::string title, std::string message);
    }
}

/*
    function: web browser
 */
namespace Lex {
    namespace Web
    {
        bool OpenLinkInBrowser(std::string link);
        
        /*
         arguments:
            link: home page
            sub_link:
         */
        bool OpenLinkInApp(std::string link, std::vector<std::string> sub_link,
                           float scale = 1.f,
                           std::function<void ()> onClose = nullptr,
                           std::function<void (bool)> onOpenResult = nullptr);
    }
}

/*
    function: text input
 */

namespace Lex {
    namespace Text
    {
        enum class EInputType {
            Number, // 数字
            Alpha, // 字母
            AlphaNumber, // 数字，字母
            Email, // xxx@xxx
            Words, // 数字 字母 中文字符
            ASCII, // 可见ASCII字符
            Text, // 单行文本
            LongText, // 长文本 任意字符, 可换行
        };
        
        /*
            function: read a string
                title: title for textbox
                type: ...
                callback:
                    std::function<void (bool res, std::string): 
                        bool res: true for "finish", false for "cancel"
                length: ...
         */
        void InputString(std::string title, EInputType type,
                         std::function<void (bool res, std::string)> callback,
                         int length = 0, // length for ascii, 0: unlimited
                         int nonAsciiLength = 0, // length if contains non-ascii, 0: same as ascii length
                         std::string defaultText = "");
        
        // 判断是否包含非ASCII字符
        bool IsContainNonAscii(std::string text);
        // ascii算一个字符, 非ascii算两个字符
        int GetLenght(std::string text);
        // 一个单独字算一个长度
        int GetWordsCount(std::string text);
        // utf-8子字符串
        std::string GetSubWords(std::string text, int from, int len);
    }
}
