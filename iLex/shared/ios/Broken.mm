//
//  Broken.cpp
//  ILex
//
//  Created by pro on 15/3/9.
//  Copyright (c) 2015年 lover. All rights reserved.
//

#include "LexShared.h"
#import <Foundation/Foundation.h>

namespace Lex {
    bool IsJaiBroken()
    {
        static const char* jailbreak_apps[] =
        {
            "/Applications/Cydia.app",
            "/Applications/limera1n.app",
            "/Applications/greenpois0n.app",
            "/Applications/blackra1n.app",
            "/Applications/blacksn0w.app",
            "/Applications/redsn0w.app",
            "/Applications/Absinthe.app",
            NULL,
        };
        // Now check for known jailbreak apps. If we encounter one, the device is jailbroken.
        for (int i = 0; jailbreak_apps[i] != NULL; ++i)
        {
            if ([[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithUTF8String:jailbreak_apps[i]]])
            {
                //NSLog(@"isjailbroken: %s", jailbreak_apps[i]);
                return true;
            }
        }
        // TODO: Add more checks? This is an arms-race we're bound to lose.
        
        return false;
    }
    
    bool IsIAPBroken()
    {
        NSString *rootAppPath = @"/Applications";
        NSArray *listApp = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:rootAppPath error:nil];
        
        for (int i = 0; i < [listApp count]; i++) {
            //        NSLog(@"%@",[listApp objectAtIndex:i]);
            NSString *name = [listApp objectAtIndex:i];
            if ([name isEqualToString:@"IAPFree.app"]) {
                return true;
            }
        }
        
        NSString *substrateAppPath = @"/Library/MobileSubstrate/DynamicLibraries";
        NSArray *substrateListApp = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:substrateAppPath error:nil];
        
        for (int i = 0; i < [substrateListApp count]; i++) {
            //        NSLog(@"%@",[substrateListApp objectAtIndex:i]);
            NSString *name = [substrateListApp objectAtIndex:i];
            if ([name isEqualToString:@"iap.dylib"]) {
                return true;
            }
        }
        return false;
    }
}