//
//  Alert.cpp
//  LexLib
//
//  Created by Pro on 14-5-22.
//
//

#include "LexShared.h"
#import "LexShared.h"
#import <UIKit/UIKit.h>

@interface AlertView1 : UIAlertView<UIAlertViewDelegate> {
    std::function<void()> mCall_0;
    std::function<void()> mCall_1;
    std::function<void()> mCall_2;
    std::function<void()> mCall_3;
}
- (void) initWithMessage:(const char *) data Title:(const char *) title
                     OK0:(const char *) butCancel
             CallBackOK0:(std::function<void()>)callBackCancel;
- (void) initWithMessage:(const char *) data Title:(const char *) title
                     OK0:(const char *) butCancel
             CallBackOK0:(std::function<void()>)callBackCancel
                     OK1:(const char *) butOK
             CallBackOK1:(std::function<void()>)callBack;
- (void) initWithMessage:(const char *) data Title:(const char *) title
                     OK0:(const char *) butCancel
             CallBackOK0:(std::function<void()>)callBackCancel
                     OK1:(const char *) butOK
             CallBackOK1:(std::function<void()>)callBack
                     OK2:(const char *) butOK2
             CallBackOK2:(std::function<void()> ) callBack2;

- (void) initWithMessage:(const char *) data Title:(const char *) title
                     OK0:(const char *) butCancel
             CallBackOK0:(std::function<void()>)callBackCancel
                     OK1:(const char *) butOK
             CallBackOK1:(std::function<void()>)callBack
                     OK2:(const char *) butOK2
             CallBackOK2:(std::function<void()> ) callBack2
                     OK3:(const char *) butOK3
             CallBackOK3:(std::function<void()> ) callBack3;

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
@end

@implementation AlertView1

- (void) initWithMessage:(const char *) data Title:(const char *) title
                     OK0:(const char *) butCancel
             CallBackOK0:(std::function<void()>)callBackCancel
{
    mCall_0 = callBackCancel;
    [self
     initWithTitle:title?[NSString stringWithUTF8String:title]:@""
     message:data?[NSString stringWithUTF8String:data]:@""
     delegate:self
     cancelButtonTitle:[NSString stringWithUTF8String:butCancel]
     otherButtonTitles:nil];
    
    [self show];
    
}
- (void) initWithMessage:(const char *) data Title:(const char *) title
                     OK0:(const char *) butCancel
             CallBackOK0:(std::function<void()>)callBackCancel
                     OK1:(const char *) butOK
             CallBackOK1:(std::function<void()>)callBack;
{
    mCall_0 = callBackCancel;
    mCall_1 = callBack;
    [self
     initWithTitle:[NSString stringWithUTF8String:title]
     message:[NSString stringWithUTF8String:data]
     delegate:self
     cancelButtonTitle:[NSString stringWithUTF8String:butCancel]
     otherButtonTitles:[NSString stringWithUTF8String:butOK], nil];
    
    [self show];
}
- (void) initWithMessage:(const char *) data Title:(const char *) title
                     OK0:(const char *) butCancel
             CallBackOK0:(std::function<void()>)callBackCancel
                     OK1:(const char *) butOK
             CallBackOK1:(std::function<void()>)callBack
                     OK2:(const char *) butOK2
             CallBackOK2:(std::function<void()>) callBack2
{
    mCall_0 = callBackCancel;
    mCall_1 = callBack;
    mCall_2	= callBack2;
    [self
     initWithTitle:[NSString stringWithUTF8String:title]
     message:[NSString stringWithUTF8String:data]
     delegate:self
     cancelButtonTitle:[NSString stringWithUTF8String:butCancel]
     otherButtonTitles:[NSString stringWithUTF8String:butOK], [NSString stringWithUTF8String:butOK2], nil];
    [self show];
    
}
- (void) initWithMessage:(const char *) data Title:(const char *) title
                     OK0:(const char *) butCancel
             CallBackOK0:(std::function<void()>)callBackCancel
                     OK1:(const char *) butOK
             CallBackOK1:(std::function<void()>)callBack
                     OK2:(const char *) butOK2
             CallBackOK2:(std::function<void()> ) callBack2
                     OK3:(const char *) butOK3
             CallBackOK3:(std::function<void()> ) callBack3
{
    mCall_0 = callBackCancel;
    mCall_1 = callBack;
    mCall_2	= callBack2;
    mCall_3	= callBack3;
    [self
     initWithTitle:[NSString stringWithUTF8String:title]
     message:[NSString stringWithUTF8String:data]
     delegate:self
     cancelButtonTitle:[NSString stringWithUTF8String:butCancel]
     otherButtonTitles:[NSString stringWithUTF8String:butOK],
     [NSString stringWithUTF8String:butOK2],
     [NSString stringWithUTF8String:butOK3], nil];
    [self show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0)
    {
        if(mCall_0) mCall_0();
    }
    if(buttonIndex == 1)
    {
        if(mCall_1) mCall_1();
    }
    if(buttonIndex == 2)
    {
        if(mCall_2) mCall_2();
    }
    if(buttonIndex == 3)
    {
        if(mCall_3) mCall_3();
    }
}
@end


namespace Lex{
	void ShowAlert(const char * title, const char * message)
	{
		UIAlertView *alert = [[UIAlertView alloc]initWithTitle:[NSString stringWithUTF8String:title]
													   message:[NSString stringWithUTF8String:message]
													  delegate:nil
											 cancelButtonTitle:@"OK" otherButtonTitles:nil];
		
		[alert show];
	}
	
	void ShowAlert(const char * title, const char * message,
				   const char * bt0, AlertCallBack call0)
	{
		AlertView1 * alertView = [[AlertView1 alloc] autorelease];
		[alertView initWithMessage:message Title:title
							   OK0:bt0
					   CallBackOK0:call0];
	}
	
	void ShowAlert(const char * title, const char * message,
				   const char * bt0, AlertCallBack call0,
				   const char * bt1, AlertCallBack call1)
	{
		AlertView1 *alertView = [[AlertView1 alloc] autorelease];
		[alertView initWithMessage:message
							 Title:title
							   OK0:bt0
					   CallBackOK0:call0
							   OK1:bt1
					   CallBackOK1:call1];
	}
	
	void ShowAlert(const char * title, const char * message,
				   const char * bt0, AlertCallBack call0,
				   const char * bt1, AlertCallBack call1,
				   const char * bt2, AlertCallBack call2)
	{
		AlertView1 *alertView = [[AlertView1 alloc]autorelease];
		[alertView initWithMessage:message
							 Title:title
							   OK0:bt0
					   CallBackOK0:call0
							   OK1:bt1
					   CallBackOK1:call1
							   OK2:bt2
					   CallBackOK2:call2];
	}
	
	void ShowAlert(const char * title, const char * message,
				   const char * bt0, AlertCallBack call0,
				   const char * bt1, AlertCallBack call1,
				   const char * bt2, AlertCallBack call2,
				   const char * bt3, AlertCallBack call3)
	{
		AlertView1 *alertView = [[AlertView1 alloc]autorelease];
		[alertView initWithMessage:message
							 Title:title
							   OK0:bt0
					   CallBackOK0:call0
							   OK1:bt1
					   CallBackOK1:call1
							   OK2:bt2
					   CallBackOK2:call2
							   OK3:bt3
					   CallBackOK3:call3];
	}
}
