//
//  Bundle.cpp
//  ILex
//
//  Created by pro on 15/3/9.
//  Copyright (c) 2015年 lover. All rights reserved.
//

#include "LexShared.h"
#include "jai_encryption.h"
#import <Foundation/Foundation.h>
#include <vector>

#define VARY_CHARS "iLex"

#define UUID_PATH "mytempid"

namespace Lex
{
#if defined(USE_IDFA)
    std::string getIDFA();
#endif
    std::string getDeviceTempID();
    std::string createDeviceTempID();
    jai::uint32 getEncCode();
    
    // unique=true and USE_IDFA=1 to get idfa for ios.
    std::string GetDeviceID(bool unique)
    {
#if defined(USE_IDFA) && USE_IDFA == 1
        if (unique)
        {
            return Lex::getIDFA();
        }
        else
        {
            return Lex::getDeviceTempID();
        }
#else
        return Lex::getDeviceTempID();
#endif
    }
    
#if defined(USE_IDFA)
    std::string getIDFA()
    {
        return [[[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString] UTF8String];
    }
#endif
    
    std::string getDeviceTempID()
    {
        auto data = Load(UUID_PATH);
        if(data.empty()) return createDeviceTempID();
        return std::string(data.begin(), data.end());
    }
    std::string createDeviceTempID()
    {
        NSString *uuid = [[NSUUID UUID] UUIDString];
        NSData * nsdata = [uuid dataUsingEncoding:NSUTF8StringEncoding];
        
        Lex::Save(UUID_PATH, (void*)nsdata.bytes, nsdata.length);
        
        return uuid.UTF8String;
    }
    
    std::vector<char> Load(const char * name)
    {
        NSString * key = [NSString stringWithUTF8String:name];
        NSData * nd = [[NSUserDefaults standardUserDefaults] objectForKey:key];
        if(nd == nil || nd.length <= 4) return std::vector<char>();
        
        int size = nd.length - 4;
        std::vector<char> vec(size + 4);
        char * buf = vec.data();
        
        jai::uint32 code = getEncCode();
        jai::Encrypt12s<char> en1(code % 255, (code >> 4) % 255);
        jai::Encrypt01<char> en2(code);
        
        ::memcpy(buf, [nd bytes], size + 4);
        en2.decode(buf, size + 4);
        en1.decode(buf, size + 4);
        for (int i = 0; i < 4; ++i) {
            if(buf[i] != VARY_CHARS[i]) return std::vector<char>();
        }
        vec.erase(vec.begin(), vec.begin() + 4);
        
        return vec;
    }
    
    bool Save(char const * buddleName, void * data, size_t size)
    {
        jai::uint32 code = getEncCode();
        jai::Encrypt12s<char> en1(code % 255, (code >> 4) % 255);
        jai::Encrypt01<char> en2(code);
        std::vector<char> vec(size+4);
        char * buf = vec.data();
        for(int i = 0; i < 4; ++i)
        {
            buf[i] = VARY_CHARS[i];
        }
        ::memcpy(buf + 4, data, size);
        en1.encode(buf, size + 4);
        en2.encode(buf, size + 4);
        NSString * key = [NSString stringWithCString:buddleName encoding:NSUTF8StringEncoding];
        [[NSUserDefaults standardUserDefaults]
         setObject:[NSData dataWithBytes:buf length:(size + 4)]
         forKey:key];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        return true;
    }
    bool Load(char const * buddleName, void * data, size_t size)
    {
        jai::uint32 code = getEncCode();
        jai::Encrypt12s<char> en1(code % 255, (code >> 4) % 255);
        jai::Encrypt01<char> en2(code);
        std::vector<char> vec(size+4);
        char * buf = vec.data();
        
        NSString * key = [NSString stringWithCString:buddleName encoding:NSUTF8StringEncoding];
        NSData * nd = [[NSUserDefaults standardUserDefaults] objectForKey:key];
        if([nd length] != size + 4) return false;
        ::memcpy(buf, [nd bytes], size + 4);
        en2.decode(buf, size + 4);
        en1.decode(buf, size + 4);
        for (int i = 0; i < 4; ++i) {
            if(buf[i] != VARY_CHARS[i]) return false;
        }
        ::memcpy(data, buf + 4, size);
        return true;
    }
    
    std::string GetResPath(const char * path)
    {
        NSString * s = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:[NSString stringWithUTF8String:path]];
        return [s UTF8String];
    }
    std::string GetDocPath(const char * path)
    {
        NSString *paths= [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        return [[paths stringByAppendingPathComponent:[NSString stringWithUTF8String:path]] UTF8String];
    }

    std::string GetDataOfFile(const char * path)
    {
        auto * nss = [NSString stringWithContentsOfFile:[NSString stringWithUTF8String:path] encoding:NSUTF8StringEncoding error:nil];
        if(nss == nil) return "";
        return [nss UTF8String];
    }
    
    std::string GetDataOfURL(const char * path)
    {
        NSURL * url = [NSURL URLWithString:[NSString stringWithUTF8String:path]];
        auto * nss = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
        if(nss == nil) return "";
        return [nss UTF8String];
    }
    
    jai::uint32 getEncCode()
    {
        static jai::uint32 res = 0;
        static bool init = false;
        if(init) return res;
        
        NSString * fileName = [NSString stringWithUTF8String:GetDocPath("gamesaving").c_str()];
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        //		jai::Random rand((jai::uint32)time(0));
        unsigned char data[12];
        if(![[NSFileManager defaultManager] fileExistsAtPath:fileName])
        {
            for (int i = 0; i < 12; ++i) {
                data[i] = (unsigned char)Lex::GetRandom().next(255);
            }
            [[NSFileManager defaultManager] createFileAtPath:fileName contents:[NSData dataWithBytes:data length:sizeof(data)] attributes:nil];
        }
        else {
            NSData * nd = [NSData dataWithContentsOfFile:fileName];
            if([nd length] != 12)
            {
                for (int i = 0; i < 12; ++i) {
                    data[i] = (unsigned char)Lex::GetRandom().next(255);
                }
                [[NSFileManager defaultManager] removeItemAtPath:fileName error:nil];
                [[NSFileManager defaultManager] createFileAtPath:fileName contents:[NSData dataWithBytes:data length:sizeof(data)] attributes:nil];
            }
            else
            {
                ::memcpy(data, [nd bytes], sizeof(data));
            }
        }
        NSDictionary *fileAttributes = [fileManager attributesOfItemAtPath:fileName error:nil];
        if (fileAttributes != nil) {
            NSDate * date = [fileAttributes objectForKey:NSFileModificationDate];
            long intev = date.timeIntervalSince1970;
            res = (jai::uint32)intev;
            unsigned char * p = (unsigned char *)(void*)&res;
            for (int i = 0; i < 12; ++i) {
                p[i%sizeof(res)] ^= data[i];
            }
            init = true;
            return res;
        }
        return -1;
    }
}