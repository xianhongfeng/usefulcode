//
//  TextView.m
//  Magicain_Aries
//
//  Created by Pro on 14-8-20.
//
//

#import <QuartzCore/QuartzCore.h>
#include "RootViewController.h"
#include "LexShared.h"

static std::function<void (bool, std::string)> OnEnd;

namespace Lex
{
    namespace Text
    {
        // 判断是否包含非ASCII字符
        bool IsContainNonAscii(std::string text)
        {
            for (int  i = 0; i < text.size(); ++i) {
                if(text.at(i) < 0 || text.at(i) > 127) return true;
            }
            return false;
        }
        // ascii算一个字符, 非ascii算两个字符
        int GetLenght(std::string text)
        {
            NSString * str = [NSString stringWithUTF8String:text.data()];
            int count = 0;
            
            NSRange range;
            for(int i=0; i<str.length; i+=range.length){
                range = [str rangeOfComposedCharacterSequenceAtIndex:i];
                if ([str characterAtIndex:i] < 0 || [str characterAtIndex:i] > 127)
                    count += 2;
                else
                    ++count;
            }
            return count;
        }
        // 一个单独字算一个长度
        int GetWordsCount(std::string text)
        {
            NSString * str = [NSString stringWithUTF8String:text.data()];
            int count = 0;
            NSRange range;
            for(int i=0; i<str.length; i+=range.length){
                range = [str rangeOfComposedCharacterSequenceAtIndex:i];
                ++count;
            }
            return count;
        }
        // utf-8子字符串
        std::string GetSubWords(std::string text, int from, int len)
        {
            NSString * str = [NSString stringWithUTF8String:text.data()];
            NSRange range;
            range.location = from;
            range.length = len;
            return [str substringWithRange:range].UTF8String;
        }
    }
}
//bool Lex::IsContainNonAscii(std::string str)
//{
//    for (int  i = 0; i < str.size(); ++i) {
//        if(str.at(i) < 0 || str.at(i) > 127) return true;
//    }
////	NSString *text = [NSString stringWithUTF8String:str.data()];
////	int length = text.length;
////	for (int i=0; i<length; ++i)
////	{
////		NSRange range = NSMakeRange(i, 1);
////		NSString *subString = [text substringWithRange:range];
////		const char *cString = [subString UTF8String];
////		if (strlen(cString) == 3)
////		{
////			return true;
////		}
////	}
//	return false;
//}
//int Lex::GetLenght(std::string text)
//{
//	NSString * str = [NSString stringWithUTF8String:text.data()];
//	int count = 0;
//	NSRange range;
//	for(int i=0; i<str.length; i+=range.length){
//		range = [str rangeOfComposedCharacterSequenceAtIndex:i];
//		++count;
//	}
//	return count;
//}
//std::string Lex::GetSubWords(std::string text, int from, int count)
//{
//	NSString * str = [NSString stringWithUTF8String:text.data()];
//	NSRange range;
//	range.location = from;
//	range.length = count;
//	return [str substringWithRange:range].UTF8String;
//}

@interface TextView : UIView <UITextViewDelegate>
@property(nonatomic, retain) UITextView * mContent;

+ (TextView *) shared;

//-(void)loadEditView:(Lex::Text::EInputType)type Title:(NSString *)str Length:(int)length Text:(NSString*)defaultText;
-(void)loadEditView:(Lex::Text::EInputType)type Title:(NSString *)str Length:(int)length NonAsciiLength:(int)nonAsciiLength Text:(NSString*)defaultText;

-(NSString *)getStringFromText;

@property(nonatomic,assign)NSInteger mLength;
@property(nonatomic,assign)NSInteger mNonAsciiLength;
@property(nonatomic,assign)BOOL mFlag;
@property(nonatomic,assign)Lex::Text::EInputType mType;
@end

static TextView * my = nil;
@implementation TextView

+ (TextView *) shared
{
	if(my == nil)
	{
		my = [[TextView alloc] init];
	}
	return my;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
	if(self.mFlag)
	{
		std::string str = self.mContent.text.UTF8String;
		std::string result;
		switch (self.mType)
		{
			case Lex::Text::EInputType::Alpha:
				for(char c : str)
				{
					if(::isalpha(c)) result.push_back(c);
				}
				break;
			case Lex::Text::EInputType::AlphaNumber:
				for (char c : str)
				{
					if(::isalnum(c)) result.push_back(c);
				}
				break;
			default:
				result = str;
				break;
		}
//        if(Lex::GetLenght(result) > self.mLength)
//            result = Lex::GetSubWords(result, 0, self.mLength);
		if(OnEnd) OnEnd(true, result);
	}
	[self removeMe];
}
-(void)loadEditView:(Lex::Text::EInputType)type Title:(NSString *)str Length:(int)length NonAsciiLength:(int)nonAsciiLength Text:(NSString*)defaultText
{
	for(UIView * view in self.subviews)
	{
		[view removeFromSuperview];
	}
	self.mType = type;
	self.mLength = length;
    self.mNonAsciiLength = nonAsciiLength==0?length:nonAsciiLength;
	self.mFlag = false;
    
	UIView * view = [[RootViewController rootViewControllerLastCreated] view];
	
	CGSize size  = view.bounds.size;
	CGRect rect = CGRectMake(0, 0,
							 size.width , size.height * 0.6);
	self.frame =rect;
	self.layer.borderWidth = 1;
	self.backgroundColor = [UIColor grayColor];
	
	UIButton * btOK = [UIButton buttonWithType:UIButtonTypeSystem];
	btOK.frame = CGRectMake(self.frame.size.width - self.frame.size.width * 0.12, 0, self.frame.size.width * 0.12, self.frame.size.height * 0.12);
	if(Lex::GetUsedLanguage() == Lex::Lang::Zh)
		[btOK setTitle:@"完成" forState:UIControlStateNormal];
	else
		[btOK setTitle:@"OK" forState:UIControlStateNormal];
	[btOK addTarget:self action:@selector(onOK:) forControlEvents:UIControlEventTouchUpInside];
	btOK.backgroundColor = [UIColor lightGrayColor];
	btOK.titleLabel.font = [UIFont systemFontOfSize:18];
	btOK.titleLabel.tintColor = [UIColor colorWithWhite:1.f alpha:1.f];
	btOK.tintColor =[UIColor colorWithWhite:0.f alpha:1.f];
	btOK.layer.borderWidth = 0;
	btOK.layer.cornerRadius = 0;
	
	UIButton * btCancel = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	btCancel.frame = CGRectMake(0, 0, self.frame.size.width * 0.12, self.frame.size.height * 0.12);
	
	if(Lex::GetUsedLanguage() == Lex::Lang::Zh)
		[btCancel setTitle:@"取消" forState:UIControlStateNormal];
	else
		[btCancel setTitle:@"Cancel" forState:UIControlStateNormal];
	
	[btCancel addTarget:self action:@selector(onCancel:) forControlEvents:UIControlEventTouchUpInside];
	btCancel.backgroundColor = [UIColor lightGrayColor];
	btCancel.titleLabel.font = [UIFont systemFontOfSize:18];
	btCancel.tintColor =[UIColor colorWithWhite:0.f alpha:1.f];
	btCancel.layer.borderWidth = 0;
	btCancel.layer.cornerRadius = 0;
	
	UILabel * titie = [[UILabel alloc]initWithFrame:CGRectMake(self.frame.size.width * 0.1, self.frame.size.height * 0.02, self.frame.size.width * 0.8, self.frame.size.height * 0.12)];
	titie.textAlignment = NSTextAlignmentCenter;
	titie.text = str;
	titie.font = [UIFont systemFontOfSize:18];
	
	self.mContent = [[[UITextView alloc]initWithFrame:CGRectMake(0, btOK.frame.size.height, rect.size.width, rect.size.height - rect.size.height * 0.25)]autorelease];
	self.mContent.delegate = self;
	self.mContent.backgroundColor = [UIColor whiteColor];
	self.mContent.layer.borderWidth = 1;
	[self.mContent becomeFirstResponder];
	self.mContent.font = [UIFont boldSystemFontOfSize:20];
	if(self.mType == Lex::Text::EInputType::Number) self.mContent.keyboardType = UIKeyboardTypeNumberPad;
	else if(self.mType == Lex::Text::EInputType::Email) self.mContent.keyboardType = UIKeyboardTypeEmailAddress;
	else if(self.mType == Lex::Text::EInputType::Alpha || self.mType == Lex::Text::EInputType::AlphaNumber) self.mContent.keyboardType = UIKeyboardTypeAlphabet;
	else self.mContent.keyboardType = UIKeyboardTypeDefault;
	
    [self.mContent setText:defaultText];
	[self addSubview:self.mContent];
	[self addSubview:btOK];
	[self addSubview:btCancel];
	[self addSubview:titie];
	[view addSubview:self];
}
-(void)onOK:(id)sender
{
	self.mFlag = true;
	[self.mContent resignFirstResponder];
}
-(void)removeMe
{
	for(UIView * view in self.subviews)
	{
		[view removeFromSuperview];
	}
	[self removeFromSuperview];
}
-(void)onCancel:(id)sender
{
	self.mFlag = false;
	[self.mContent resignFirstResponder];
}
-(NSString *)getStringFromText
{
	return self.mContent.text;
}
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
	if([text isEqualToString:@"\n"] && self.mType != Lex::Text::EInputType::LongText)
	{
		[self onOK:self];
		return NO;
	}
	else if(self.mType == Lex::Text::EInputType::Number)
	{
		std::string ustr = [text UTF8String];
		for(int i = 0; i < ustr.size(); ++i)
		{
			if(!::isnumber(ustr.at(i)))return NO;
		}
        if(self.mLength > 0 && Lex::Text::GetLenght(self.mContent.text.UTF8String) + Lex::Text::GetLenght(text.UTF8String) > self.mLength)
        {
            return NO;
        }
	}
	else if(self.mType == Lex::Text::EInputType::AlphaNumber)
	{
		std::string ustr = [text UTF8String];
		for (int i = 0; i < ustr.size(); ++i) {
			if(!::isalnum(ustr.at(i))) return NO;
		}
        if(self.mLength > 0 && Lex::Text::GetLenght(self.mContent.text.UTF8String) + Lex::Text::GetLenght(text.UTF8String) > self.mLength)
        {
            return NO;
        }
	}
	else if(self.mType == Lex::Text::EInputType::Alpha)
	{
		std::string ustr = [text UTF8String];
		for (int i = 0; i < ustr.size(); ++i) {
			if(!::isalpha(ustr.at(i))) return NO;
		}
        if(self.mLength > 0 && Lex::Text::GetLenght(self.mContent.text.UTF8String) + Lex::Text::GetLenght(text.UTF8String) > self.mLength)
        {
            return NO;
        }
	}
	else if(self.mType == Lex::Text::EInputType::Words)
	{
		NSRange range;
		for(int i=0; i< text.length; i+=range.length){
			range = [text rangeOfComposedCharacterSequenceAtIndex:i];
			std::string str = [text substringWithRange:range].UTF8String;
			if(str.size()== 1)
			{
				if(!::isalnum(str[0])) return NO;
			}
		}
        bool isContainNonAscii = Lex::Text::IsContainNonAscii(self.mContent.text.UTF8String) && Lex::Text::IsContainNonAscii(text.UTF8String);
        if(isContainNonAscii?self.mNonAsciiLength:self.mLength > 0 && Lex::Text::GetLenght(self.mContent.text.UTF8String) + Lex::Text::GetLenght(text.UTF8String) > isContainNonAscii?self.mNonAsciiLength:self.mLength)
        {
            return NO;
        }
	}
    else if (self.mType == Lex::Text::EInputType::ASCII)
    {
        std::string ustr = [text UTF8String];
        for(int i = 0; i < ustr.size(); ++i)
        {
            if(ustr.at(i) < 32 && ustr.at(i) > 127)return NO;
        }
        if(self.mLength > 0 && Lex::Text::GetLenght(self.mContent.text.UTF8String) + Lex::Text::GetLenght(text.UTF8String) > self.mLength)
        {
            return NO;
        }
    }
    else if (self.mType == Lex::Text::EInputType::Text)
    {
        bool isContainNonAscii = Lex::Text::IsContainNonAscii(self.mContent.text.UTF8String) && Lex::Text::IsContainNonAscii(text.UTF8String);
        if(isContainNonAscii?self.mNonAsciiLength:self.mLength > 0 && Lex::Text::GetLenght(self.mContent.text.UTF8String) + Lex::Text::GetLenght(text.UTF8String) > isContainNonAscii?self.mNonAsciiLength:self.mLength)
        {
            return NO;
        }
    }

	return YES;
}
@end

namespace Lex
{
    namespace Text
    {
        /*
         function: read a string
         title: title for textbox
         type: ...
         callback:
         std::function<void (bool res, std::string):
         bool res: true for "finish", false for "cancel"
         length: ...
         */
        void InputString(std::string title, EInputType type,
                         std::function<void (bool res, std::string)> callback,
                         int length, // length for ascii, 0: unlimited
                         int nonAsciiLength, // length if contains non-ascii, 0: same as ascii length
                         std::string defaultText)
        {
            OnEnd = callback;
            [[TextView shared] loadEditView:type
                                      Title:[NSString stringWithUTF8String:title.data()]
                                     Length:length
                                        NonAsciiLength:nonAsciiLength
                                       Text:[NSString stringWithUTF8String:defaultText.data()]];
        }
    }
}
