//
//  WebView.cpp
//  ILex
//
//  Created by pro on 15/3/9.
//  Copyright (c) 2015年 lover. All rights reserved.
//

#include "LexShared.h"
#import "RootViewController.h"

@interface LexWebViewBridge : UIView<UIWebViewDelegate,UIAlertViewDelegate>
{
    UIWebView *mWebView;
    UIToolbar *mToolbar;
    UIBarButtonItem *mBackButton;
    UIActivityIndicatorView *loadingAnimation;
}
@property()BOOL allSubs;
@property(retain)NSSet * subLinks;
@property(retain)NSString * homePage;

@end

@interface LexWebViewBridge()
{
    //	std::shared_ptr<Lex::Web::WebViewDelegate> mCallBack;
    std::function<void ()> mOnClose;
    std::function<void (bool)> mOnOpenResult;
}

@end
@implementation LexWebViewBridge

- (void)dealloc{
    
    [mBackButton release];
    [mToolbar release];
    [mWebView release];
    [loadingAnimation release];
    
    [super dealloc];
}
-(id)initinitWithLink:(NSString*)url WithSubLinks:(NSSet *)subs OpenAll:(BOOL)openAll InRect:(CGRect)rect onClose:(std::function<void ()>)onClose onOpenResult:(std::function<void (bool)>)onOpenResult
{
    self = [super initWithFrame:rect];
    if(!self) return nil;
    //    mCallBack = dele;
    mOnClose = onClose;
    mOnOpenResult = onOpenResult;
    self.homePage = url;
    self.subLinks = subs;
    self.allSubs = openAll;
    
    CGSize size = CGSizeMake(rect.size.width, rect.size.height);
    
    self.layer.cornerRadius = 15;
    self.layer.masksToBounds = YES;
    self.layer.borderWidth = 10;
    [self.layer setBorderColor:[UIColor colorWithRed:0.5 green: 0.5 blue:0.5 alpha:0.5].CGColor];
    
    // create webView
    //Bottom size
    int wBottomMargin = size.height * 0.10;
    int wWebViewHeight = size.height - wBottomMargin;
    
    mWebView = [[UIWebView alloc] initWithFrame:CGRectMake(10, 10, size.width - 20, size.height - 20)];
    mWebView.delegate = self;
    
    self.layer.cornerRadius = 15;
    
    [mWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.homePage]]];
    [mWebView setUserInteractionEnabled:NO]; //don't let the user scroll while things are
    
    //create a tool bar for the bottom of the Screen to hold the back button
    mToolbar = [UIToolbar new];
    [mToolbar setFrame:CGRectMake(10, wWebViewHeight -10, size.width - 20, wBottomMargin)];
    UIImage *exitImg = [UIImage imageWithContentsOfFile:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"close.png"]];
    
    [mToolbar setBackgroundImage:exitImg forToolbarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
    mToolbar.barStyle = UIBarStyleBlackTranslucent;
    UIButton *close = nil;
    close =[[UIButton alloc]initWithFrame:CGRectMake(self.bounds.size.width - 32 - 10, 10, 32, 32)];
    [close setImage:exitImg forState:UIControlStateNormal];
    [close addTarget:self action:@selector(backClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:mToolbar];
    
    [self addSubview:mWebView];
    [self addSubview:close];
    return self;
}

- (void)webViewDidStartLoad:(UIWebView *)thisWebView
{
    [self startLoading];
}

- (void) startLoading
{
    if (!loadingAnimation)
    {
        loadingAnimation = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 100.0f, 100.0f)];
        [loadingAnimation setBackgroundColor:[UIColor colorWithRed:0.5f green:0.5f blue:0.5f alpha:0.5f]];
        [loadingAnimation setCenter:CGPointMake(self.bounds.size.width/2.0, self.bounds.size.height/2.0)];
        [loadingAnimation setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
        
        [self addSubview:loadingAnimation];
    }
    
    [loadingAnimation startAnimating];
}
- (void) stopLoading
{
    if (loadingAnimation)
    {
        [loadingAnimation stopAnimating];
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)thisWebView
{
    [self stopLoading];
    [mWebView setUserInteractionEnabled:YES];
    if (mOnOpenResult) mOnOpenResult(true);
    //	if(mCallBack) mCallBack->onOpenResult(true);
    // mLayerWebView->webViewDidFinishLoad();
}

- (void)webView:(UIWebView *)thisWebView didFailLoadWithError:(NSError *)error
{
    if (mOnOpenResult) mOnOpenResult(false);
    //	if(mCallBack) mCallBack->onOpenResult(false);
    
    if ([error code] != -999 && error != NULL) { //error -999 happens when the user clicks on something before it's done loading.
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"网络错误" message:@"无法连接到网络，请检查网络连接。" delegate:self cancelButtonTitle:nil otherButtonTitles:@"确认", nil];
        
        [alert show];
        [alert release];
    }
    [self stopLoading];
}

-(void) backClicked:(id)sender
{
    if (mOnClose) mOnClose();
    //	if(mCallBack) mCallBack->onClose();
    
    mWebView.delegate = nil; //keep the webview from firing off any extra messages
    //remove items from the Superview...just to make sure they're gone
    [mToolbar removeFromSuperview];
    [mWebView removeFromSuperview];
    [self removeFromSuperview];
    // mLayerWebView->onBackbuttonClick();
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *url = request.URL.absoluteString;;
    //NSLog(url);
    if([url isEqualToString:@"http://exit/"])
    {
        [self backClicked:nil];
        return NO;
    }
    if(self.allSubs) return YES;
    if([url isEqualToString:self.homePage]) return YES;
    for(NSString * sub in self.subLinks)
    {
        if([sub isEqualToString:url]) return YES;
    }
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    return NO;
}

@end

////////////////////////////
namespace Lex {
    namespace Web {
        /*
         arguments:
         link: home page
         sub_link:
         */
        bool OpenLinkInApp(std::string link, std::vector<std::string> sub_link,
                           float scale,
                           std::function<void ()> onClose,
                           std::function<void (bool)> onOpenResult)
        {
            NSMutableSet * subLinks = [[[NSMutableSet alloc] init] autorelease];
            bool openAll = false;
            for (auto sub : sub_link)
            {
                if (sub == "*")
                {
                    openAll = true;
                    break;
                }
                NSString *iden = [NSString stringWithUTF8String:sub.data()];
                [subLinks addObject:iden];
            }
            [subLinks removeAllObjects];
            
            UIView * view = [[RootViewController rootViewControllerLastCreated] view];
            CGSize size  = view.bounds.size;
            CGRect rect = CGRectMake(size.width * (1.f - scale) / 2, size.height * (1.f - scale) / 2,
                                     size.width * scale, size.height * scale);
            LexWebViewBridge * gview =
            [[LexWebViewBridge alloc] initinitWithLink:[NSString stringWithUTF8String:link.data()] WithSubLinks:subLinks OpenAll:openAll InRect:rect onClose:onClose onOpenResult:onOpenResult];
            
            [view addSubview:gview];
            return true;
        }
        
    }
}

namespace Lex {
	namespace Web
	{
        bool OpenLinkInBrowser(std::string link)
        {
            NSURL * url = [NSURL URLWithString:[NSString stringWithUTF8String:link.c_str()]];
            return [[UIApplication sharedApplication] openURL:url];
        }
	}
}