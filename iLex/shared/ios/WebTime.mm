//
//  WebTime.cpp
//  ILex
//
//  Created by pro on 15/3/9.
//  Copyright (c) 2015年 lover. All rights reserved.
//

#include "LexShared.h"
#include "RootViewController.h"

namespace Lex
{
    static DateCallBack onCallBack = nullptr;
    static int year = 0;
    static int month = 0;
    static int dayInMonth = 0;
    static int dayInWeek = 0;
    static int hour = 0;
    static int minute = 0;
    static int second = 0;
//    static bool valid = false;
    static time_t local = 0;
    static time_t web_c_time = 0;
}

@interface  RootViewController( LXWebTime)

@end

@implementation RootViewController(LXWebTime)

-(void) updateWebTime
{
    NSURL *url=[NSURL URLWithString:@"http://www.baidu.com"];
    NSURLRequest *request=[NSURLRequest requestWithURL:url];
    NSURLConnection *connection=[[NSURLConnection alloc]initWithRequest:request delegate:self startImmediately:YES];
    [connection start];
}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSLog(@"response%@",response);
    NSHTTPURLResponse *httpResponse=(NSHTTPURLResponse *)response;
    if ([response respondsToSelector:@selector(allHeaderFields)]) {
        NSDictionary *dic=[httpResponse allHeaderFields];
        NSLog(@"dic:%@",dic);
        NSString *time = [[[dic objectForKey:@"Date"] substringFromIndex:5] substringToIndex:20];
        // Tue, 29 Apr 2014 01:36:45 GMT
        
        NSCalendar * calendar = [NSCalendar currentCalendar];
        NSDateFormatter *inputFormatter = [[[NSDateFormatter alloc] init] autorelease];
        
        inputFormatter.locale = [[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"] autorelease];
        //		inputFormatter.dateStyle = NSDateFormatterMediumStyle;
        [inputFormatter setDateFormat:@"dd MMM yyyy HH:mm:ss"];
        [inputFormatter setCalendar:calendar];
        
        NSDate* inputDate = [inputFormatter dateFromString:time];
        
        NSDateComponents *comps = nil;
        NSInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay |
        NSCalendarUnitWeekday | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
        
        NSLog(@"weekDay:%d day:%d mon:%d YEAR:%d hour:%d", comps.weekday, comps.day, comps.month, comps.year, comps.hour);
        NSLog(@"%@", time);
        
        comps = [calendar components:unitFlags fromDate:inputDate];
        if(inputDate == nil || comps == nil || calendar == nil || inputFormatter == nil) return;
        
        using namespace Lex;
        year = comps.year;
        month = comps.month;
        dayInMonth = comps.day;
        dayInWeek = comps.weekday;
        hour = comps.hour;
        minute = comps.minute;
        second = comps.second;
//        valid  = true;
        local = ::time(0);
        struct tm temp;
        temp.tm_year = year - 1900;
        temp.tm_mon = month - 1;
        temp.tm_mday = dayInMonth;
        temp.tm_hour = hour;
        temp.tm_min = minute;
        temp.tm_sec = second;
        temp.tm_zone = 0;
        web_c_time = mktime(&temp) + 17 * 3600;
        
        using namespace std;
        if(onCallBack) onCallBack(true);
    }
}
@end

/////////////////////
namespace Lex
{
    bool UpdateWebDate(DateCallBack callBack)
    {
        web_c_time = 0;
        local = 0;
        onCallBack = callBack;
        [[RootViewController rootViewControllerLastCreated] updateWebTime];
        return true;
    }
    
    time_t GetWebTime()
    {
        if(web_c_time == 0) return 0;
        return (time(0) - local) + web_c_time;
    }
}