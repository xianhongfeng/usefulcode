//
//  Net.cpp
//  iLexLib
//
//  Created by leaving on 14-5-20.
//
//

#include "LexShared.h"
#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface EmailViewController : UIViewController<MFMailComposeViewControllerDelegate>
{
    Lex::Email::EmailResult mailResult;
}
+(BOOL)canSendMail;
-(void)sendEmailInAppTo:(Lex::Email::EmailResult)onResult andAddress:(NSString *)address andTitle:(NSString *)title andMessage:(NSString *)message;

@end

@implementation EmailViewController

+(BOOL)canSendMail
{
    Class mailClass =(NSClassFromString(@"MFMailComposeViewController"));
    if(!mailClass || ![mailClass canSendMail])
    {
        return NO;
    }
    return YES;
}
-(void)sendEmailInAppTo:(Lex::Email::EmailResult)onResult andAddress:(NSString *)address andTitle:(NSString *)title andMessage:(NSString *)message
{
    mailResult = onResult;
    MFMailComposeViewController *mailPicker =[[MFMailComposeViewController alloc]init];
    mailPicker.mailComposeDelegate = self;
    if(address)
    {
        NSArray *toRecipients = [NSArray arrayWithObject:address];
        [mailPicker setToRecipients:toRecipients];
    }
    if(title) [mailPicker setSubject:title];
    
    [mailPicker setMessageBody:message isHTML:YES];
    [self presentViewController:mailPicker animated:YES completion:nil];
    [mailPicker release];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
    if(mailResult) mailResult(result == MFMailComposeResultSent);
    //	[self removeFromParentViewController];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
@end


////////////////////////////////
namespace Lex {
    namespace Email
    {
        EmailResult mCallBack = nullptr;
        
        bool SendMailInApp(std::string title, std::string message, EmailResult callback)
        {
            if([EmailViewController canSendMail])
            {
                mCallBack = callback;
                EmailViewController *mVC=[[[EmailViewController alloc]init]autorelease];
                [mVC sendEmailInAppTo:mCallBack andAddress:nil andTitle:[NSString stringWithUTF8String:title.c_str()] andMessage:[NSString stringWithUTF8String:message.c_str()]];
                [[[[UIApplication sharedApplication] keyWindow] rootViewController] addChildViewController:mVC];
                return true;
            }
            else
                return false;
        }
        bool SendMailOutApp(std::string title, std::string message)
        {
            return [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"mailto:"]];
        }
        
        bool SendMailInAppTo(std::string address, std::string title, std::string message, EmailResult callback)
        {
            if([EmailViewController canSendMail])
            {
                mCallBack = callback;
                EmailViewController *mVC=[[[EmailViewController alloc]init]autorelease];
                [mVC sendEmailInAppTo:mCallBack andAddress:[NSString stringWithUTF8String:address.c_str()] andTitle:[NSString stringWithUTF8String:title.c_str()] andMessage:[NSString stringWithUTF8String:message.c_str()]];
                [[[[UIApplication sharedApplication] keyWindow] rootViewController] addChildViewController:mVC];
                return true;
            }
            else
                return false;
        }
        bool SendMailOutAppTo(std::string address, std::string title, std::string message)
        {
            NSString * mailTo = [@"mailto:" stringByAppendingString:[NSString stringWithUTF8String:address.data()]];
            return [[UIApplication sharedApplication] openURL:[NSURL URLWithString:mailTo]];
        }
    }
}

