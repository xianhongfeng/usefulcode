//
//  main.m
//  ILex
//
//  Created by lover on 15/2/27.
//  Copyright (c) 2015年 lover. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, @"AppController");
    }
}
