#include "Scene.h"

Scene::Scene()
{
	must(init());
	this->autorelease();
}
Scene::Scene(CCLayer * layer)
{
	must(init());
	this->autorelease();
	this->addChild(layer);
}
void Scene::load()
{

}
void Scene::unload()
{
}
Scene::~Scene()
{
	CCSpriteFrameCache::sharedSpriteFrameCache()->removeUnusedSpriteFrames();
	CCTextureCache::sharedTextureCache()->removeUnusedTextures();
}