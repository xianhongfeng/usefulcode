﻿#pragma once

#include "Texture.h"

class Animation : public CCAnimation
{
public:
	Animation(Src::AnimationSrc1 const & Src,bool reverse = false);
	Animation(Src::PvrAnimationSrc const & Src);
	Animation(Src::AnimationSrc0 const & src, bool reverse = false, bool goBack = false);
	Animation(Src::AnimationSrc2 const & src, size_t fromFrame = 0, bool flag = false);
	void createWithArray(CCArray *frames, float delay);
};