#include "Menu.h"

MenuX::MenuX()
{
	must(init());
	autorelease();
	setTouchPriority(128);
	this->setAnchorPoint(Src::Anchor::LB);
	this->setPosition(Src::Screen::LB);
}
MenuX::MenuX(CCPoint const & anchor, CCPoint const & position)
{
	must(init());
	autorelease();
	setTouchPriority(128);
	this->setPosition(position);
	this->setAnchorPoint(anchor);
}
void MenuX::setAnchorPos(CCPoint const & anchor, CCPoint const & position)
{
	this->setPosition(position);
	this->setAnchorPoint(anchor);
}
void MenuX::useTouch(int key)
{
	setTouchPriority(key);
	setTouchMode(kCCTouchesOneByOne);
	setTouchEnabled(true);
}
void MenuX::unuseTouch()
{
	setTouchEnabled(false);
}

ButtonImage::ButtonImage(const char * file, size_t count, CCObject* target, SEL_MenuHandler selector, bool flip)
{
	CCTexture2D * texture = CCTextureCache::sharedTextureCache()->addImage(file);
	must(texture != nullptr);
	Sprite * sprite[3] = { nullptr, nullptr, nullptr};
	CCSize sz = texture->getContentSize();
	float diffy = sz.height / count;
	for(size_t i = 0; i < count; ++i)
	{
		CCRect rect(0, diffy * i, sz.width, diffy);
		sprite[i] = new Sprite(texture, rect);
		if(flip) sprite[i]->setFlipX(true);
	}
	must(this->initWithNormalSprite(sprite[0], sprite[1], sprite[2], target, selector));
	this->autorelease();
}
ButtonImage::ButtonImage(Src::ButtonSrc & src, CCObject* target, SEL_MenuHandler selector, bool flip)
{
	//        CCLOG("%s %s", __func__, src.path);
	CCTexture2D * texture = CCTextureCache::sharedTextureCache()->addImage(Src::FixPath(src.path));
	must(texture != nullptr);
	Sprite * sprite[3] = { nullptr, nullptr, nullptr};
	CCSize size = texture->getContentSize();
	if(src.srcRect.size.width < 0) src.srcRect.size.width *= -size.width;
	if(src.srcRect.size.height < 0) src.srcRect.size.height *= -size.height;
	if(src.srcRect.origin.x < 0) src.srcRect.origin.x *= -size.width;
	if(src.srcRect.origin.y < 0) src.srcRect.origin.y *= -size.height;
	float diffy = src.srcRect.size.height / src.count;
	for(size_t i = 0; i < src.count; ++i)
	{
		CCRect rect(src.srcRect.origin.x, src.srcRect.origin.y + diffy * i, src.srcRect.size.width, diffy);
		sprite[i] = new Sprite(texture, rect);
		if(flip) sprite[i]->setFlipX(true);
	}
	must(this->initWithNormalSprite(sprite[0], sprite[1], sprite[2], target, selector));

	this->setAnchorPos(src.anchor, src.dest);
	this->autorelease();
}

ButtonImage::ButtonImage(Src::PvrButtonSrc & src, CCObject* target, SEL_MenuHandler selector, bool flip)
{

	Sprite * sprite[3] = { nullptr, nullptr, nullptr};
	char buff[1024];
	CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(Src::FixPath(src.plist));
	for(size_t i = 0; i < src.count; ++i)
	{
		if(Src::PvrButtomFromZero) sprintf(buff, Src::FixPath(src.name), i);
		else sprintf(buff, Src::FixPath(src.name), i + 1);
		auto sf = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(buff);
		sprite[i] = new Sprite(sf);
		if(flip) sprite[i]->setFlipX(true);
	}
	must(this->initWithNormalSprite(sprite[0], sprite[1], sprite[2], target, selector));

	this->setAnchorPos(src.anchor, src.dest);
	this->autorelease();
}
void ButtonImage::reset(Src::ButtonSrc & src)
{
	CCTexture2D * texture = CCTextureCache::sharedTextureCache()->addImage(Src::FixPath(src.path));
	must(texture != nullptr);
	Sprite * sprite[3] = { nullptr, nullptr, nullptr};
	CCSize size = texture->getContentSize();
	if(src.srcRect.size.width < 0) src.srcRect.size.width *= -size.width;
	if(src.srcRect.size.height < 0) src.srcRect.size.height *= -size.height;
	if(src.srcRect.origin.x < 0) src.srcRect.origin.x *= -size.width;
	if(src.srcRect.origin.y < 0) src.srcRect.origin.y *= -size.height;
	float diffy = src.srcRect.size.height / src.count;
	for(size_t i = 0; i < src.count; ++i)
	{
		CCRect rect(src.srcRect.origin.x,
			src.srcRect.origin.y + diffy * i, src.srcRect.size.width, diffy);
		sprite[i] = new Sprite(texture, rect);
	}
	this->setNormalImage(sprite[0]);
	this->setSelectedImage(sprite[1]);
	this->setDisabledImage(sprite[2]);
}

void ButtonImage::reset(Src::PvrButtonSrc & src)
{
	Sprite * sprite[3] = { nullptr, nullptr, nullptr};
	char buff[1024];
	CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(Src::FixPath(src.plist));
	for(size_t i = 0; i < src.count; ++i)
	{
		if(Src::PvrButtomFromZero) sprintf(buff, Src::FixPath(src.name), i);
		else sprintf(buff, Src::FixPath(src.name), i + 1);
		auto sf = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(buff);
		sprite[i] = new Sprite(sf);
	}
	this->setNormalImage(sprite[0]);
	this->setSelectedImage(sprite[1]);
	this->setDisabledImage(sprite[2]);
}
void ButtonImage::setAnchorPos(CCPoint const & anchor, CCPoint const & position)
{
	this->setAnchorPoint(anchor);
	this->setPosition(position);
}
void ButtonImage::setState(bool enable, bool visible)
{
	this->setEnabled(enable);
	this->setVisible(visible);
}
