#pragma once

#ifndef _WIN32
#define override 
#endif

template<typename tType>
class ptr1
{
protected:
	tType * myPtr;
public:
	ptr1()
		: myPtr(new tType())
	{
		myPtr->retain();
	}
	ptr1(tType * p)
		: myPtr(p)
	{
		if(myPtr) myPtr->retain();
	}
	void reset(tType * ptr)
	{
		if(myPtr) myPtr->release();
		myPtr = ptr;
		if(myPtr) myPtr->retain();
	}
	tType * get() const
	{
		return myPtr;
	}
	operator tType * () const
	{
		return myPtr;
	}
	tType * operator ->() const
	{
		return myPtr;
	}
	tType * operator = (tType * ptr)
	{
		this->reset(ptr);
		return myPtr;
	}
	ptr1(ptr1 && o)
	{
		myPtr = o.myPtr;
		o.myPtr = nullptr;
	}
	~ptr1()
	{
		if(myPtr) myPtr->release();
	}
protected:
	ptr1(ptr1 const &);
	void operator =(ptr1 const &);
};
template<typename tType>
class ptr0
{
protected:
	tType * myPtr;
public:
	ptr0()
	{
		myPtr = nullptr;
	}
	~ptr0()
	{
		if(myPtr) myPtr->release();
	}
	ptr0(tType * p)
		: myPtr(p)
	{
		if(myPtr) myPtr->retain();
	}
	void reset(tType * ptr)
	{
		if(myPtr) myPtr->release();
		myPtr = ptr;
		if(myPtr) myPtr->retain();
	}
	tType * get() const
	{
		return myPtr;
	}
	operator tType * () const
	{
		return myPtr;
	}
	tType * operator ->() const
	{
		return myPtr;
	}
	tType * operator = (tType * ptr)
	{
		this->reset(ptr);
		return myPtr;
	}
	ptr0(ptr0 && o)
	{
		myPtr = o.myPtr;
		o.myPtr = nullptr;
	}
protected:
	ptr0(ptr0 const &);
	void operator =(ptr0 const &);
};