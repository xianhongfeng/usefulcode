﻿#include "SourceStruct.h"
#include "LexShared.h"

namespace Src {
	static std::string g_Platform = "lex";
	static std::string g_TempPath;

	static void appendLanguage(std::string & to)
	{
		if(Lex::GetUsedLanguage() ==Lex::Lang::En)
		{
			to += "en";
		}
		else if(Lex::GetUsedLanguage() == Lex::Lang::Zh)
		{
			to += "zh";
		}
		else
		{
			to += "th";
		}
	}

	char * FixPath(char const * path)
	{
		g_TempPath.clear();
		while(*path != 0)
		{
			if(path[0] == '*')
			{
				if(path[1] == 'L')
				{
//					setLanguageChar(g_TempPath);
                    appendLanguage(g_TempPath);
				}
				else if(path[1] == 'P')
				{
					g_TempPath += g_Platform;
				}
				else if(path[1] == 0) break;
				path+=2;
			}
			else
			{
				g_TempPath += *path++;
			}
		}
        return (char *)g_TempPath.data();
	}
	void SetPlatformSign(char c0, char c1)
	{
		g_Platform.clear();
		g_Platform.push_back(c0);
		g_Platform.push_back(c1);
	}
	void SetPlatformSign(std::string name)
	{
		g_Platform = name;
	}
	
	bool MakeScreenShot(const char * path, float scale)
	{
		CCScene* temp = CCDirector::sharedDirector()->getRunningScene();
		if(temp == nullptr) return false;
		CCSize size = CCEGLView::sharedOpenGLView()->getDesignResolutionSize();
		CCRenderTexture* screen = CCRenderTexture::create(size.width, size.height);
		screen->setScale(scale);
		screen->begin();
		temp->visit();
		screen->end();
		CCImage *pImage = screen->newCCImage();
		if (pImage)
		{
			pImage->saveToFile(path);
			return true;
		}
		return false;
	}
	
	bool PvrButtomFromZero = false;
}