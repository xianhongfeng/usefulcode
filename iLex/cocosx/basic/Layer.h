#pragma once

#include "SourceStruct.h"

using namespace cocos2d;

class Node : public CCNode
{
public:
	Node();
};

class Layer : public CCLayer
{
protected:
	int mTouch;
public:
	Layer();
	~Layer();
	Layer(CCPoint const &Anchor, CCPoint const & position);
	void setAnchorPos(CCPoint const &Anchor, CCPoint const & position);
	virtual void vLoad();
	static Layer * create();
	virtual void useTouch(int key);
	virtual void unuseTouch();

	virtual void ccTouchMoved(CCTouch * touch, CCEvent *);
	virtual void ccTouchEnded(CCTouch * touch, CCEvent *);
	virtual bool ccTouchBegan(CCTouch * touch, CCEvent *);

	template<typename tFunc>
	void waitThenRunOnce(float delay, tFunc func)
	{
		this->scheduleOnce(SEL_SCHEDULE(func), delay);
	}
	template<typename tFunc>
	void waitThenRunAndRun(float delay, tFunc func)
	{
		this->schedule(SEL_SCHEDULE(func), delay);
	}
	template<typename tFunc>
	void stopWait(tFunc func)
	{
		this->unschedule(SEL_SCHEDULE(func));
	}
	template<typename ParentType>
	ParentType * getParentWithType()
	{
		ParentType * res = dynamic_cast<ParentType*>(this->getParent());
		return res;
	}
};