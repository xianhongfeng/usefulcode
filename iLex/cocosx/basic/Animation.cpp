﻿#include "Animation.h"

Animation::Animation(Src::AnimationSrc1 const & Src,bool reverse)
{
	char const * fmt = Src.fmt;
	size_t count = Src.count;
	char fullName[1024];
	CCArray * frameArray = CCArray::create();
	must(frameArray != nullptr);
	if(reverse)
		for(size_t i = 0; i < count; ++i)
		{
			sprintf(fullName, fmt, count-((int)i + Src.from)+1);
			Texture * tex = Texture::create(fullName);
			CCSpriteFrame * frame = CCSpriteFrame::createWithTexture(tex, tex->getContentRect());
			must(frame != nullptr);
			frameArray->addObject(frame);
		}
	else
		for(size_t i = 0; i < count; ++i)
		{
			sprintf(fullName, fmt, (int)i + Src.from);
			Texture * tex = Texture::create(fullName);
			CCSpriteFrame * frame = CCSpriteFrame::createWithTexture(tex, tex->getContentRect());
			must(frame != nullptr);
			frameArray->addObject(frame);
		}
		createWithArray(frameArray, Src.delay);
}
Animation::Animation(Src::PvrAnimationSrc const & Src)
{
	size_t count = Src.count;
	char fullName[1024];
	CCArray * frameArray = CCArray::create();
	must(frameArray != nullptr);
	CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(Src::FixPath(Src.plist));
	for(size_t i = 0; i < count; ++i)
	{
		sprintf(fullName, Src::FixPath(Src.name), (int)i + Src.from);
		auto sf = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(fullName);
		must(sf != nullptr);
		frameArray->addObject(sf);
	}
	createWithArray(frameArray, Src.delay);
}
Animation::Animation(Src::AnimationSrc0 const & src, bool reverse, bool goBack)
{
	char const * path = Src::FixPath(src.path);
	size_t width = src.widht;			//
	size_t height = src.height;
	CCArray * frameArray = CCArray::create();
	must(frameArray != nullptr);
	Texture * tex = Texture::create(path);
	float texWidth = tex->getWidth() / width;
	float texHeight = tex->getHeight() / height;
	if(reverse)
	{
		for(size_t y = 0; y < height; ++y) for(size_t x = 0; x < width; ++x)
		{
			CCSpriteFrame * frame = CCSpriteFrame::createWithTexture(tex,
				CCRect(texWidth * (width-1-x), texHeight * (height-1-y), texWidth, texHeight));

			must(frame != nullptr);
			frameArray->addObject(frame);
		}
	}
	else
	{
		for(size_t y = 0; y < height; ++y) for(size_t x = 0; x < width; ++x)
		{
			CCSpriteFrame * frame = CCSpriteFrame::createWithTexture(tex,
				CCRect(texWidth * x, texHeight * y, texWidth, texHeight));

			must(frame != nullptr);
			frameArray->addObject(frame);
		}
	}
	if(goBack)
	{
		for (int i = 0, n = frameArray->count(); i < n; ++i) {
			frameArray->addObject(frameArray->objectAtIndex(n - 1 - i));
		}
	}
	createWithArray(frameArray, src.delay);
}

Animation::Animation(Src::AnimationSrc2 const & src, size_t fromFrame, bool flag)
{
	char const * path = Src::FixPath(src.path);
	size_t width = src.width;			//
	size_t height = src.height;
	CCArray * frameArray = CCArray::create();
	CCArray * mNewFrameArray = CCArray::create();
	must(frameArray != nullptr);
	Texture * tex = Texture::create(path);
	float mDeltWidth = (tex->getWidth() - src.szFrame.width) / ((width - 1) > 0 ? (width - 1) : width);					//每次增加mDeltWidth 宽	 calc: 图片宽 + mDeltWidth * width = tex->getWidth()
	float mDeltHeight = (tex->getHeight() - src.szFrame.height) / ((height - 1) > 0 ? (height - 1) : height);				//每次增加mDeltHeight 高 calc: 图片高 + mDeltHeight * height = tex->getHeight()

	for(size_t y = 0; y < height; ++y) for(size_t x = 0; x < width; ++x)
	{
		CCSpriteFrame * frame = CCSpriteFrame::createWithTexture(tex,
			CCRect(mDeltWidth * x, mDeltHeight * y, src.szFrame.width, src.szFrame.height));

		must(frame != nullptr);
		frameArray->addObject(frame);
	}

	if(flag)
	{
		frameArray->reverseObjects();
		if(fromFrame > 0)
		{
			for(size_t i = frameArray->count() - fromFrame - 1; i < frameArray->count(); ++i)
				mNewFrameArray->addObject(frameArray->objectAtIndex(i));
			for(size_t i = 0; i < frameArray->count() - fromFrame - 1; ++i)
				mNewFrameArray->addObject(frameArray->objectAtIndex(i));
			createWithArray(mNewFrameArray, src.delay);
		}
		else
			createWithArray(frameArray, src.delay);
	}
	else
	{
		if(fromFrame > 0)
		{
			for(size_t i = fromFrame; i < frameArray->count(); ++i)
				mNewFrameArray->addObject(frameArray->objectAtIndex(i));
			for(size_t i = 0; i < fromFrame; ++i)
				mNewFrameArray->addObject(frameArray->objectAtIndex(i));
			createWithArray(mNewFrameArray, src.delay);
		}
		else
			createWithArray(frameArray, src.delay);
	}
}

void Animation::createWithArray(CCArray *frames, float delay)
{
	initWithSpriteFrames(frames, delay);
	this->autorelease();
}
