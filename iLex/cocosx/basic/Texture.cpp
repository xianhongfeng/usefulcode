#include "SourceStruct.h"
#include "Texture.h"

Texture * Texture::load(char const * path)
{
	return create(path);
}
Texture * Texture::create(char const * path)
{
	CCTexture2D * tex = CCTextureCache::sharedTextureCache()->addImage(path);
	must(tex != nullptr);
	return reinterpret_cast<Texture*>(tex);
}
float Texture::getWidth()
{
    return this->getContentSize().width;
}
float Texture::getHeight()
{
    return this->getContentSize().height;
}
CCRect Texture::getContentRect()
{
    return CCRect(0, 0, this->getContentSize().width, this->getContentSize().height);
}
