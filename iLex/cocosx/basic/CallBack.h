#pragma once

#include "SourceStruct.h"

struct CallBack
{
	CCObject * obj;
	SEL_CallFunc func;
	CallBack()
	{
		obj = nullptr;
		func = nullptr;
	}
	CallBack(CCObject * _obj, SEL_CallFunc _func)
	{
		obj = _obj;
		func = _func;
	}
	void set(CCObject * _obj, SEL_CallFunc _func)
	{
		obj = _obj;
		func = _func;
	}
	void run()
	{
		if(obj)
			(obj->*func)();
	}
	void clearIf(CCObject* _obj)
	{
		if(obj == _obj)
		{
			obj = nullptr;
		}
	}
};