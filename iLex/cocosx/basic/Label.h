#pragma once

#include "SourceStruct.h"

class LabelTTF : public CCLabelTTF
{
public:
	LabelTTF(const char * data, size_t size);
	LabelTTF(Src::LabelTTFSrc sr, const char * data);
};

class Label : public CCLabelAtlas
{
protected:
	float myScale;
	size_t myMaxWidth;
public:
	Label(Src::LabelSrc const & df);
	void setFontScale(float scale);
	void setStringEx(const char * buf);
};

class NumLabel : public Label
{
	int64_t mNum;
	double mTemp;
	double mActDiff;
public:
	NumLabel(Src::LabelSrc const & df, int64_t num = 0);
	int64_t getNumber();
	void setNumber(int64_t number, char const * head = "", char const * tail = "");
	void setNumberAct(int64_t number, float durSum = 0.5f);
    void setNumberContinueAct(int64_t number, float durSum = 0.5f);
    void setNumberIncOne(int64_t number, float durEachUp);
private:
    void onInc(float);
	void onAct(float);
    void _setNum(int64_t num);
};