﻿#pragma once

#include "SourceConst.h"
#include <iostream>

using namespace cocos2d;

namespace Src {
	// some global functions
	char * FixPath(char const * path);
	void SetPlatformSign(char c0, char c1);
	void SetPlatformSign(std::string name);
	
	bool MakeScreenShot(const char * path, float scale);
	
	inline ccColor3B GetColor3B(uint32_t value, GLbyte by)
	{
		int r = int(by + int((value & 0xff0000) >> 16));
		int g = int(by + int((value & 0xff00) >> 8));
		int b = int(by + int(value & 0xff));
		return ccc3(r, g, b);
	}
	inline ccColor4F GetColor4f(uint32_t value)
	{
		float a = float(value >> 24) / 255.f;
		float r = float((value & 0xff0000) >> 16) / 255.f;
		float g = float((value & 0xff00) >> 8) / 255.f;
		float b = float(value & 0xff) / 255.f;
		return ccc4f(r, g, b, a);
	}
	inline CCTexture2D * LoadTexture(char const * path)
	{
		CCTexture2D * tex = CCTextureCache::sharedTextureCache()->addImage(path);
		must(tex);
		return tex;
	}

	// Point extension

    // ccpoint ＝ 大小， 左上
	struct Point : public CCPoint
	{
		Point(float x, float y)
		{
			this->x = x;
			this->y = Height - y;
		}
		void set(float x, float y)
		{
			this->x = x;
			this->y = Height - y;
		}
	};

    // 左下， 自适应
	struct Pos : public CCPoint
	{
		Pos(float x, float y)
		{
			this->x = x * Ratio;
			this->y = y * Ratio;
		}
	};

    // 左上， 自适应
	struct PosLT : public CCPoint
	{
		PosLT(float x, float y)
		{
			this->x = x * Ratio;
			this->y = Height - y * Ratio;
		}
	};

    // 自适应
	struct Size : public CCSize
	{
		Size(float width, float height)
		{
			this->width = width * Ratio;
			this->height = height * Ratio;
		}
	};

	struct Rect : public CCRect 
	{
		Rect(float x, float y, float width, float height)
		{
			if(x > 0) x *= Ratio;
			if(y > 0) y *= Ratio;
			if(width > 0) width *= Ratio;
			if(height > 0) height *= Ratio;
			setRect(x, y, width, height);
		}
	};
	
	// source struct
	struct LabelTTFSrc
	{
		size_t size; // fontSize
		ccColor3B color;
		CCPoint anchor;
		CCPoint dest;
		const char * fontName; // ""
	};
	struct AnimationSrc0
	{
		const char * path;		//路径
		size_t widht;			//宽度上子图个数
		size_t height;			//高度上子图个数
		float delay;			//延迟
	};
	struct AnimationSrc1
	{
		const char * fmt; // "hello%d.png"
		size_t from; // 0
		size_t count; // 3
		float delay;
	};
	struct AnimationSrc2
	{
		const char * path;
		size_t width;
		size_t height;
		Size szFrame;
		float delay;
	};
	// .., 1, 20, 154, 154
	struct ButtonSrc
	{
		const char * path;
		size_t count;
		CCRect srcRect;
		CCPoint anchor;
		CCPoint dest;
	};
	struct SpriteSrc
	{
		const char * path;
		CCRect srcRect;			
		CCPoint anchor;	
		CCPoint dest;
	};
	struct LabelSrc
	{
		char const * file;
		size_t count;
		unsigned int mapStart; // '0', not 0
		CCPoint destAnchor;
		CCPoint dest;
		float percent;
		size_t strLen; // 超过宽度时缩小
	};
}


namespace Src {
	// source struct for pvr 
    extern bool PvrButtomFromZero;

	struct PvrSpriteSrc
	{
		const char * plist;
		const char * name;
		CCPoint anchor;	
		CCPoint dest;
	};
	struct PvrLabelSrc
	{
		const char * plist;
		const char * name;
		size_t count;
		unsigned int mapStart;
		CCPoint destAnchor;
		CCPoint dest;
		float percent;
		size_t strLen;
	};
	struct PvrButtonSrc
	{
		const char * plist;
		const char * name;
		size_t count;
		CCPoint anchor;
		CCPoint dest;
	};
	struct PvrAnimationSrc
	{
		const char * plist;
		const char * name;		//路径
		size_t from;			//宽度上子图个数
		size_t count;		//高度上子图个数
		float delay;			//延迟
	};
}