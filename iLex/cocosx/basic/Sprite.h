#pragma once
#include "SourceStruct.h"
#include "ptr.h"
#include "Animation.h"

class Sprite : public CCSprite
{
	ptr0<CCAction> mCurAnimate;
public:
	Sprite();
	Sprite(CCSpriteFrame * src);
	Sprite(Src::AnimationSrc0 & src);
	Sprite(Src::PvrAnimationSrc & src);
	Sprite(Src::PvrSpriteSrc & src);
	Sprite(Src::SpriteSrc & src, float scaleX = 1.f, float scaleY = 1.f);
	Sprite(char const * file);
	Sprite(char const * file, CCPoint anchor, CCPoint pos);
	Sprite(char const * file, CCRect rect);
	Sprite(CCTexture2D * texture, CCRect rect);
	void setAnchorPos(CCPoint const & anchor, CCPoint const & position);
	void set(Src::SpriteSrc & src, bool move = false);
	void set(Src::PvrSpriteSrc & src, bool move = false);
	void set(Src::AnimationSrc0 & src);
	void set(Src::PvrAnimationSrc & src);
	void set(CCTexture2D * tex, CCRect rect);
	void runAnimationOnce(Animation * ani);
	void runAnimationForever(Animation * ani);
	void stopAnimation();

	//template<typename ParentType>
	//ParentType * getParentWithType()
	//{
	//	ParentType * res = dynamic_cast<ParentType*>(this->getParent());
	//	return res;
	//}
};