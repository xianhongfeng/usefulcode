#pragma once

#include "SourceStruct.h"

class Texture : public CCTexture2D
{
public:
	static Texture * load(char const * path);
	static Texture * create(char const * path);
	float getWidth();
	float getHeight();
	CCRect getContentRect() ;
};

