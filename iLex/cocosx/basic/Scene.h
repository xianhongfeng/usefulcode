#pragma once
#include "SourceStruct.h"

class Scene : public CCScene
{
public:
	Scene();
	Scene(CCLayer * layer);
	virtual void load();
	virtual void unload();
	virtual ~Scene();
};