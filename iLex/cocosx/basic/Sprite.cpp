#include "Sprite.h"

Sprite::Sprite()
{
	must(CCSprite::init());
	this->autorelease();
}
Sprite::Sprite(CCSpriteFrame * src)
{
	must(CCSprite::init());
	this->initWithSpriteFrame(src);
	this->autorelease();
}
Sprite::Sprite(Src::AnimationSrc0 & src)
{
	must(CCSprite::init());
	CCTexture2D * tex = CCTextureCache::sharedTextureCache()->addImage(Src::FixPath(src.path));
	CCSize size = tex->getContentSize();
	CCRect rect(0, 0, size.width / src.widht, size.height / src.height);
	this->initWithTexture(tex, rect);
	this->autorelease();
}
Sprite::Sprite(Src::PvrAnimationSrc & src)
{
	must(CCSprite::init());
	CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(Src::FixPath(src.plist));
	char fullName[1024];
	sprintf(fullName, src.name, 1);
	auto sf = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(Src::FixPath(fullName));
	this->initWithSpriteFrame(sf);
	this->autorelease();
}
Sprite::Sprite(Src::PvrSpriteSrc & src)
{
	must(CCSprite::init());
	CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(Src::FixPath(src.plist));
	auto sf = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(Src::FixPath(src.name));
	this->setDisplayFrame(sf);
	this->setAnchorPos(src.anchor, src.dest);
	this->autorelease();
}
Sprite::Sprite(Src::SpriteSrc & src, float scaleX, float scaleY)
{
	must(CCSprite::init());
	CCTexture2D * tex = CCTextureCache::sharedTextureCache()->addImage(Src::FixPath(src.path));
	must(tex);
	CCSize size = tex->getContentSize();
	if(src.srcRect.size.width < 0) src.srcRect.size.width *= -size.width;
	if(src.srcRect.size.height < 0) src.srcRect.size.height *= -size.height;
	if(src.srcRect.origin.x < 0) src.srcRect.origin.x *= -size.width;
	if(src.srcRect.origin.y < 0) src.srcRect.origin.y *= -size.height;

	this->initWithTexture(tex, src.srcRect);
	this->setAnchorPos(src.anchor, src.dest);
	this->autorelease();
	this->setScaleX(scaleX);
	this->setScaleY(scaleY);
}
Sprite::Sprite(char const * file)
{
	must(initWithFile(file));
	this->autorelease();
}
Sprite::Sprite(char const * file, CCPoint anchor, CCPoint pos)
{
	must(initWithFile(file));
	this->autorelease();
	this->setAnchorPoint(anchor);
	this->setPosition(pos);
}
Sprite::Sprite(char const * file, CCRect rect)
{
	must(initWithFile(file));
	this->autorelease();
	this->setTextureRect(rect);
}
Sprite::Sprite(CCTexture2D * texture, CCRect rect)
{
	must(init());
	this->autorelease();
	this->setTexture(texture);
	this->setTextureRect(rect);
}
void Sprite::setAnchorPos(CCPoint const & anchor, CCPoint const & position)
{
	this->setPosition(position);
	this->setAnchorPoint(anchor);
}
void Sprite::set(Src::SpriteSrc & src, bool move)
{
	CCTexture2D * tex = CCTextureCache::sharedTextureCache()->addImage(Src::FixPath(src.path));
	must(tex);
	CCSize size = tex->getContentSize();
	if(src.srcRect.size.width < 0) src.srcRect.size.width *= -size.width;
	if(src.srcRect.size.height < 0) src.srcRect.size.height *= -size.height;
	if(src.srcRect.origin.x < 0) src.srcRect.origin.x *= -size.width;
	if(src.srcRect.origin.y < 0) src.srcRect.origin.y *= -size.height;
	this->set(tex, src.srcRect);
	if(move)
	{
		this->setAnchorPos(src.anchor, src.dest);
	}
}

void Sprite::set(Src::PvrSpriteSrc & src, bool move)
{
	must(CCSprite::init());
	CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(Src::FixPath(src.plist));
	auto sf = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(Src::FixPath(src.name));
	this->setDisplayFrame(sf);
}

void Sprite::set(Src::AnimationSrc0 & src)
{
	CCTexture2D * tex = CCTextureCache::sharedTextureCache()->addImage(Src::FixPath(src.path));
	must(tex);
	CCSize size = tex->getContentSize();
	CCRect rect(0, 0, size.width / src.widht, size.height / src.height);

	this->set(tex, rect);
}
void Sprite::set(Src::PvrAnimationSrc & src)
{
	must(CCSprite::init());
	CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(Src::FixPath(src.plist));
	char fullName[1024];
	sprintf(fullName, src.name, 1);
	auto sf = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(Src::FixPath(fullName));
	this->setDisplayFrame(sf);
}
void Sprite::set(CCTexture2D * tex, CCRect rect)
{
	this->setTexture(tex);
	this->setTextureRect(rect);
}
void Sprite::runAnimationOnce(Animation * ani)
{
	if(mCurAnimate != nullptr) this->stopAction(mCurAnimate);
	mCurAnimate = CCAnimate::create(ani);
	mCurAnimate->setTag(0);
	this->runAction(mCurAnimate);
}
void Sprite::runAnimationForever(Animation * ani)
{
	if(mCurAnimate != nullptr) this->stopAction(mCurAnimate);
	mCurAnimate = CCRepeatForever::create(
		CCAnimate::create(ani));		
	this->runAction(mCurAnimate);
}
void Sprite::stopAnimation()
{
	if(mCurAnimate != nullptr) this->stopAction(mCurAnimate);
}
