﻿#pragma once
#include "cocos2d.h"
#include "LexJai.h"
#include <iostream>

using namespace cocos2d;

namespace Src {
	
	const float Ratio = 1.f;
	const float Width = 1024 * Ratio;
	const float Height = 768 * Ratio;
	
	namespace Anchor {
		const CCPoint LB(0.f, 0.f);
		const CCPoint RB(1.f, 0.f); 
		const CCPoint LT(0.f, 1.f);
		const CCPoint RT(1.f, 1.f);
		const CCPoint CE(0.5f, 0.5f);
		const CCPoint CB(0.5f, 0.f);
		const CCPoint CT(0.5f, 1.f);
		const CCPoint RC(1.f, 0.5f);
		const CCPoint LC(0.f, 0.5f);
	}
	
	namespace Screen {
		const CCSize Size(Width, Height);
		const CCPoint LT(0, Height);
		const CCPoint LB(0, 0);
		const CCPoint RT(Width, Height);
		const CCPoint RB(Width, 0);
		const CCPoint CE(Width/2, Height/2);
		const CCPoint CB(Width/2, 0);
		const CCPoint CT(Width/2, Height);
	}
	
	const CCRect defaultRect(0.f, 0.f, -1.f, -1.f);
	const CCPoint defaultCoord(0, 0);
	const CCPoint defaultSize(-1.f, -1.f);

	
}



#define SS schedule_selector
#define CS callfunc_selector 
#define CSO callfuncO_selector 
#define CSN callfuncN_selector 
#define MS menu_selector
#define TOUCH_BEGIN	bool virtual ccTouchBegan(CCTouch* touch, CCEvent * event)
#define TOUCH_MOVE	void virtual ccTouchMoved(CCTouch* touch, CCEvent * event)
#define TOUCH_END	void virtual ccTouchEnded(CCTouch* touch, CCEvent * event)
#define TOUCH_CC    virtual void ccTouchCancelled(CCTouch *touch, CCEvent *event) 