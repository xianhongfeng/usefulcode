#pragma once

#include "Animation.h"

inline CCBezierTo * AccMoveTo(float dur, CCPoint current, CCPoint end)
{
	ccBezierConfig config;
	CCPoint diff = end - current;
	config.controlPoint_1= current + diff / 8;
	config.controlPoint_2 = current + diff /2;
	config.endPosition = end;
	return CCBezierTo::create(dur, config);
}
inline CCBezierTo * AAccMoveTo(float dur, CCPoint current, CCPoint end)
{
	ccBezierConfig config;
	CCPoint diff = current - end;
	config.controlPoint_1= end + diff / 2;
	config.controlPoint_2 = end + diff / 8;
	config.endPosition = end;
	return CCBezierTo::create(dur, config);
}
inline CCSpawn * Spawn(CCFiniteTimeAction * act0)
{
	return CCSpawn::create(act0, nullptr);
}
inline CCSpawn * Spawn(CCFiniteTimeAction * act0, CCFiniteTimeAction * act1)
{
	return CCSpawn::create(act0, act1, nullptr);
}
inline CCSpawn * Spawn(CCFiniteTimeAction * act0, CCFiniteTimeAction * act1, CCFiniteTimeAction * act2)
{
	return CCSpawn::create(act0, act1, act2, nullptr);
}
inline CCSpawn * Spawn(CCFiniteTimeAction * act0, CCFiniteTimeAction * act1, CCFiniteTimeAction * act2, CCFiniteTimeAction * act3)
{
	return CCSpawn::create(act0, act1, act2, act3, nullptr);
}
inline CCSequence * Sequence(CCFiniteTimeAction * act0)
{
	return CCSequence::create(act0, nullptr);
}
inline CCSequence * Sequence(CCFiniteTimeAction * act0, CCFiniteTimeAction * act1)
{
	return CCSequence::create(act0, act1, nullptr);
}
inline CCSequence * Sequence(CCFiniteTimeAction * act0, CCFiniteTimeAction * act1, CCFiniteTimeAction * act2)
{
	return CCSequence::create(act0, act1, act2, nullptr);
}
inline CCSequence * Sequence(CCFiniteTimeAction * act0, CCFiniteTimeAction * act1, CCFiniteTimeAction * act2, CCFiniteTimeAction * act3)
{
	return CCSequence::create(act0, act1, act2, act3, nullptr);
}
inline CCBezierTo * BezierTo(float dur, CCPoint c0, CCPoint c1, CCPoint end)
{
	ccBezierConfig config;
	config.controlPoint_1= c0;
	config.controlPoint_2 = c1;
	config.endPosition = end;
	return CCBezierTo::create(dur, config);
}