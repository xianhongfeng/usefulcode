#pragma once
#include "cocos2d.h"
#include "SimpleAudioEngine.h"

using namespace cocos2d;

class Audio
{
protected:
	CocosDenshion::SimpleAudioEngine * myAudio;
	const char * myBG;
	int myBGSound;
	bool mfSound;
	bool mfMusic;
public:
	Audio()
	{
		mfSound = true;
		mfMusic = true;
		myAudio = nullptr;
	}
	void init()
	{
		myAudio = CocosDenshion::SimpleAudioEngine::sharedEngine();
	}
	void pauseAll()
	{
		myAudio->pauseAllEffects();
		if(myBG)
			myAudio->stopBackgroundMusic(false);
	}
	void resumeAll()
	{
		myAudio->resumeAllEffects();
		if(myBG && mfSound)
			myAudio->playBackgroundMusic(myBG, true);
	}
	void playBackgroundMusic(const char * path, bool loop = true)
	{
		myBG = path;
		if(mfMusic)
		{
			myAudio->playBackgroundMusic(path, loop);
		}
	}
	void stopBackgroundMusic()
	{
		myBG = 0;
		myAudio->stopBackgroundMusic(false);
	}
	unsigned int playEffect(const char * path, bool loop = false)
	{
		if(mfSound)
			return myAudio->playEffect(path, loop);
		else return 0;
	}
	void stopEffect(unsigned int & e)
	{
		if(mfSound)
		{
			myAudio->stopEffect(e);
		}
		e = 0;
	}
	void stopAllEffects()
	{
		myAudio->stopAllEffects();
		//this->stopBackgroundMusic();
	}
	bool hasSound()
	{
		return mfSound;
	}
	bool hasMusic()
	{
		return mfMusic;
	}
	void setSound(bool flag)
	{
		mfSound = flag;
		if(!mfSound)
		{
			myAudio->stopAllEffects();
		}
	}
	void setMusic(bool flag)
	{
		mfMusic=flag;
		if(!mfMusic)
		{
			myAudio->stopBackgroundMusic(false);
		}
		else
		{
			if(myBG != nullptr){
				myAudio->playBackgroundMusic(myBG, true);
			}
		}
	}
	static Audio * GetShared();
};

// ������ǰ�汾
namespace g {
	extern Audio audio;
}