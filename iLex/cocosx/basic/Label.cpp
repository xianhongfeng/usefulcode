#include "Label.h"


LabelTTF::LabelTTF(const char * data, size_t size)
{
	must(initWithString(data, "Arival", size, CCSizeZero, 
		kCCTextAlignmentCenter, kCCVerticalTextAlignmentTop));
	this->autorelease();
}
LabelTTF::LabelTTF(Src::LabelTTFSrc sr, const char * data)
{
	must(initWithString(data, "Candara", sr.size, CCSizeZero,
		kCCTextAlignmentLeft, kCCVerticalTextAlignmentTop));
	this->autorelease();
	this->setPosition(sr.dest);
	this->setColor(sr.color);
	this->setAnchorPoint(sr.anchor);
	this->setFontFillColor(sr.color, false);
}

Label::Label(Src::LabelSrc const & df)
{
	myScale = df.percent;
	myMaxWidth = df.strLen;
	CCTexture2D * texture = Src::LoadTexture(df.file);
	CCSize sz = texture->getContentSize();
	initWithString("0", Src::FixPath(df.file), sz.width / df.count, sz.height, df.mapStart);
	this->autorelease();
	CCLabelAtlas::setScale(myScale);
	this->setAnchorPoint(df.destAnchor);
	this->setPosition(df.dest);
}
void Label::setFontScale(float scale)
{
	CCLabelAtlas::setScale(getScale() * scale / myScale);
	myScale = scale;
}
void Label::setStringEx(const char * buf)
{		
	size_t width = strlen(buf);
	if(width <= myMaxWidth) this->setScale(1.f * myScale);
	else this->setScale((float)myMaxWidth/(float)width * myScale);
	this->setString(buf);
}

NumLabel::NumLabel(Src::LabelSrc const & df, int64_t num)  : Label(df)
{
	mTemp  = 0;
	mActDiff = 1;
	mNum = num;
	this->_setNum(num);
}
int64_t NumLabel::getNumber()
{
	return mNum;
}
void NumLabel::setNumber(int64_t number, char const * head, char const * tail)
{
	this->unschedule(SS(NumLabel::onAct));
	char buf[128];
	mNum = number;
	sprintf(buf, "%s%lld%s", head, number, tail);
	setStringEx(buf);
}
void NumLabel::setNumberAct(int64_t number, float durSum)
{
	int count = (durSum / 0.05f);
	if(count <= 1) {
		this->setNumber(number);
		return;
	}
	this->unschedule(SS(NumLabel::onAct));
	mTemp = mNum;
	mNum = number;
	mActDiff = (mNum - mTemp) / count;
	this->schedule(SS(NumLabel::onAct), durSum / count, kCCRepeatForever, 0.f);
}
void NumLabel::setNumberContinueAct(int64_t number, float durSum)
{
	int count = (durSum / 0.05f);
	if(count <= 1) {
		this->setNumber(number);
		return;
	}
	this->unschedule(SS(NumLabel::onAct));
	mNum = number;
	mActDiff = (mNum - mTemp) / count;
	this->schedule(SS(NumLabel::onAct), durSum / count, kCCRepeatForever, 0.f);
}
void NumLabel::setNumberIncOne(int64_t number, float durEachUp)
{
	mNum = number;
	this->unschedule(SS(NumLabel::onInc));
	this->schedule(SS(NumLabel::onInc), durEachUp);
}

void NumLabel::onInc(float)
{
	if(mTemp > mNum)
	{
		mTemp -= 1;
		if(mTemp < mNum)
		{
			this->_setNum(mNum);
			mTemp = mNum;
			this->unschedule(SS(NumLabel::onInc));
		}
		else this->_setNum((int64_t)mTemp);
	}
	else
	{
		mTemp += 1;
		if(mTemp > mNum)
		{
			this->_setNum(mNum);
			mTemp = mNum;
			this->unschedule(SS(NumLabel::onInc));
		}
		else this->_setNum((int64_t)mTemp);
	}
}
void NumLabel::onAct(float)
{
	mTemp += mActDiff;
	if((mActDiff > 0 && mTemp < mNum ) || (mActDiff < 0 && mTemp > mNum));
	else
	{
		this->unschedule(SS(NumLabel::onAct));
		mTemp = mNum;
	}
	this->_setNum((int64_t)mTemp);
}
void NumLabel::_setNum(int64_t num)
{
	char buf[128];
	sprintf(buf, "%lld", num);
	setStringEx(buf);
}
