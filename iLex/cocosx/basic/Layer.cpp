#include "Layer.h"

Node::Node()
{
	must(init());
	this->autorelease();
}


Layer::Layer()
{
	must(init());
	this->autorelease();
	this->setPosition(0, 0);
	setTouchPriority(1024);
	setTouchMode(kCCTouchesOneByOne);
	mTouch = 1024;
}
Layer::~Layer()
{
	CCSpriteFrameCache::sharedSpriteFrameCache()->removeUnusedSpriteFrames();
	CCTextureCache::sharedTextureCache()->removeUnusedTextures();
}
Layer::Layer(CCPoint const &Anchor, CCPoint const & position)
{
	must(init());
	this->autorelease();		
	setTouchPriority(1024);
	setTouchMode(kCCTouchesOneByOne);
	this->setPosition(position);
	this->setAnchorPoint(Anchor);
}
void Layer::setAnchorPos(CCPoint const &Anchor, CCPoint const & position)
{
	this->setPosition(position);
	this->setAnchorPoint(Anchor);
}
void Layer::vLoad()
{
}
Layer * Layer::create()
{
	return new Layer();
}
void Layer::useTouch(int key)
{
	setTouchPriority(key);
	setTouchMode(kCCTouchesOneByOne);
	setTouchEnabled(true);
	mTouch = key;
}
void Layer::unuseTouch()
{
	setTouchEnabled(false);
}

void Layer::ccTouchMoved(CCTouch * touch, CCEvent *)
{
}
void Layer::ccTouchEnded(CCTouch * touch, CCEvent *)
{
}
bool Layer::ccTouchBegan(CCTouch * touch, CCEvent *)
{
	return true;
}
