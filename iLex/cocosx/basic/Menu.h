#pragma once
#include "Sprite.h"

class MenuX : public CCMenu
{
public:
	MenuX();
	MenuX(CCPoint const & anchor, CCPoint const & position);
	void setAnchorPos(CCPoint const & anchor, CCPoint const & position);
	virtual void useTouch(int key);
	virtual void unuseTouch();
};

class ButtonImage : public CCMenuItemSprite
{
public:
	ButtonImage(const char * file, size_t count, CCObject* target, SEL_MenuHandler selector, bool flip = false);
	ButtonImage(Src::ButtonSrc & src, CCObject* target, SEL_MenuHandler selector, bool flip = false);
	ButtonImage(Src::PvrButtonSrc & src, CCObject* target, SEL_MenuHandler selector, bool flip = false);
	void reset(Src::ButtonSrc & src);
	void reset(Src::PvrButtonSrc & src);
	void setAnchorPos(CCPoint const & anchor, CCPoint const & position);
	void setState(bool enable, bool visible);
};