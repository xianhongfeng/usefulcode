//
//  ScrollLayer.h
//  Magician
//
//  Created by leaving on 4/1/14.
//
//

#include "ScrollLayer.h"

ScrollLayer::ScrollLayer(Sprite * sprite, CCRect touchRect, CCRect viewRect, int touch)
{
	this->useTouch(touch);
	mSprite = sprite;
	mTouchRect = touchRect;
	mViewRect = viewRect;
	mSpriteRect.size = viewRect.size;
	mContent = mSprite->getContentSize();

	mSpriteRect.size = mViewRect.size;
	mSpriteRect.origin.setPoint(0, 0);
	if(mSpriteRect.size.width > mContent.width)
	{
		mSpriteRect.size.width = mContent.width;
	}
	if(mSpriteRect.size.height > mContent.height)
	{
		mSpriteRect.size.height = mContent.height;
	}

	mPosition = 0;
	mHeight = mContent.height - mViewRect.size.height;
	if(mHeight < 0) mHeight = 0.f;
	mSprite->setPosition(ccp(viewRect.origin.x, viewRect.origin.y + viewRect.size.height));
	mSprite->setAnchorPoint(Src::Anchor::LT);
	this->addChild(mSprite, 0);
	resetPosition();
}
void ScrollLayer::resetPosition()
{
	mSpriteRect.origin.setPoint(0, mPosition);
	mSprite->setTextureRect(mSpriteRect);
}
void ScrollLayer::move(float dy)
{
	mPosition += dy;
	if(mPosition < 0) mPosition = 0.f;
	if(mPosition > mHeight) mPosition = mHeight;
	resetPosition();
}
void ScrollLayer::ccTouchMoved(CCTouch * touch, CCEvent *)
{
	move(touch->getLocation().y - lastTouch.y);
	lastTouch = touch->getLocation();
}
void ScrollLayer::ccTouchEnded(CCTouch * touch, CCEvent *)
{
}
bool ScrollLayer::ccTouchBegan(CCTouch * touch, CCEvent *)
{
	if(mTouchRect.containsPoint(touch->getLocation()))
	{
		lastTouch = touch->getLocation();
		return true;
	}
	return false;
}
