//
//  LoadCCBI.h
//  ccbifirst
//
//  Created by lover on 14/12/3.
//
//

#pragma once
#include "cocos-ext.h"
#include "../LexCocosx.h"

template<typename T>
class NodeAM : public T
{
    ptr0<cocos2d::extension::CCBAnimationManager> mAM;
public:
    static NodeAM * create()
    {
        return new NodeAM;
    }
    virtual void setActionManager(cocos2d::extension::CCBAnimationManager * am)
    {
        mAM = am;
    }
    cocos2d::extension::CCBAnimationManager * getAnimationManager()
    {
        return mAM;
    }
};

typedef NodeAM<Node> NodeAct;
typedef NodeAM<Sprite> SpriteAct;
typedef NodeAM<Layer> LayerAct;

template<typename T>
class SpriteLoader : public cocos2d::extension::CCSpriteLoader
{
public:
    CCB_STATIC_NEW_AUTORELEASE_OBJECT_METHOD(SpriteLoader, loader);
    
    virtual T * createCCSprite(cocos2d::CCSprite * pParent, cocos2d::extension::CCBReader * pCCBReader) {
        return new T();
    }
};
template<typename T>
class SpriteLoaderAM : public cocos2d::extension::CCSpriteLoader
{
public:
    CCB_STATIC_NEW_AUTORELEASE_OBJECT_METHOD(SpriteLoaderAM, loader);
    
    virtual T * createCCSprite(cocos2d::CCSprite * pParent, cocos2d::extension::CCBReader * pCCBReader) {
        auto t = new T();
        t->setActionManager(pCCBReader->getAnimationManager());
        return t;
    }
};
template<typename T>
class LayerLoader : public cocos2d::extension::CCLayerLoader
{
public:
    CCB_STATIC_NEW_AUTORELEASE_OBJECT_METHOD(LayerLoader, loader);
    
    virtual T * createCCLayer(cocos2d::CCLayer * pParent,
                              cocos2d::extension::CCBReader * pCCBReader)
    {
        return new T();
    }
};
template<typename T>
class LayerLoaderAM : public cocos2d::extension::CCLayerLoader
{
public:
    CCB_STATIC_NEW_AUTORELEASE_OBJECT_METHOD(LayerLoaderAM, loader);
    
    virtual T * createCCLayer(cocos2d::CCLayer * pParent,
                              cocos2d::extension::CCBReader * pCCBReader)
    {
        auto t = new T();
        t->setActionManager(pCCBReader->getAnimationManager());
        return t;
    }
};
template<typename T>
class NodeLoader : public cocos2d::extension::CCNodeLoader
{
public:
    NodeLoader()
    {
        printf("Node loader craeted");
    }
    CCB_STATIC_NEW_AUTORELEASE_OBJECT_METHOD(NodeLoader<T>, loader);
    
    virtual CCNode * createCCNode(cocos2d::CCNode * pParent,
                             cocos2d::extension::CCBReader * pCCBReader)
    {
        auto t = new T();
        return t;
    }
};
template<typename T>
class NodeLoaderAM : public cocos2d::extension::CCNodeLoader
{
public:
    NodeLoaderAM()
    {
        printf("Node loader craeted");
    }
    CCB_STATIC_NEW_AUTORELEASE_OBJECT_METHOD(NodeLoaderAM, loader);
    
    virtual CCNode * createCCNode(cocos2d::CCNode * pParent,
                                  cocos2d::extension::CCBReader * pCCBReader)
    {
        auto t = new T();
        t->setActionManager(pCCBReader->getAnimationManager());
        return t;
    }
};

template<typename Type, typename Loader>
inline Type * loadCCBI(std::string customClass, std::string ccbiFile)
{
    using namespace cocos2d::extension;
    CCNodeLoaderLibrary * ccNodeLoaderLibrary = cocos2d::extension::CCNodeLoaderLibrary::newDefaultCCNodeLoaderLibrary();
    ccNodeLoaderLibrary->registerCCNodeLoader(customClass.data(), Loader::loader());
    
    /* Create an autorelease CCBReader. */
    cocos2d::extension::CCBReader * ccbReader = new cocos2d::extension::CCBReader(ccNodeLoaderLibrary);
    
    /* Read a ccbi file. */
    Type * node = (Type*)ccbReader->readNodeGraphFromFile(ccbiFile.data());
    ccbReader->release();
    
    return node;
}
template<typename Type>
inline Type * loadCCBINode(std::string customClass, std::string ccbiFile)
{
    using namespace cocos2d::extension;
    return loadCCBI<Type, NodeLoader<Type> >(customClass, ccbiFile);
}
template<typename Type>
inline Type * loadCCBILayer(std::string customClass, std::string ccbiFile)
{
    using namespace cocos2d::extension;
    return loadCCBI<Type, SpriteLoader<Type> >(customClass, ccbiFile);
}
template<typename Type>
inline Type * loadCCBISprite(std::string customClass, std::string ccbiFile)
{
    using namespace cocos2d::extension;
    return loadCCBI<Type, SpriteLoader<Type> >(customClass, ccbiFile);
}

template<typename Type>
inline Type * loadCCBINodeAM(std::string customClass, std::string ccbiFile)
{
    using namespace cocos2d::extension;
    return loadCCBI<Type, NodeLoaderAM<Type> >(customClass, ccbiFile);
}
template<typename Type>
inline Type * loadCCBILayerAM(std::string customClass, std::string ccbiFile)
{
    using namespace cocos2d::extension;
    return loadCCBI<Type, SpriteLoaderAM<Type> >(customClass, ccbiFile);
}
template<typename Type>
inline Type * loadCCBISpriteAM(std::string customClass, std::string ccbiFile)
{
    using namespace cocos2d::extension;
    return loadCCBI<Type, SpriteLoaderAM<Type> >(customClass, ccbiFile);
}