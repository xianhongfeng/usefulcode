#include "Grey.h"

Grey::Grey(GLbyte op)
{
	must(initWithColor(ccc4(0, 0, 0, 0)));
	this->autorelease();
	this->setOpacity(op);
}
Grey::Grey(CCSize size, GLbyte op)
{
	must(initWithColor(ccc4(0,0,0,0), size.width, size.height));
	this->autorelease();
	this->setOpacity(op);
}
void Grey::fade(float dur, GLubyte op, GLubyte opEnd)
{
	this->stopAllActions();
	this->setOpacity(op);
	this->runAction(CCFadeTo::create(dur, opEnd));
}
