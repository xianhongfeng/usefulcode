#pragma once
#include "Sprite.h"

class Grey : public CCLayerColor
{
public:
	Grey(GLbyte op);
	Grey(CCSize size, GLbyte op = (GLbyte)0xff);
	void fade(float dur, GLubyte op, GLubyte opEnd);
};
