#include "ProgressLine.h"

ProgressLine::ProgressLine(Src::SpriteSrc & spSrc, bool isHori)
	: Sprite(spSrc)
{
	if(!spSrc.anchor.fuzzyEquals(Src::Anchor::LB, 0.01f))
	{
		CCPoint diff = ccp(getContentSize().width * getAnchorPoint().x,
			getContentSize().height * getAnchorPoint().y);
		CCPoint center = getPosition() - diff;
		this->setAnchorPoint(Src::Anchor::LB);
		this->setPosition(center);
	}
	mSrc = spSrc;
	bHori = isHori;
	this->updateValue(0.f);
}
float ProgressLine::getValue()
{
	return mValue;
}
void ProgressLine::updateValue(float value)
{
	mValue = value;
	CCRect rect;
	if(bHori)
	{
		rect.origin = mSrc.srcRect.origin;
		rect.size.height = mSrc.srcRect.size.height;
		rect.size.width = mSrc.srcRect.size.width * mValue;
	}
	else
	{
		rect.size.height = mSrc.srcRect.size.height * mValue;
		rect.size.width = mSrc.srcRect.size.width;
		rect.origin.x = mSrc.srcRect.origin.x;
		rect.origin.y = mSrc.srcRect.origin.y + mSrc.srcRect.size.height * (1.f - mValue);
	}
	this->setTextureRect(rect);
}
