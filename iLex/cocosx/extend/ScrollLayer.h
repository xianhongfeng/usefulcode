//
//  ScrollLayer.h
//  Magician
//
//  Created by leaving on 4/1/14.
//
//

#pragma once

#include "Layer.h"
#include "Sprite.h"

class ScrollLayer : public Layer
{
private:
	Sprite * mSprite;
	CCRect mTouchRect;
	CCRect mViewRect;
	CCRect mSpriteRect;
	CCSize mContent;
	float mPosition;
	CCPoint lastTouch;
	float mHeight;

public:
	ScrollLayer(Sprite * sprite, CCRect touchRect, CCRect viewRect, int touch);
	void resetPosition();
	void move(float dy);
	virtual void ccTouchMoved(CCTouch * touch, CCEvent *);
	virtual void ccTouchEnded(CCTouch * touch, CCEvent *);
	virtual bool ccTouchBegan(CCTouch * touch, CCEvent *);
};

