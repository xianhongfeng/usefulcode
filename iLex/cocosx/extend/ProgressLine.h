#pragma once
#include "Sprite.h"

class ProgressLine : public Sprite
{
	float mValue;
	Src::SpriteSrc mSrc;
	bool bHori;
public:
	ProgressLine(Src::SpriteSrc & spSrc, bool isHori);
	float getValue();
	void updateValue(float value);
};