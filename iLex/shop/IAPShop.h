//  Purchase.h
//  iLexLib
//
//  Created by leaving on 14-5-19.
//
//

#pragma once

#include "ShopBase.h"

namespace Lex {

    class IapShop : public ShopBase
	{
    public:
        static IapShop * getShared();
        virtual bool isShopValid();
        virtual void connectStore(ShopResult resultCallBack, std::string storeCode);
        virtual bool isConnected();
        virtual void requestProducts(std::vector<std::string> itemNames);
        virtual bool buy(std::string itemName);
        virtual std::string getReceiptData();
    };
}
