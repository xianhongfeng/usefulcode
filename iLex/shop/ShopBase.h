//
//  ShopBase.h
//  ILex
//
//  Created by lover on 15/3/9.
//  Copyright (c) 2015年 lover. All rights reserved.
//

#pragma once
#include <vector>
#include <string>
#include <functional>

namespace Lex {
    struct ShopBase
    {
    public:
        typedef std::function<void (std::string item, bool success)> ShopResult;
        virtual ~ShopBase() {}
        virtual bool isShopValid() = 0;
        virtual void connectStore(ShopResult resultCallBack, std::string storeCode) = 0;
        virtual bool isConnected() = 0;
        virtual void requestProducts(std::vector<std::string> itemNames) = 0;
        virtual bool buy(std::string itemName) = 0;
        
        virtual std::string getReceiptData() = 0;
    };
}