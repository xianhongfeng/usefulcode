#import <StoreKit/StoreKit.h>
#import <Foundation/Foundation.h>
#import "IAPShop.h"

#define kProductsLoadedNotification         @"ProductsLoaded"
#define kProductPurchasedNotification       @"ProductPurchased"
#define kProductPurchaseFailedNotification  @"ProductPurchaseFailed"

@interface IAPHelper : NSObject <SKProductsRequestDelegate, SKPaymentTransactionObserver> {
    NSArray * _products;
    SKProductsRequest * _request;
    Lex::ShopBase::ShopResult _resultCallBack;
    BOOL bOK;
}

@property (retain) NSArray * products;
@property (retain) SKProductsRequest *request;

+ (IAPHelper *) sharedHelper;
- (void)start;
- (void)setResultCallBack:(Lex::ShopBase::ShopResult) func;

- (void)requestProducts:(NSSet *) identifiers;
- (BOOL)buyProduct:(NSString *)productIdentifier;
- (BOOL)isConnected;
- (NSString* )getReceiptData;
@end


#define ITMS_PROD_VERIFY_RECEIPT_URL        @"https://buy.itunes.apple.com/verifyReceipt"
#define ITMS_SANDBOX_VERIFY_RECEIPT_URL     @"https://sandbox.itunes.apple.com/verifyReceipt"

@implementation IAPHelper

@synthesize products = _products;
@synthesize request = _request;

static IAPHelper * _sharedHelper;

+ (IAPHelper *) sharedHelper {
    
    if (_sharedHelper != nil) {
        return _sharedHelper;
    }
    _sharedHelper = [[IAPHelper alloc] init];
    _sharedHelper->_resultCallBack = nullptr;
    _sharedHelper->bOK = NO;
    return _sharedHelper;
    
}
- (void)start
{
    [[SKPaymentQueue defaultQueue] addTransactionObserver:[IAPHelper sharedHelper]];
    bOK = YES;
}
- (void)setResultCallBack:(Lex::ShopBase::ShopResult) func
{
    _resultCallBack = func;
}
- (BOOL) isConnected
{
    return bOK && _resultCallBack != nullptr;
}
- (void)requestProducts:(NSSet *) identifiers {
    
    self.request = [[[SKProductsRequest alloc] initWithProductIdentifiers:identifiers] autorelease];
    _request.delegate = self;
    [_request start];
    
}
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    
    self.products = response.products;
    self.request = nil;
    NSLog(@"Received products results count [%d]", response.products.count);
    
    for (int i = 0; i < response.products.count; ++i)
    {
        NSLog(@"-- %@\n", [response.products objectAtIndex:i]);
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kProductsLoadedNotification object:_products];
}
// In dealloc
- (void)dealloc
{
    [_products release];
    _products = nil;
    [_request release];
    _request = nil;
    [super dealloc];
}

- (void)recordTransaction:(SKPaymentTransaction *)transaction {
    // Optional: Record the transaction on the server side...
}

- (void)provideContent:(NSString *)productIdentifier {
    
    NSLog(@"Toggling flag for: %@", productIdentifier);
    [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:productIdentifier];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kProductPurchasedNotification object:productIdentifier];
}

- (void)completeTransaction:(SKPaymentTransaction *)transaction {
    
    NSLog(@"completeTransaction:%@", [transaction.payment productIdentifier]);
    //	[self verifyPruchase];
    if(_resultCallBack) _resultCallBack([transaction.payment.productIdentifier UTF8String], true);
    
    [self recordTransaction: transaction];
    [self provideContent: transaction.payment.productIdentifier];
    //    [self verifyPurchase];
    //    [self verifyPurchase];
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    //    [self getReceiptData];
}

- (void)restoreTransaction:(SKPaymentTransaction *)transaction {
    
    NSLog(@"restoreTransaction...");
    [self recordTransaction: transaction];
    [self provideContent: transaction.originalTransaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    
    if(_resultCallBack) _resultCallBack([transaction.payment.productIdentifier UTF8String], true);
}
- (NSString* )getReceiptData
{
    NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
    // 从沙盒中获取到购买凭据
    NSData *receiptData = [NSData dataWithContentsOfURL:receiptURL];
    
    // 在网络中传输数据，大多情况下是传输的字符串而不是二进制数据
    // 传输的是BASE64编码的字符串
    /**
     BASE64 常用的编码方案，通常用于数据传输，以及加密算法的基础算法，传输过程中能够保证数据传输的稳定性
     BASE64是可以编码和解码的
     */
    NSString *encodeStr = [receiptData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    
    //    NSString *payload = [NSString stringWithFormat:@"{\"receipt-data\" : \"%@\"}", encodeStr];
    //    NSData *payloadData = [payload dataUsingEncoding:NSUTF8StringEncoding];
    //    std::string str((const char *)payloadData.bytes, payloadData.length);
    //    NSLog(@"ReceiptData:%s", str.data());
    return encodeStr;
}
- (void)verifyPurchase
{
    // 验证凭据，获取到苹果返回的交易凭据
    // appStoreReceiptURL iOS7.0增加的，购买交易完成后，会将凭据存放在该地址
    NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
    // 从沙盒中获取到购买凭据
    NSData *receiptData = [NSData dataWithContentsOfURL:receiptURL];
    
    // 发送网络POST请求，对购买凭据进行验证
    NSURL *url = [NSURL URLWithString:ITMS_SANDBOX_VERIFY_RECEIPT_URL];
    // 国内访问苹果服务器比较慢，timeoutInterval需要长一点
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0f];
    
    request.HTTPMethod = @"POST";
    
    // 在网络中传输数据，大多情况下是传输的字符串而不是二进制数据
    // 传输的是BASE64编码的字符串
    /**
     BASE64 常用的编码方案，通常用于数据传输，以及加密算法的基础算法，传输过程中能够保证数据传输的稳定性
     BASE64是可以编码和解码的
     */
    NSString *encodeStr = [receiptData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    
    NSString *payload = [NSString stringWithFormat:@"{\"receipt-data\" : \"%@\"}", encodeStr];
    NSData *payloadData = [payload dataUsingEncoding:NSUTF8StringEncoding];
    
    request.HTTPBody = payloadData;
    
    // 提交验证请求，并获得官方的验证JSON结果
    NSData *result = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    
    // 官方验证结果为空
    if (result == nil) {
        NSLog(@"验证失败");
    }
    
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:result options:NSJSONReadingAllowFragments error:nil];
    
    NSLog(@"%@", dict);
    
    if (dict != nil) {
        // 比对字典中以下信息基本上可以保证数据安全
        NSLog(@"验证成功");
    }
}


- (void)failedTransaction:(SKPaymentTransaction *)transaction {
    
    if (transaction.error.code != SKErrorPaymentCancelled)
    {
        if(transaction.error.localizedDescription)
        {
            if(_resultCallBack)
                _resultCallBack([transaction.payment.productIdentifier UTF8String], false);
            NSLog(@"Transaction error: %@", transaction.error.localizedDescription);
        }
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:kProductPurchaseFailedNotification object:transaction];
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    
    if(_resultCallBack) _resultCallBack([transaction.payment.productIdentifier UTF8String], false);
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction *transaction in transactions)
    {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
            default:
                break;
        }
    }
}

- (BOOL)buyProduct:(NSString *)productIdentifier {
    
    NSLog(@"Buying %@...", productIdentifier);
    
    SKPayment * payment = nil;
    for (SKProduct * product in _products)
    {
        if([[product productIdentifier] isEqualToString:productIdentifier])
        {
            payment = [SKPayment paymentWithProduct:product];
            break;
        }
    }
    if(payment)
    {
        [[SKPaymentQueue defaultQueue] addPayment:payment];
        return YES;
    }
    return NO;
}
@end


namespace Lex {
    IapShop * IapShop::getShared()
    {
        static IapShop shop;
        return &shop;
    }
    bool IapShop::isShopValid()
    {
        return true;
    }
    void IapShop::connectStore(ShopResult resultCallBack, std::string storeCode)
    {
        [[IAPHelper sharedHelper] setResultCallBack:resultCallBack];
    }
    
    bool IapShop::isConnected()
    {
        return [[IAPHelper sharedHelper] isConnected];
    }
    void IapShop::requestProducts(std::vector<std::string> itemNames)
    {
        if(!isConnected()) return;
        NSMutableSet *identifierSet = [[[NSMutableSet alloc] init] autorelease];
        for (auto identifier : itemNames)
        {
            NSString *iden = [NSString stringWithUTF8String:identifier.c_str()];
            [identifierSet addObject:iden];
        }
        [[IAPHelper sharedHelper] requestProducts:identifierSet];
    }
    bool IapShop::buy(std::string itemName)
    {
        NSString *iden = [NSString stringWithUTF8String:itemName.c_str()];
        return [[IAPHelper sharedHelper] buyProduct:iden];
    }
    std::string getReceiptData()
    {
        return [[[IAPHelper sharedHelper] getReceiptData] UTF8String];
    }
}
